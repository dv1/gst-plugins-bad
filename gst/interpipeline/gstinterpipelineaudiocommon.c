#include "gstinterpipelineaudiocommon.h"


GST_DEBUG_CATEGORY_EXTERN (inter_pipeline_audio_debug);
#define GST_CAT_DEFAULT inter_pipeline_audio_debug




G_DEFINE_TYPE (GstInterPipelineAudioChannel, gst_inter_pipeline_audio_channel,
    GST_TYPE_INTER_PIPELINE_CHANNEL);


static void gst_inter_pipeline_audio_channel_finalize (GObject * object);

static GstInterPipelineChannel
    * gst_inter_pipeline_audio_channel_create (GstInterPipelineChannelTable *
    table, const gchar * name, gpointer extra_params, gpointer user_data);


void
gst_inter_pipeline_audio_channel_class_init (GstInterPipelineAudioChannelClass *
    klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_channel_finalize);
}


void
gst_inter_pipeline_audio_channel_init (GstInterPipelineAudioChannel *
    inter_pipeline_audio_channel)
{
  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_channel->audio_info));

  gst_inter_pipeline_audio_channel_properties_init (&
      (inter_pipeline_audio_channel->properties));

  inter_pipeline_audio_channel->audio_adapter = gst_adapter_new ();
  g_assert (inter_pipeline_audio_channel->audio_adapter != NULL);
}


static void
gst_inter_pipeline_audio_channel_finalize (GObject * object)
{
  GstInterPipelineAudioChannel *inter_pipeline_audio_channel =
      GST_INTER_PIPELINE_AUDIO_CHANNEL (object);

  if (inter_pipeline_audio_channel->audio_adapter != NULL)
    g_object_unref (G_OBJECT (inter_pipeline_audio_channel->audio_adapter));

  G_OBJECT_CLASS (gst_inter_pipeline_audio_channel_parent_class)->finalize
      (object);
}


static GstInterPipelineChannel *
gst_inter_pipeline_audio_channel_create (G_GNUC_UNUSED
    GstInterPipelineChannelTable * table, const gchar * name,
    gpointer extra_params, G_GNUC_UNUSED gpointer user_data)
{
  GstInterPipelineAudioChannelProperties *properties;
  GstInterPipelineAudioChannel *audio_channel;

  g_assert (extra_params != NULL);

  properties = (GstInterPipelineAudioChannelProperties *) extra_params;
  audio_channel = (GstInterPipelineAudioChannel *)
      g_object_new (gst_inter_pipeline_audio_channel_get_type (), NULL);
  gst_object_set_name (GST_OBJECT (audio_channel), name);
  gst_object_ref_sink (GST_OBJECT (audio_channel));

  audio_channel->properties = *properties;

  return GST_INTER_PIPELINE_CHANNEL_CAST (audio_channel);
}



void
gst_inter_pipeline_audio_info_init (GstInterPipelineAudioInfo * info)
{
  g_assert (info != NULL);

  gst_audio_info_init (&(info->details.pcm_details));
  info->type = GST_INTER_PIPELINE_AUDIO_TYPE_PCM;
  info->valid = FALSE;
  info->updated = FALSE;
}


gboolean
gst_inter_pipeline_audio_info_from_caps (GstInterPipelineAudioInfo * info,
    const GstCaps * caps)
{
  gboolean ret = FALSE;
  GstStructure *structure;

  g_assert (info != NULL);
  g_assert (caps != NULL);

  info->valid = FALSE;

  structure = gst_caps_get_structure (caps, 0);

  if (gst_structure_has_name (structure, "audio/x-raw")) {
    info->type = GST_INTER_PIPELINE_AUDIO_TYPE_PCM;

    if (!gst_audio_info_from_caps (&(info->details.pcm_details), caps)) {
      GST_ERROR ("failed to parse PCM caps %" GST_PTR_FORMAT, (gpointer) caps);
      goto done;
    }
  } else if (gst_structure_has_name (structure, "audio/x-alaw")
      || gst_structure_has_name (structure, "audio/x-mulaw")) {
    gint i;
    guint num_channels;
    guint64 channel_mask;

    if (gst_structure_has_name (structure, "audio/x-alaw"))
      info->type = GST_INTER_PIPELINE_AUDIO_TYPE_ALAW;
    else
      info->type = GST_INTER_PIPELINE_AUDIO_TYPE_MULAW;

    if (!gst_structure_get_int (structure, "rate", &i)) {
      GST_ERROR ("missing rate value in caps %" GST_PTR_FORMAT,
          (gpointer) caps);
      goto done;
    }
    info->details.a_mu_law_details.sample_rate = i;

    if (!gst_structure_get_int (structure, "channels", &i)) {
      GST_ERROR ("missing channels value in caps %" GST_PTR_FORMAT,
          (gpointer) caps);
      goto done;
    }
    info->details.a_mu_law_details.num_channels = num_channels = i;

    if (!gst_structure_get (structure, "channel-mask", &channel_mask, NULL)) {
      channel_mask = gst_audio_channel_get_fallback_mask (num_channels);
      GST_DEBUG ("input caps have no channel mask - using fallback mask %#"
          G_GINT64_MODIFIER "x for %u channels", channel_mask, num_channels);
      goto done;
    }
    info->details.a_mu_law_details.channel_mask = channel_mask;
  } else {
    GST_ERROR ("caps %" GST_PTR_FORMAT " have an unsupported media type",
        (gpointer) caps);
    goto done;
  }

  info->valid = TRUE;
  info->updated = TRUE;

  ret = TRUE;

done:
  return ret;
}


GstCaps *
gst_inter_pipeline_audio_info_to_caps (const GstInterPipelineAudioInfo * info)
{
  GstCaps *caps;

  if (G_UNLIKELY (!info->valid))
    return NULL;

  switch (info->type) {
    case GST_INTER_PIPELINE_AUDIO_TYPE_PCM:
    {
      caps = gst_audio_info_to_caps (&(info->details.pcm_details));
      break;
    }

    case GST_INTER_PIPELINE_AUDIO_TYPE_ALAW:
    case GST_INTER_PIPELINE_AUDIO_TYPE_MULAW:
    {
      const gchar *media_type =
          (info->type ==
          GST_INTER_PIPELINE_AUDIO_TYPE_ALAW) ? "audio/x-alaw" :
          "audio/x-mulaw";

      caps = gst_caps_new_simple (media_type,
          "rate", G_TYPE_INT,
          (gint) (info->details.a_mu_law_details.sample_rate), "channels",
          G_TYPE_INT, (gint) (info->details.a_mu_law_details.num_channels),
          "channel-mask", GST_TYPE_BITMASK,
          (guint64) (info->details.a_mu_law_details.channel_mask), NULL);

      break;
    }

    default:
      g_assert_not_reached ();
  }

  return caps;
}


void
gst_inter_pipeline_audio_info_copy (GstInterPipelineAudioInfo * dest,
    const GstInterPipelineAudioInfo * src)
{
  g_assert (dest != NULL);
  g_assert (src != NULL);

  memcpy (dest, src, sizeof (GstInterPipelineAudioInfo));
  /* Mark the dest as updated, since we modified dest.
   * (There is no need to check the "valid" member prior to this, since
   * if it is set to FALSE, then "updated" is ignored anyway.) */
  dest->updated = TRUE;
}


guint
gst_inter_pipeline_audio_info_get_sample_rate (const GstInterPipelineAudioInfo *
    info)
{
  g_assert (info != NULL);
  g_assert (info->valid);

  switch (info->type) {
    case GST_INTER_PIPELINE_AUDIO_TYPE_PCM:
      return GST_AUDIO_INFO_RATE (&(info->details.pcm_details));

    case GST_INTER_PIPELINE_AUDIO_TYPE_MULAW:
    case GST_INTER_PIPELINE_AUDIO_TYPE_ALAW:
      return info->details.a_mu_law_details.sample_rate;

    default:
      g_assert_not_reached ();
  }
}


guint
gst_inter_pipeline_audio_info_get_bpf (const GstInterPipelineAudioInfo * info)
{
  g_assert (info != NULL);
  g_assert (info->valid);

  switch (info->type) {
    case GST_INTER_PIPELINE_AUDIO_TYPE_PCM:
      return GST_AUDIO_INFO_BPF (&(info->details.pcm_details));

    case GST_INTER_PIPELINE_AUDIO_TYPE_MULAW:
    case GST_INTER_PIPELINE_AUDIO_TYPE_ALAW:
      /* mu-law & a-law use 8 bits per sample, so the number
       * of channel equals the number of bytes per frame. */
      return info->details.a_mu_law_details.num_channels;

    default:
      g_assert_not_reached ();
  }
}


void
gst_inter_pipeline_audio_fill_silence (const GstInterPipelineAudioInfo * info,
    gpointer dest, gsize num_frames)
{
  g_assert (info != NULL);
  g_assert (info->valid);

  switch (info->type) {
    case GST_INTER_PIPELINE_AUDIO_TYPE_PCM:
    {
      gst_audio_format_fill_silence (info->details.pcm_details.finfo, dest,
          num_frames * GST_AUDIO_INFO_BPF (&(info->details.pcm_details)));
      break;
    }

    case GST_INTER_PIPELINE_AUDIO_TYPE_MULAW:
    case GST_INTER_PIPELINE_AUDIO_TYPE_ALAW:
    {
      /* 1 sample = 8 bits = 1 byte, so num_frames equals
       * the number of samples in the memory block. */
      memset (dest, 0, num_frames);
      break;
    }

    default:
      g_assert_not_reached ();
  }
}



void gst_inter_pipeline_audio_channel_properties_init
    (GstInterPipelineAudioChannelProperties * properties)
{
  properties->capacity_in_ns = DEFAULT_AUDIO_CHANNEL_CAPACITY;
  properties->base_latency_in_ns = DEFAULT_AUDIO_CHANNEL_BASE_LATENCY;
  properties->period_length_in_ns = DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH;
  properties->capacity_in_frames = 0;
  properties->base_latency_in_frames = 0;
  properties->period_length_in_frames = 0;
}


void gst_inter_pipeline_audio_channel_properties_recalc_num_frames
    (GstInterPipelineAudioChannelProperties * properties,
    const GstInterPipelineAudioInfo * audio_info)
{
  properties->capacity_in_frames =
      gst_util_uint64_scale (properties->capacity_in_ns,
      gst_inter_pipeline_audio_info_get_sample_rate (audio_info), GST_SECOND);
  properties->base_latency_in_frames =
      gst_util_uint64_scale (properties->base_latency_in_ns,
      gst_inter_pipeline_audio_info_get_sample_rate (audio_info), GST_SECOND);
  properties->period_length_in_frames =
      gst_util_uint64_scale (properties->period_length_in_ns,
      gst_inter_pipeline_audio_info_get_sample_rate (audio_info), GST_SECOND);

  /* Make sure the sizes in frames are never 0. */
  properties->capacity_in_frames = MAX (properties->capacity_in_frames, 1);
  properties->base_latency_in_frames =
      MAX (properties->base_latency_in_frames, 1);
  properties->period_length_in_frames =
      MAX (properties->period_length_in_frames, 1);

  GST_DEBUG ("recalculated channel properties: time -> frames:"
      "  capacity: %" GST_TIME_FORMAT " -> %" G_GUINT64_FORMAT
      "  base latency: %" GST_TIME_FORMAT " -> %" G_GUINT64_FORMAT
      "  period length: %" GST_TIME_FORMAT " -> %" G_GUINT64_FORMAT,
      GST_TIME_ARGS (properties->capacity_in_ns),
      properties->capacity_in_frames,
      GST_TIME_ARGS (properties->base_latency_in_ns),
      properties->base_latency_in_frames,
      GST_TIME_ARGS (properties->period_length_in_ns),
      properties->period_length_in_frames);
}



static GstInterPipelineChannelTable audio_channel_table =
GST_INTER_PIPELINE_CHANNEL_TABLE_INIT ("audio-channel-table",
    gst_inter_pipeline_audio_channel_create, NULL);

GstInterPipelineChannelTable *
gst_inter_pipeline_audio_channel_table_get (void)
{
  return &audio_channel_table;
}
