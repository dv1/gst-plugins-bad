#ifndef GSTINTERPIPELINEVIDEOSRC_H
#define GSTINTERPIPELINEVIDEOSRC_H

#include <gst/gst.h>


G_BEGIN_DECLS


#define GST_TYPE_INTER_PIPELINE_VIDEO_SRC \
    (gst_inter_pipeline_video_src_get_type())


GType gst_inter_pipeline_video_src_get_type (void);


G_END_DECLS


#endif /* GSTINTERPIPELINEVIDEOSRC_H */
