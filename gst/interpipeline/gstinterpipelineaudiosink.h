#ifndef GSTINTERPIPELINEAUDIOSINK_H
#define GSTINTERPIPELINEAUDIOSINK_H

#include <gst/gst.h>


G_BEGIN_DECLS


#define GST_TYPE_INTER_PIPELINE_AUDIO_SINK \
    (gst_inter_pipeline_audio_sink_get_type())


GType gst_inter_pipeline_audio_sink_get_type (void);


G_END_DECLS


#endif /* GSTINTERPIPELINEAUDIOSINK_H */
