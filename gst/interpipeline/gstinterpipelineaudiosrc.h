#ifndef GSTINTERPIPELINEAUDIOSRC_H
#define GSTINTERPIPELINEAUDIOSRC_H

#include <gst/gst.h>


G_BEGIN_DECLS


#define GST_TYPE_INTER_PIPELINE_AUDIO_SRC \
    (gst_inter_pipeline_audio_src_get_type())


GType gst_inter_pipeline_audio_src_get_type (void);


G_END_DECLS


#endif /* GSTINTERPIPELINEAUDIOSRC_H */
