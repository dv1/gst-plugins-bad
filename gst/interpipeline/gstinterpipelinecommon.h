#ifndef GSTINTERPIPELINECOMMON_H
#define GSTINTERPIPELINECOMMON_H

#include <glib.h>
#include <gst/gst.h>


G_BEGIN_DECLS


/* Note that all data structures in here are for internal use in
 * the interpipeline plugin ONLY. So, no gtkdoc compatible comments
 * are provided. */




/**** Forward declarations ****/

typedef struct _GstInterPipelineChannel GstInterPipelineChannel;
typedef struct _GstInterPipelineChannelClass GstInterPipelineChannelClass;

typedef struct _GstInterPipelineChannelTable GstInterPipelineChannelTable;

typedef struct _GstInterPipelineChannelTablePriv
    GstInterPipelineChannelTablePriv;
typedef struct _GstInterPipelineChannelTablePrivClass
    GstInterPipelineChannelTablePrivClass;




/**** GstInterPipelineOverflowMode ****/

/* Enum for determining what to do when a channel reaches capacity. */

typedef enum
{
  /* Overwrite the oldest data if the channel is full. */
  GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE,
  /* Block (using the channel's condition variable and mutex)
   * if the channel is full. */
  GST_INTER_PIPELINE_OVERFLOW_MODE_BLOCK
}
GstInterPipelineOverflowMode;

#define GST_TYPE_INTER_PIPELINE_OVERFLOW_MODE \
    (gst_inter_pipeline_overflow_mode_get_type())
GType gst_inter_pipeline_overflow_mode_get_type (void);




/**** GstInterPipelineChannel ****/

/* The structure of a channel. Each channel is tied to a table. When
 * the channel is deallocated, it automatically removes itself from
 * its associated table. Each channel has a name that has to be
 * unique within the table. A channel also has a mutex and a condition
 * variable. The mutex is there to allow for synchronized access to
 * the payload, while the condition variable is there in case the
 * sink element that is transmitting data through this channel is
 * operating in the "block" overflow mode (see above).
 *
 * Video/audio/etc. interpipeline elements all have their media
 * specific subclasses of GstInterPipelineChannel. */


#define GST_TYPE_INTER_PIPELINE_CHANNEL \
    (gst_inter_pipeline_channel_get_type())
#define GST_INTER_PIPELINE_CHANNEL(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_CHANNEL, \
    GstInterPipelineChannel))
#define GST_INTER_PIPELINE_CHANNEL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_CHANNEL, \
    GstInterPipelineChannelClass))
#define GST_INTER_PIPELINE_CHANNEL_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_CHANNEL, \
    GstInterPipelineChannelClass))
#define GST_INTER_PIPELINE_CHANNEL_CAST(obj) \
    ((GstInterPipelineChannel *)(obj))
#define GST_IS_INTER_PIPELINE_CHANNEL(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_CHANNEL))
#define GST_IS_INTER_PIPELINE_CHANNEL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_CHANNEL))


/* Utility macros for subclasses to make use of the payload
 * mutex & condition variable. */

#define GST_INTER_PIPELINE_CHANNEL_LOCK(CHANNEL) \
  G_STMT_START \
    g_mutex_lock(&(GST_INTER_PIPELINE_CHANNEL_CAST(CHANNEL)->payload_mutex)); \
  G_STMT_END

#define GST_INTER_PIPELINE_CHANNEL_UNLOCK(CHANNEL) \
  G_STMT_START \
    g_mutex_unlock(&(GST_INTER_PIPELINE_CHANNEL_CAST(CHANNEL)->payload_mutex)); \
  G_STMT_END

#define GST_INTER_PIPELINE_CHANNEL_SIGNAL_COND(CHANNEL) \
  G_STMT_START \
    g_cond_signal(&(GST_INTER_PIPELINE_CHANNEL_CAST(CHANNEL)->payload_cond)); \
  G_STMT_END

#define GST_INTER_PIPELINE_CHANNEL_WAIT_COND(CHANNEL) \
  G_STMT_START \
    g_cond_wait(&(GST_INTER_PIPELINE_CHANNEL_CAST(CHANNEL)->payload_cond), \
        &(GST_INTER_PIPELINE_CHANNEL_CAST(CHANNEL)->payload_mutex)); \
  G_STMT_END


enum
{
  GST_INTER_PIPELINE_CHANNEL_ASSOCIATED_SINK_IDX = 0,
  GST_INTER_PIPELINE_CHANNEL_ASSOCIATED_SRC_IDX,
  GST_INTER_PIPELINE_CHANNEL_NUM_ASSOCIATED_ELEM_INDICES
};


struct _GstInterPipelineChannel
{
  GstObject parent;

  /* Mutex and condition variable to synchronize access to the payload held
   * by GstInterPipelineChannel subclasses. */
  GMutex payload_mutex;
  GCond payload_cond;

  /* Pointer to the global table that contains a pointer to this channel. */
  GstInterPipelineChannelTable *table;

  /* These pointers are there to catch cases when the user tries to associate
   * a sink with a channel that is already associated with a sink.
   * (Same with sources.) These pointers are accessed by get_channel()
   * and by the GstInterPipelineChannel finalizer.
   * See GST_INTER_PIPELINE_CHANNEL_ASSOCIATED_*_IDX for the array indices. */
  GstElement *associated_elements[2];
};


struct _GstInterPipelineChannelClass
{
  GstObjectClass parent_class;
};


/* Convenience macro for getting the name.
 * Casts CHANNEL to GstInterPipelineChannel*. */
#define GST_INTER_PIPELINE_CHANNEL_NAME(CHANNEL) \
    gst_inter_pipeline_channel_get_name(GST_INTER_PIPELINE_CHANNEL(CHANNEL))

/* Returns a const pointer to the name of the channel.
 * This pointer is only valid for as long as the channel exists. */
const gchar *gst_inter_pipeline_channel_get_name (GstInterPipelineChannel *
    channel);

/* Unrefs the channel. Prefer this over calling gst_object_unref()
 * directly, since this one also resets the corresponding associated
 * elements to NULL. */
void gst_inter_pipeline_channel_unref (GstInterPipelineChannel * channel,
    GstElement * associated_element);

GType gst_inter_pipeline_channel_get_type (void);




/**** GstInterPipelineChannelTable ****/

/* This table contains channels. A table is supposed to be globally available,
 * that is, defined as an internal global variable. There is one table per
 * data category. For example, there is one table for video data, one for audio
 * data etc.
 *
 * The names of channels have to be unique _within_ the same table, but not
 * _across_ tables.
 *
 * As said, a table is defined globally with GST_INTER_PIPELINE_CHANNEL_TABLE_INIT.
 * Example:
 *
 * static GstInterPipelineChannelTable my_table =
 *   GST_INTER_PIPELINE_CHANNEL_TABLE_INIT("my_table", create_my_channel, NULL);
 *
 * Channels are created by the gst_inter_pipeline_channel_table_get_channel()
 * call if they don't already exist, and automatically get added to the table.
 * Once a channel is finalized, it removes itself from the table.
 *
 * Tables have a private structure that is based on GstObject. This structure
 * contains the actual hashtable, and doubles as a way to detect table related
 * leaks. When a channel removes itself trom the table, a check if performed
 * to see if the table is now empty. If it is, then the private structure is
 * unref'd. If later a new channel is created and added, and there is no
 * hashtable, the private structure is created. If for some reason a channel
 * does not remove itself from a table, the private GstObject based structure
 * will not be destroyed properly after all channels are destroyed. In other
 * words, it will leak. Being GstObject-based, it will show up in the GStreamer
 * leak tracer, which helps with debugging such leaks.
 */


/* Function pointer type for functions that a table uses for
 * creating new channels on-demand.
 *
 * user_data is set to the create_channel_func_data pointer
 * of the table. Since that data is supposed to be statically
 * stored, there is no GDestroyNotify function (also, there
 * is no place where said function could be executed). */
typedef GstInterPipelineChannel
    *(*GstInterPipelineCreateChannelFunc) (GstInterPipelineChannelTable * table,
    const gchar * name, gpointer extra_params, gpointer user_data);

struct _GstInterPipelineChannelTable
{
  GstInterPipelineChannelTablePriv *priv;
  GMutex mutex;
  const gchar *table_name;

  GstInterPipelineCreateChannelFunc create_channel_func;
  gpointer create_channel_func_data;
};

#define GST_INTER_PIPELINE_CHANNEL_TABLE_INIT(TABLE_NAME, CREATE_CHANNEL_FUNC, CREATE_CHANNEL_FUNC_DATA) \
  { NULL, { 0 }, (TABLE_NAME), CREATE_CHANNEL_FUNC, CREATE_CHANNEL_FUNC_DATA }

/* Retrieves a channel with the given name. If none exists, one is created
 * with the given name (by using the table's create_channel_func) and then
 * added to the table.
 *
 * channel_name must be a non-empty string.
 *
 * requesting_element is the element that is calling this function. This
 * is used to keep track of what elements are making use of this channel.
 * The requesting_element pointer is stored in the associated_elements
 * array in the channel structure. req_element_is_sink determines whether
 * requesting_element refers to a sink or a source element.
 *
 * extra_params is a pointer to a structure that contains
 * additional parameters, and is passed to the create_channel_func pointer
 * of the given table. This allows for creating a channel with some sort
 * of format specific configuration set right from the start, for example. */
GstInterPipelineChannel
    *gst_inter_pipeline_channel_table_get_channel (GstInterPipelineChannelTable
    * table, GstElement * requesting_element, gboolean req_element_is_sink,
    gchar const *channel_name, gpointer extra_params);


G_END_DECLS


#endif /* GSTINTERPIPELINECOMMON_H */
