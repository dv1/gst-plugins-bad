#include <gst/gst.h>
#include "gstinterpipelinecommon.h"


GST_DEBUG_CATEGORY_EXTERN (inter_pipeline_common_debug);
#define GST_CAT_DEFAULT inter_pipeline_common_debug




static void
gst_inter_pipeline_channel_table_remove_channel (GstInterPipelineChannelTable *
    table, const gchar * channel_name);




/**** GstInterPipelineOverflowMode implementation ****/


GType
gst_inter_pipeline_overflow_mode_get_type (void)
{
  static volatile GType type;
  static const GEnumValue values[] = {
    {GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE,
        "Overwrite oldest data in the channel", "overwrite"},
    {GST_INTER_PIPELINE_OVERFLOW_MODE_BLOCK,
        "Block until channel has enough free space", "block"},
    {0, NULL, NULL}
  };

  if (g_once_init_enter (&type)) {
    GType _type =
        g_enum_register_static ("GstInterPipelineOverflowMode", values);
    g_once_init_leave (&type, _type);
  }

  return type;
}




/**** GstInterPipelineChannel implementation ****/


G_DEFINE_ABSTRACT_TYPE (GstInterPipelineChannel, gst_inter_pipeline_channel,
    GST_TYPE_OBJECT);


static void gst_inter_pipeline_channel_finalize (GObject * object);


void
gst_inter_pipeline_channel_class_init (GstInterPipelineChannelClass * klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_channel_finalize);
}


void
gst_inter_pipeline_channel_init (GstInterPipelineChannel *
    inter_pipeline_channel)
{
  guint i;
  g_mutex_init (&inter_pipeline_channel->payload_mutex);
  g_cond_init (&inter_pipeline_channel->payload_cond);

  inter_pipeline_channel->table = NULL;

  for (i = 0; i < GST_INTER_PIPELINE_CHANNEL_NUM_ASSOCIATED_ELEM_INDICES; ++i)
    inter_pipeline_channel->associated_elements[i] = NULL;
}


static void
gst_inter_pipeline_channel_finalize (GObject * object)
{
  GstInterPipelineChannel *inter_pipeline_channel =
      (GstInterPipelineChannel *) (object);

  g_mutex_clear (&inter_pipeline_channel->payload_mutex);
  g_cond_clear (&inter_pipeline_channel->payload_cond);

  GST_DEBUG_OBJECT (inter_pipeline_channel, "finalizing channel %s",
      GST_OBJECT_NAME (object));

  if (G_LIKELY (inter_pipeline_channel->table != NULL))
    gst_inter_pipeline_channel_table_remove_channel
        (inter_pipeline_channel->table, GST_OBJECT_NAME (object));

  G_OBJECT_CLASS (gst_inter_pipeline_channel_parent_class)->finalize (object);
}


const gchar *
gst_inter_pipeline_channel_get_name (GstInterPipelineChannel * channel)
{
  g_assert (channel != NULL);
  return GST_OBJECT_NAME (channel);
}


void
gst_inter_pipeline_channel_unref (GstInterPipelineChannel * channel,
    GstElement * associated_element)
{
  g_assert (channel != NULL);

  if (associated_element != NULL) {
    guint i;

    /* Protect with the table lock to make sure
     * the associated_elements array is not subject
     * to race conditions when two elements unref
     * the channel at the same time. */
    g_mutex_lock (&channel->table->mutex);

    for (i = 0; i < GST_INTER_PIPELINE_CHANNEL_NUM_ASSOCIATED_ELEM_INDICES; ++i) {
      if (channel->associated_elements[i] == associated_element) {
        channel->associated_elements[i] = NULL;
        break;
      }
    }

    g_mutex_unlock (&channel->table->mutex);
  }

  gst_object_unref (GST_OBJECT (channel));
}




/**** GstInterPipelineChannelTablePriv implementation ****/

/* This helper structure contains the actual hashtable. It is created
 * on-demand, and destroyed once all elements get removed from the hashtable.
 *
 * We could in theory directly store the hashtable in GstInterPipelineChannelTable.
 * But by using GstInterPipelineChannelTablePriv, it becomes possible to track
 * leaked tables with GStreamer's tracing framework. */


struct _GstInterPipelineChannelTablePriv
{
  GstObject parent;
  GHashTable *hashtable;
};


struct _GstInterPipelineChannelTablePrivClass
{
  GstObjectClass parent_class;
  GstObject parent;
};


GType gst_inter_pipeline_channel_table_priv_get_type (void);

G_DEFINE_TYPE (GstInterPipelineChannelTablePriv,
    gst_inter_pipeline_channel_table_priv, GST_TYPE_OBJECT);


static void gst_inter_pipeline_channel_table_priv_finalize (GObject * object);


void gst_inter_pipeline_channel_table_priv_class_init
    (GstInterPipelineChannelTablePrivClass * klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_channel_table_priv_finalize);
}


void
gst_inter_pipeline_channel_table_priv_init (GstInterPipelineChannelTablePriv *
    channel_hash_table)
{
  channel_hash_table->hashtable = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, /* the key is part of the value (= the channel) */
      NULL                      /* the hash table does not own the channels */
      );
}


static void
gst_inter_pipeline_channel_table_priv_finalize (GObject * object)
{
  GstInterPipelineChannelTablePriv *channel_hash_table =
      (GstInterPipelineChannelTablePriv *) object;

  g_hash_table_destroy (channel_hash_table->hashtable);

  G_OBJECT_CLASS (gst_inter_pipeline_channel_table_priv_parent_class)->finalize
      (object);
}




/**** GstInterPipelineChannelTable implementation ****/


GstInterPipelineChannel *
gst_inter_pipeline_channel_table_get_channel (GstInterPipelineChannelTable *
    table, GstElement * requesting_element, gboolean req_element_is_sink,
    gchar const *channel_name, gpointer extra_params)
{
  GstInterPipelineChannel *channel = NULL;

  g_assert (table != NULL);
  g_assert (table->create_channel_func != NULL);
  g_assert ((channel_name != NULL) && (channel_name[0] != '\0'));

  g_mutex_lock (&table->mutex);

  /* Create the actual table structure if it does not exist yet. */
  if (G_UNLIKELY (table->priv == NULL)) {
    GST_DEBUG ("table \"%s\" contains no hashtable; creating one",
        table->table_name);

    table->priv = (GstInterPipelineChannelTablePriv *)
        g_object_new (gst_inter_pipeline_channel_table_priv_get_type (), NULL);

    gst_object_set_name (GST_OBJECT (table->priv), table->table_name);
  }

  /* Now try to get the channel. If if does not exist yet,
   * create it and add it to the hashtable. */
  channel =
      (GstInterPipelineChannel *) g_hash_table_lookup (table->priv->hashtable,
      channel_name);
  if (channel == NULL) {
    gboolean not_yet_in_table;

    GST_DEBUG
        ("table \"%s\"'s hashtable does not contain any channel \"%s\"; adding new one to it",
        table->table_name, channel_name);

    channel =
        table->create_channel_func (table, channel_name, extra_params,
        table->create_channel_func_data);
    if (G_UNLIKELY (channel == NULL)) {
      GST_WARNING ("could not create new channel");
      goto finish;
    }
    GST_DEBUG
        ("created new channel \"%s\" (pointer %p) and added it to table \"%s\"",
        channel_name, (gpointer) channel, table->table_name);

    /* Associate the new channel with this table so the channel can
     * remove itself from said table in its finalize() function. */
    channel->table = table;

    /* Using GST_OBJECT_NAME() as the key, since the string it refers to
     * continues to exist until the object is destroyed, which is
     * a requirement for GHashTable keys. */
    /* TODO: Is it safe to keep a reference to the name like that? */
    not_yet_in_table =
        g_hash_table_insert (table->priv->hashtable, GST_OBJECT_NAME (channel),
        channel);
    g_assert (not_yet_in_table);
  } else {
    GST_DEBUG
        ("table \"%s\"'s hashtable does contain an channel \"%s\" (pointer %p); ref'ing it",
        table->table_name, channel_name, (gpointer) channel);
    gst_object_ref (GST_OBJECT (channel));
  }

  /* Check the associated_elements array to see if this channel is already
   * associated with a sink/src element. If not, associate it with the
   * requesting_element. */
  {
    guint assoc_element_idx;

    assoc_element_idx =
        req_element_is_sink ? GST_INTER_PIPELINE_CHANNEL_ASSOCIATED_SINK_IDX :
        GST_INTER_PIPELINE_CHANNEL_ASSOCIATED_SRC_IDX;

    if (G_UNLIKELY (channel->associated_elements[assoc_element_idx] != NULL)) {
      GST_ERROR ("channel \"%s\" is already associated to element %"
          GST_PTR_FORMAT, channel_name,
          (gpointer) (channel->associated_elements[assoc_element_idx]));
      gst_object_unref (GST_OBJECT (channel));
      channel = NULL;
      goto finish;
    } else
      channel->associated_elements[assoc_element_idx] = requesting_element;
  }

finish:
  /* Corner case: We created a fresh new table (because none existed
   * before), but then something went wrong (create_channel_func()
   * failed for example). In such a case, there is no new channel,
   * nothing got added to the new table, and we are exiting with failure,
   * so destroy the table before we exit. */
  if (g_hash_table_size (table->priv->hashtable) == 0) {
    gst_object_unref (table->priv);
    table->priv = NULL;
  }

  g_mutex_unlock (&table->mutex);
  return channel;
}


static void
gst_inter_pipeline_channel_table_remove_channel (GstInterPipelineChannelTable *
    table, const gchar * channel_name)
{
  g_assert (table != NULL);
  g_assert ((channel_name != NULL) && (channel_name[0] != '\0'));

  g_mutex_lock (&table->mutex);

  if (table->priv != NULL) {
    guint hashtable_size;

    GST_DEBUG
        ("there is a hashtable in table \"%s\"; removing channel \"%s\" from it",
        table->table_name, channel_name);

    g_hash_table_remove (table->priv->hashtable, channel_name);

    hashtable_size = g_hash_table_size (table->priv->hashtable);
    GST_DEBUG ("table \"%s\"'s hash table size after removing channel: %u",
        table->table_name, hashtable_size);

    if (hashtable_size == 0) {
      GST_DEBUG ("table \"%s\"'s' hashtable is now empty; discarding it",
          table->table_name);

      gst_object_unref (table->priv);
      table->priv = NULL;
    }
  } else {
    GST_DEBUG
        ("there is no hashtable in table \"%s\"; no need to remove channel \"%s\"",
        table->table_name, channel_name);
  }

  g_mutex_unlock (&table->mutex);
}
