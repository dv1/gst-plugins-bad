#ifndef GSTINTERPIPELINEAUDIOCOMMON_H
#define GSTINTERPIPELINEAUDIOCOMMON_H

#include <gst/gst.h>
#include <gst/audio/audio.h>
#include <gst/base/gstadapter.h>
#include "gstinterpipelinecommon.h"


G_BEGIN_DECLS


typedef enum
{
  GST_INTER_PIPELINE_AUDIO_TYPE_PCM,
  GST_INTER_PIPELINE_AUDIO_TYPE_MULAW,
  GST_INTER_PIPELINE_AUDIO_TYPE_ALAW,
}
GstInterPipelineAudioType;


#define DEFAULT_AUDIO_CHANNEL_CAPACITY      (GST_SECOND)
#define DEFAULT_AUDIO_CHANNEL_BASE_LATENCY  (100 * GST_MSECOND)
#define DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH (25 * GST_MSECOND)


#define GST_INTER_PIPELINE_AUDIO_CAPS \
  GST_AUDIO_CAPS_MAKE(GST_AUDIO_FORMATS_ALL) \
  ", layout = (string) { interleaved, non-interleaved }; " \
  "audio/x-alaw, rate = (int) [ 1, MAX ], channels = (int) [ 1, MAX ]; " \
  "audio/x-mulaw, rate = (int) [ 1, MAX ], channels = (int) [ 1, MAX ]; "


#define GST_TYPE_INTER_PIPELINE_AUDIO_CHANNEL \
    (gst_inter_pipeline_audio_channel_get_type())
#define GST_INTER_PIPELINE_AUDIO_CHANNEL(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_AUDIO_CHANNEL, \
    GstInterPipelineAudioChannel))
#define GST_INTER_PIPELINE_AUDIO_CHANNEL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_AUDIO_CHANNEL, \
    GstInterPipelineAudioChannelClass))
#define GST_INTER_PIPELINE_AUDIO_CHANNEL_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_AUDIO_CHANNEL, \
    GstInterPipelineAudioChannelClass))
#define GST_INTER_PIPELINE_AUDIO_CHANNEL_CAST(obj) \
    ((GstInterPipelineAudioChannel *)(obj))
#define GST_IS_INTER_PIPELINE_AUDIO_CHANNEL(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_AUDIO_CHANNEL))
#define GST_IS_INTER_PIPELINE_AUDIO_CHANNEL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_AUDIO_CHANNEL))


typedef struct _GstInterPipelineAudioChannel GstInterPipelineAudioChannel;
typedef struct _GstInterPipelineAudioChannelClass
    GstInterPipelineAudioChannelClass;


/* Audio information structure. This is used instead of GstAudioInfo,
 * since that one is only usable for PCM formats, while we support
 * non-PCM audio formats as well (A-law etc). */
typedef struct
{
  /* The actual information. */
  union
  {
    GstAudioInfo pcm_details;
    struct
    {
      guint sample_rate;
      gsize num_channels;
      guint64 channel_mask;
    }
    a_mu_law_details;
  } details;

  /* What format we are using: PCM, A-law etc. */
  GstInterPipelineAudioType type;

  /* TRUE if this structure is filled with valid values. */
  gboolean valid;
  /* TRUE if this structure was updated since the last time
   * the consumer looked at it. (The consumer sets this
   * to FALSE once it has noticed the updated audio info.) */
  gboolean updated;
}
GstInterPipelineAudioInfo;


/* Audio channel properties. These are kept in this structure,
 * since this makes it easier for sources, sinks etc. to copy
 * and store this set of properties. */
typedef struct
{
  /* NOTE: The *_frames fields here are calculated out of
   * the *_in_ns ones by calling
   * gst_inter_pipeline_audio_channel_properties_recalculate(). */

  /* How much data the channel can hold, in nanoseconds
   * and frames.
   *
   * Overflow happens when a sink tries to fill in data
   * and there is not enough free space. (Free space is
   * defined as (capacity - fill level).) Depending on
   * the sink's overflow mode, either the oldest data
   * is discarded (GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE),
   * or the sink blocks until there is enough free space
   * (GST_INTER_PIPELINE_OVERFLOW_MODE_BLOCK).
   *
   * The channel capacity is also used as the max-latency
   * in latency queries in audio src elements.
   *
   * This value can be modified by audio sink elements. */
  GstClockTime capacity_in_ns;
  guint64 capacity_in_frames;
  /* The base latency of this channel, in nanoseconds
   * and frames.
   *
   * This base latency is used as the min-latency value
   * of latency queries (in both audio sink and source
   * elements), and as the render delay in audio sinks.
   *
   * This value can be modified by audio sink elements. */
  GstClockTime base_latency_in_ns;
  guint64 base_latency_in_frames;
  /* Length of an audio src period, in nanoseconds
   * and frames.
   *
   * A "period" is a block of data that is read by the
   * source from the channel and then pushed downstream.
   * The source always reads out data from the channel
   * in period lengths.
   *
   * This value can be modified by audio sink elements. */
  GstClockTime period_length_in_ns;
  guint64 period_length_in_frames;
}
GstInterPipelineAudioChannelProperties;


/* Audio specific inter pipeline channel subclass. */
struct _GstInterPipelineAudioChannel
{
  GstInterPipelineChannel parent;

  /* All of these members must be accessed with the
   * channel payload_mutex locked. */

  /* Audio info for data that passes through the channel.
   * The "details" and "type" members are defined only if
   * the "valid" member is set to TRUE. */
  GstInterPipelineAudioInfo audio_info;

  /* The channel's current properties. These are defined
   * when the channel is created, and are immutable
   * afterwards. */
  GstInterPipelineAudioChannelProperties properties;

  /* The adapter that holds the actual audio data. */
  GstAdapter *audio_adapter;
};


struct _GstInterPipelineAudioChannelClass
{
  GstInterPipelineChannelClass parent_class;
};


GType gst_inter_pipeline_audio_channel_get_type (void);


/* Initializes the audio info structure by setting its type
 * to PCM, calling gst_audio_info_init() on the pcm_details,
 * and setting the valid and updated flags to FALSE. */
void gst_inter_pipeline_audio_info_init (GstInterPipelineAudioInfo * info);

/* Sets audio info fields with values from the given caps.
 * Only the first structure in the caps is looked at.
 * Caps have to be fixed.
 * In case of an error, this returns FALSE. */
gboolean gst_inter_pipeline_audio_info_from_caps (GstInterPipelineAudioInfo *
    info, const GstCaps * caps);

/* Creates a caps out of the given audio info. If the audio
 * info is marked as invalid, this returns NULL. */
GstCaps *gst_inter_pipeline_audio_info_to_caps (const GstInterPipelineAudioInfo
    * info);

/* Copies the contents of one audio info to another, and sets
 * the dest audio info's updated flag to TRUE. */
void gst_inter_pipeline_audio_info_copy (GstInterPipelineAudioInfo * dest,
    const GstInterPipelineAudioInfo * src);

/* Utility function for retrieving the sample rate, which has
 * to be done differently depending on the type of audio
 * (PCM, A-law etc). */
guint gst_inter_pipeline_audio_info_get_sample_rate (const
    GstInterPipelineAudioInfo * info);
/* Utility function for retrieving the number of bytes per frame,
 * which has to be done differently depending on the type of audio
 * (PCM, A-law etc). */
guint gst_inter_pipeline_audio_info_get_bpf (const GstInterPipelineAudioInfo *
    info);


/* Fills the first num_frames frames at the memory region pointed to by dest
 * with silence. This is similar to gst_audio_format_fill_silence(), but also
 * works for non-PCM formats. */
void gst_inter_pipeline_audio_fill_silence (const GstInterPipelineAudioInfo *
    info, gpointer dest, gsize num_frames);


/* Initializes the structure by setting its *_in_ns fields to default values
 * and its *_frames fields to 0. */
void
gst_inter_pipeline_audio_channel_properties_init
    (GstInterPipelineAudioChannelProperties * properties);

/* Recalculates the *_frames fields based on the values from audio_info and
 * the *_in_ns fields. This is typically called after one or more of the
 * *_in_ns fields got updated, or after the format of the audio data changed. */
void
gst_inter_pipeline_audio_channel_properties_recalc_num_frames
    (GstInterPipelineAudioChannelProperties * properties,
    const GstInterPipelineAudioInfo * audio_info);


/* Returns a pointer to the global audio channel table. */
GstInterPipelineChannelTable *gst_inter_pipeline_audio_channel_table_get (void);


G_END_DECLS


#endif /* GSTINTERPIPELINEAUDIOCOMMON_H */
