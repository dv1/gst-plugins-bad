#ifndef GSTINTERPIPELINEVIDEOSINK_H
#define GSTINTERPIPELINEVIDEOSINK_H

#include <gst/gst.h>


G_BEGIN_DECLS


#define GST_TYPE_INTER_PIPELINE_VIDEO_SINK \
    (gst_inter_pipeline_video_sink_get_type())


GType gst_inter_pipeline_video_sink_get_type (void);


G_END_DECLS


#endif /* GSTINTERPIPELINEVIDEOSINK_H */
