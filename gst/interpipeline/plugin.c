#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>

#include "gstinterpipelinevideosink.h"
#include "gstinterpipelinevideosrc.h"
#include "gstinterpipelineaudiosink.h"
#include "gstinterpipelineaudiosrc.h"


GST_DEBUG_CATEGORY (inter_pipeline_common_debug);
GST_DEBUG_CATEGORY (inter_pipeline_audio_debug);


static gboolean
plugin_init (GstPlugin * plugin)
{
  gboolean ok = TRUE;

  GST_DEBUG_CATEGORY_INIT (inter_pipeline_common_debug, "interpipelinecommon",
      0, "inter pipeline common functionality");
  GST_DEBUG_CATEGORY_INIT (inter_pipeline_audio_debug, "interpipelineaudio", 0,
      "inter pipeline misc audio functionality");

  ok = ok
      && gst_element_register (plugin, "interpipelinevideosrc", GST_RANK_NONE,
      GST_TYPE_INTER_PIPELINE_VIDEO_SRC);
  ok = ok
      && gst_element_register (plugin, "interpipelinevideosink", GST_RANK_NONE,
      GST_TYPE_INTER_PIPELINE_VIDEO_SINK);
  ok = ok
      && gst_element_register (plugin, "interpipelineaudiosrc", GST_RANK_NONE,
      GST_TYPE_INTER_PIPELINE_AUDIO_SRC);
  ok = ok
      && gst_element_register (plugin, "interpipelineaudiosink", GST_RANK_NONE,
      GST_TYPE_INTER_PIPELINE_AUDIO_SINK);
  return ok;
}


GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    interpipeline,
    "plugin for inter-pipeline communication (in the same process)",
    plugin_init, VERSION, "LGPL", GST_PACKAGE_NAME, GST_PACKAGE_ORIGIN)
