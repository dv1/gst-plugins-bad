#ifndef GSTINTERPIPELINEVIDEOCOMMON_H
#define GSTINTERPIPELINEVIDEOCOMMON_H

#include <gst/gst.h>
#include <gst/video/video.h>
#include "gstinterpipelinecommon.h"


G_BEGIN_DECLS


#define GST_TYPE_INTER_PIPELINE_VIDEO_CHANNEL \
    (gst_inter_pipeline_video_channel_get_type())
#define GST_INTER_PIPELINE_VIDEO_CHANNEL(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_VIDEO_CHANNEL, \
    GstInterPipelineVideoChannel))
#define GST_INTER_PIPELINE_VIDEO_CHANNEL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_VIDEO_CHANNEL, \
    GstInterPipelineVideoChannelClass))
#define GST_INTER_PIPELINE_VIDEO_CHANNEL_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_VIDEO_CHANNEL, \
    GstInterPipelineVideoChannelClass))
#define GST_INTER_PIPELINE_VIDEO_CHANNEL_CAST(obj) \
    ((GstInterPipelineVideoChannel *)(obj))
#define GST_IS_INTER_PIPELINE_VIDEO_CHANNEL(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_VIDEO_CHANNEL))
#define GST_IS_INTER_PIPELINE_VIDEO_CHANNEL_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_VIDEO_CHANNEL))


typedef struct _GstInterPipelineVideoChannel GstInterPipelineVideoChannel;
typedef struct _GstInterPipelineVideoChannelClass
    GstInterPipelineVideoChannelClass;


/* Video specific inter pipeline channel subclass. */
struct _GstInterPipelineVideoChannel
{
  GstInterPipelineChannel parent;

  /* All of these members must be accessed with the
   * channel payload_mutex locked. */

  /* Video info for frames that pass through the channel. */
  GstVideoInfo video_info;
  /* TRUE if video_info is set to a valid value. */
  gboolean video_info_valid;
  /* TRUE if video_info was updated since the last time
   * the consumer looked at it. (The consumer sets this
   * to FALSE once it has noticed the updated video info.) */
  gboolean video_info_updated;

  /* The current video frame that is being transmitted from
   * producer to consumer. If this is NULL, then the consumer
   * may substitute the missing frame with some placeholder,
   * like a black frame. */
  GstBuffer *video_frame;
  /* How many times the consumer pushed video_frame downstream
   * since video_frame was set by the producer. (Every time
   * the producer provides a new frame, it sets video_frame
   * to point to the new frame, and resets the value of
   * num_times_video_frame_pushed to 0.) */
  gsize num_times_video_frame_pushed;
};


struct _GstInterPipelineVideoChannelClass
{
  GstInterPipelineChannelClass parent_class;
};


GType gst_inter_pipeline_video_channel_get_type (void);


/* Returns a pointer to the global video channel table. */
GstInterPipelineChannelTable *gst_inter_pipeline_video_channel_table_get (void);


G_END_DECLS


#endif /* GSTINTERPIPELINEVIDEOCOMMON_H */
