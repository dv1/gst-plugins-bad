#include <gst/gst.h>
#include <gst/audio/audio.h>
#include "gstinterpipelinecommon.h"
#include "gstinterpipelineaudiocommon.h"
#include "gstinterpipelineaudiosrc.h"


GST_DEBUG_CATEGORY_STATIC (inter_pipeline_audio_src_debug);
#define GST_CAT_DEFAULT inter_pipeline_audio_src_debug


enum
{
  PROP_0,
  PROP_CHANNEL_NAME,
  PROP_CHANNEL_CAPACITY,
  PROP_CHANNEL_BASE_LATENCY,
  PROP_CHANNEL_PERIOD_LENGTH,
  PROP_ACTUAL_CHANNEL_CAPACITY,
  PROP_ACTUAL_CHANNEL_BASE_LATENCY,
  PROP_ACTUAL_CHANNEL_PERIOD_LENGTH,
  PROP_NULLSAMPLES_AS_GAP,
  PROP_FIXATION_CAPS
};


#define DEFAULT_CHANNEL_NAME  NULL
#define DEFAULT_OVERFLOW_MODE GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE
#define DEFAULT_NULLSAMPLES_AS_GAP TRUE

#define DEFAULT_FIXATION_CAPS \
    "audio/x-raw, " \
    "format = (string) S16LE, " \
    "rate = (int) 48000, " \
    "channels = (int) 2, " \
    "layout = (string) interleaved "


static GstStaticPadTemplate static_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_INTER_PIPELINE_AUDIO_CAPS));


#define GST_INTER_PIPELINE_AUDIO_SRC(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_AUDIO_SRC, \
    GstInterPipelineAudioSrc))
#define GST_INTER_PIPELINE_AUDIO_SRC_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_AUDIO_SRC, \
    GstInterPipelineAudioSrcClass))
#define GST_INTER_PIPELINE_AUDIO_SRC_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_AUDIO_SRC, \
    GstInterPipelineAudioSrcClass))
#define GST_INTER_PIPELINE_AUDIO_SRC_CAST(obj) \
    ((GstInterPipelineAudioSrc *)(obj))
#define GST_IS_INTER_PIPELINE_AUDIO_SRC(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_AUDIO_SRC))
#define GST_IS_INTER_PIPELINE_AUDIO_SRC_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_AUDIO_SRC))


typedef struct _GstInterPipelineAudioSrc GstInterPipelineAudioSrc;
typedef struct _GstInterPipelineAudioSrcClass GstInterPipelineAudioSrcClass;


struct _GstInterPipelineAudioSrc
{
  GstPushSrc parent;

  /* Value of channel-name property.
   * Must be accessed with OBJECT_LOCK held. */
  gchar *channel_name;
  /* Value of nullsamples-as-gap property.
   * Must be accessed with OBJECT_LOCK held. */
  gboolean nullsamples_as_gap;
  /* Value of fixation-caps property.
   * Must be accessed with OBJECT_LOCK held. */
  GstCaps *fixation_caps;

  /* These are local copies of the corresponding
   * members in GstInterPipelineAudioChannel.
   * These members exist here to make it possible
   * to set and store their values over GObject
   * properties even before the channel exists.
   * Must be accessed with OBJECT_LOCK held. */
  GstInterPipelineAudioChannelProperties channel_properties;
  GstInterPipelineAudioChannelProperties actual_channel_properties;

  /* The inter pipeline channel to use for transmitting audio data
   * to an interpipelineaudiosrc element. */
  GstInterPipelineAudioChannel *channel;

  /* Audio info computed from the negotiated src caps. */
  GstInterPipelineAudioInfo output_audio_info;

  /* A copy of the audio info from the pipeline channel.
   * We use a copy to avoid race conditions and/or overly long
   * mutex locks. (We copy the audio info while the channel's
   * mutex is locked. Later, the copy can be used even without
   * that mutex locked.) */
  GstInterPipelineAudioInfo cur_channel_audio_info;

  /* Audio info containing reference values that are used in the
   * fixate() vmethod. */
  GstInterPipelineAudioInfo fixation_audio_info;

  /* How many frames got pushed since the last time the srcpad's
   * caps were (re)negotiated. Used for timestamp generation. */
  gsize num_frames_pushed;
  /* Base offset for generated timestamps. This gets incremented
   * every time caps are (re)negotiated. */
  GstClockTime timestamp_base_offset;
};

struct _GstInterPipelineAudioSrcClass
{
  GstPushSrcClass parent_class;
};


G_DEFINE_TYPE (GstInterPipelineAudioSrc, gst_inter_pipeline_audio_src,
    GST_TYPE_PUSH_SRC);


static void gst_inter_pipeline_audio_src_dispose (GObject * object);
static void gst_inter_pipeline_audio_src_set_property (GObject * object,
    guint prop_id, GValue const *value, GParamSpec * pspec);
static void gst_inter_pipeline_audio_src_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static GstCaps *gst_inter_pipeline_audio_src_get_caps (GstBaseSrc *
    base_src, GstCaps * filter);
static gboolean gst_inter_pipeline_audio_src_set_caps (GstBaseSrc *
    base_src, GstCaps * caps);
static GstCaps *gst_inter_pipeline_audio_src_fixate (GstBaseSrc * base_src,
    GstCaps * caps);
static gboolean gst_inter_pipeline_audio_src_query (GstBaseSrc * base_src,
    GstQuery * query);
static gboolean gst_inter_pipeline_audio_src_negotiate (GstBaseSrc * base_src);
static gboolean gst_inter_pipeline_audio_src_start (GstBaseSrc * base_src);
static gboolean gst_inter_pipeline_audio_src_stop (GstBaseSrc * base_src);
static void gst_inter_pipeline_audio_src_get_times (GstBaseSrc * base_src,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);

static GstFlowReturn gst_inter_pipeline_audio_src_create (GstPushSrc *
    push_src, GstBuffer ** buffer);

static gboolean
gst_inter_pipeline_audio_src_update_fixation_states (GstInterPipelineAudioSrc *
    inter_pipeline_audio_src, const GstCaps * input_fixation_caps);




static void
gst_inter_pipeline_audio_src_class_init (GstInterPipelineAudioSrcClass * klass)
{
  GObjectClass *object_class;
  GstElementClass *element_class;
  GstBaseSrcClass *base_src_class;
  GstPushSrcClass *push_src_class;

  GST_DEBUG_CATEGORY_INIT (inter_pipeline_audio_src_debug,
      "interpipelineaudiosrc", 0, "inter pipeline audio source");

  object_class = G_OBJECT_CLASS (klass);
  element_class = GST_ELEMENT_CLASS (klass);
  base_src_class = GST_BASE_SRC_CLASS (klass);
  push_src_class = GST_PUSH_SRC_CLASS (klass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&static_src_template));

  object_class->dispose =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_dispose);
  object_class->set_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_set_property);
  object_class->get_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_get_property);

  base_src_class->get_caps =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_get_caps);
  base_src_class->set_caps =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_set_caps);
  base_src_class->fixate =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_fixate);
  base_src_class->query =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_query);
  base_src_class->negotiate =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_negotiate);
  base_src_class->start =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_start);
  base_src_class->stop = GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_stop);
  base_src_class->get_times =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_get_times);

  push_src_class->create =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_src_create);

  gst_element_class_set_static_metadata (element_class,
      "interpipelineaudiosrc",
      "Src/Audio",
      "Virtual audio source for transferring audio data between pipelines in the same process",
      "Carlos Rafael Giani <crg7475@mailbox.org>");

  g_object_class_install_property (object_class,
      PROP_CHANNEL_NAME,
      g_param_spec_string ("channel-name",
          "Channel name",
          "Unique name of transmission channel between this source and a corresponding sink",
          DEFAULT_CHANNEL_NAME, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_CHANNEL_CAPACITY,
      g_param_spec_uint64 ("channel-capacity",
          "Channel capacity",
          "How much data the channel can maximally hold, in nanoseconds",
          0, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_CAPACITY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_CHANNEL_BASE_LATENCY,
      g_param_spec_uint64 ("base-latency",
          "Base latency",
          "The base latency that is always present, in nanoseconds",
          0, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_BASE_LATENCY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_CHANNEL_PERIOD_LENGTH,
      g_param_spec_uint64 ("period-length",
          "Period length",
          "Length of one period, in nanoseconds; transmissions from sink to source are performed in period units",
          0, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_ACTUAL_CHANNEL_CAPACITY,
      g_param_spec_uint64 ("actual-channel-capacity",
          "Actual channel capacity",
          "Actual channel capacity that is being used, in nanoseconds",
          1, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_CAPACITY,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_ACTUAL_CHANNEL_BASE_LATENCY,
      g_param_spec_uint64 ("actual-base-latency",
          "Actual base latency",
          "Actual base latency that is being used, in nanoseconds",
          1, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_BASE_LATENCY,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_ACTUAL_CHANNEL_PERIOD_LENGTH,
      g_param_spec_uint64 ("actual-period-length",
          "Actual period length",
          "Actual period length that is being used, in nanoseconds",
          1, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_NULLSAMPLES_AS_GAP,
      g_param_spec_boolean ("nullsamples-as-gap",
          "Nullsamples as gap",
          "Set the gap flag in buffers that contain extra filler nullsamples (added if a buffer does not contain a full period length's worth of audio data)",
          DEFAULT_NULLSAMPLES_AS_GAP,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_FIXATION_CAPS,
      g_param_spec_boxed ("fixation-caps",
          "Fixation caps",
          "Fixed caps to use as reference during the fixation process",
          GST_TYPE_CAPS, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
}


static void
gst_inter_pipeline_audio_src_init (GstInterPipelineAudioSrc *
    inter_pipeline_audio_src)
{
  gboolean fixation_states_set_up;
  GstCaps *initial_fixation_caps;

  gst_base_src_set_format (GST_BASE_SRC (inter_pipeline_audio_src),
      GST_FORMAT_TIME);
  gst_base_src_set_live (GST_BASE_SRC (inter_pipeline_audio_src), TRUE);

  inter_pipeline_audio_src->channel_name = g_strdup (DEFAULT_CHANNEL_NAME);
  inter_pipeline_audio_src->nullsamples_as_gap = DEFAULT_NULLSAMPLES_AS_GAP;

  inter_pipeline_audio_src->channel_properties.capacity_in_ns =
      DEFAULT_AUDIO_CHANNEL_CAPACITY;
  inter_pipeline_audio_src->channel_properties.base_latency_in_ns =
      DEFAULT_AUDIO_CHANNEL_BASE_LATENCY;
  inter_pipeline_audio_src->channel_properties.period_length_in_ns =
      DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH;

  inter_pipeline_audio_src->actual_channel_properties =
      inter_pipeline_audio_src->channel_properties;

  inter_pipeline_audio_src->channel = NULL;
  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_src->output_audio_info));

  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_src->cur_channel_audio_info));

  inter_pipeline_audio_src->num_frames_pushed = 0;
  inter_pipeline_audio_src->timestamp_base_offset = 0;

  /* Fill fixation_caps and fixation_audio_info to
   * values from the hardcoded DEFAULT_FIXATION_CAPS. */
  initial_fixation_caps = gst_caps_from_string (DEFAULT_FIXATION_CAPS);
  g_assert (initial_fixation_caps != NULL);
  fixation_states_set_up =
      gst_inter_pipeline_audio_src_update_fixation_states
      (inter_pipeline_audio_src, initial_fixation_caps);
  gst_caps_unref (initial_fixation_caps);
  g_assert (fixation_states_set_up);
}


static void
gst_inter_pipeline_audio_src_dispose (GObject * object)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (object);

  g_free (inter_pipeline_audio_src->channel_name);
  gst_caps_replace (&(inter_pipeline_audio_src->fixation_caps), NULL);

  G_OBJECT_CLASS (gst_inter_pipeline_audio_src_parent_class)->dispose (object);
}


static void
gst_inter_pipeline_audio_src_set_property (GObject * object, guint prop_id,
    GValue const *value, GParamSpec * pspec)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_free (inter_pipeline_audio_src->channel_name);
      inter_pipeline_audio_src->channel_name = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_CHANNEL_CAPACITY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      inter_pipeline_audio_src->channel_properties.capacity_in_ns =
          g_value_get_uint64 (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_CHANNEL_BASE_LATENCY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      inter_pipeline_audio_src->channel_properties.base_latency_in_ns =
          g_value_get_uint64 (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_CHANNEL_PERIOD_LENGTH:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      inter_pipeline_audio_src->channel_properties.period_length_in_ns =
          g_value_get_uint64 (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_NULLSAMPLES_AS_GAP:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      inter_pipeline_audio_src->nullsamples_as_gap =
          g_value_get_boolean (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_FIXATION_CAPS:
    {
      gboolean ret;
      GstCaps *caps = GST_CAPS_CAST (g_value_get_boxed (value));

      if (caps == NULL) {
        GST_ELEMENT_ERROR (inter_pipeline_audio_src, STREAM, FORMAT,
            ("Cannot use NULL as fixation caps"), (NULL));
        break;
      }

      if (gst_caps_is_empty (caps)) {
        GST_ELEMENT_ERROR (inter_pipeline_audio_src, STREAM, FORMAT,
            ("Cannot use empty caps as fixation caps"), (NULL));
        break;
      }

      if (!gst_caps_is_fixed (caps)) {
        GST_ELEMENT_ERROR (inter_pipeline_audio_src, STREAM, FORMAT,
            ("Fixation caps must themselves also be fixed"),
            ("provided caps: %" GST_PTR_FORMAT, (gpointer) caps));
        break;
      }

      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      /* Update fixation_caps and fixation_audio_info to
       * the values provided here over the GObject property. */
      ret =
          gst_inter_pipeline_audio_src_update_fixation_states
          (inter_pipeline_audio_src, caps);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);

      if (!ret) {
        GST_ELEMENT_ERROR (inter_pipeline_audio_src, STREAM, FORMAT,
            ("Could not use provided fixation caps"),
            ("provided caps: %" GST_PTR_FORMAT, (gpointer) caps));
        break;
      }

      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_inter_pipeline_audio_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_string (value, inter_pipeline_audio_src->channel_name);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_CHANNEL_CAPACITY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_uint64 (value,
          inter_pipeline_audio_src->channel_properties.capacity_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_CHANNEL_BASE_LATENCY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_uint64 (value,
          inter_pipeline_audio_src->channel_properties.base_latency_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_CHANNEL_PERIOD_LENGTH:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_uint64 (value,
          inter_pipeline_audio_src->channel_properties.period_length_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_ACTUAL_CHANNEL_CAPACITY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_uint64 (value,
          inter_pipeline_audio_src->actual_channel_properties.capacity_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_ACTUAL_CHANNEL_BASE_LATENCY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_uint64 (value,
          inter_pipeline_audio_src->actual_channel_properties.
          base_latency_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_ACTUAL_CHANNEL_PERIOD_LENGTH:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_uint64 (value,
          inter_pipeline_audio_src->actual_channel_properties.
          period_length_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_NULLSAMPLES_AS_GAP:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      g_value_set_boolean (value, inter_pipeline_audio_src->nullsamples_as_gap);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    case PROP_FIXATION_CAPS:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_src);
      gst_caps_ref (inter_pipeline_audio_src->fixation_caps);
      g_value_take_boxed (value, inter_pipeline_audio_src->fixation_caps);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_src);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static GstCaps *
gst_inter_pipeline_audio_src_get_caps (GstBaseSrc * base_src, GstCaps * filter)
{
  GstCaps *caps = NULL;
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);

  if (inter_pipeline_audio_src->channel == NULL) {
    /* If we haven't gotten a channel (because start() was not yet called),
     * just call the base class' get_caps() vfunction, since we cannot
     * do anything else without information from channel. */

    return
        GST_BASE_SRC_CLASS (gst_inter_pipeline_audio_src_parent_class)->get_caps
        (base_src, filter);
  }

  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_src->channel);

  /* If we already have a channel, use its audio info to generate caps.
   * get_caps() is called to report to other elements what caps we
   * can handle (by using a caps query). And the caps we can handle are
   * defined by what formats the channel can handle.
   * If however the channel does not (yet) have a valid audio info,
   * then we cannot generate any caps. In that case, fall back to
   * default base class behavior (done at the end of the function). */
  if (inter_pipeline_audio_src->channel->audio_info.valid) {
    caps =
        gst_inter_pipeline_audio_info_to_caps (&
        (inter_pipeline_audio_src->channel->audio_info));
    g_assert (caps != NULL);

    /* Apply filter caps if present. */
    if (filter != NULL) {
      GstCaps *tmp;

      tmp = gst_caps_intersect_full (filter, caps, GST_CAPS_INTERSECT_FIRST);
      gst_caps_unref (caps);
      caps = tmp;
    }
  }

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_src->channel);

  if (caps != NULL)
    return caps;
  else
    return
        GST_BASE_SRC_CLASS (gst_inter_pipeline_audio_src_parent_class)->get_caps
        (base_src, filter);
}


static gboolean
gst_inter_pipeline_audio_src_set_caps (GstBaseSrc * base_src, GstCaps * caps)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);

  /* Set output_audio_info to the new output caps. */

  if (!gst_inter_pipeline_audio_info_from_caps (&
          (inter_pipeline_audio_src->output_audio_info), caps)) {
    GST_ERROR_OBJECT (inter_pipeline_audio_src,
        "could not parse audio caps %" GST_PTR_FORMAT, (gpointer) caps);
    return FALSE;
  }

  return TRUE;
}


static GstCaps *
gst_inter_pipeline_audio_src_fixate (GstBaseSrc * base_src, GstCaps * caps)
{
  /* In here, we fixate unfixed caps to the reference values from
   * fixation_audio_info. That audio info is configurable over the
   * fixation-caps GObject property. */

  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);
  GstStructure *structure;
  const GstInterPipelineAudioInfo *ref_audio_info =
      &(inter_pipeline_audio_src->fixation_audio_info);

  GST_DEBUG_OBJECT (inter_pipeline_audio_src,
      "caps before fixation: %" GST_PTR_FORMAT, (gpointer) caps);


  /* First, make the caps writable and truncate it (= get rid of all but the
   * first GstStructure in the caps). */
  caps = gst_caps_make_writable (caps);
  caps = gst_caps_truncate (caps);
  structure = gst_caps_get_structure (caps, 0);


  /* Next, fixate specific fields if they aren't fixed already. These fields
   * are fixated to reference values from fixation_audio_info. If these
   * fields are not present, they are set to the reference values.*/

#define FIXATE_FIELD_TO_REFERENCE_VALUE(FIELD_NAME, FIXATE_TYPE, FIELD_GTYPE, ...) \
  G_STMT_START \
    if (gst_structure_has_field(structure, (FIELD_NAME))) \
      gst_structure_fixate_field_ ##FIXATE_TYPE (structure, (FIELD_NAME), \
          __VA_ARGS__); \
    else \
      gst_structure_set(structure, (FIELD_NAME), FIELD_GTYPE, __VA_ARGS__, \
          NULL); \
  G_STMT_END

  switch (ref_audio_info->type) {
    case GST_INTER_PIPELINE_AUDIO_TYPE_PCM:
    {
      const GstAudioInfo *pcm_info = &(ref_audio_info->details.pcm_details);

      FIXATE_FIELD_TO_REFERENCE_VALUE ("format", string, G_TYPE_STRING,
          gst_audio_format_to_string (GST_AUDIO_INFO_FORMAT (pcm_info)));
      FIXATE_FIELD_TO_REFERENCE_VALUE ("rate", nearest_int, G_TYPE_INT,
          GST_AUDIO_INFO_RATE (pcm_info));
      FIXATE_FIELD_TO_REFERENCE_VALUE ("channels", nearest_int, G_TYPE_INT,
          GST_AUDIO_INFO_CHANNELS (pcm_info));
      FIXATE_FIELD_TO_REFERENCE_VALUE ("layout", string, G_TYPE_STRING,
          (GST_AUDIO_INFO_LAYOUT (pcm_info) ==
              GST_AUDIO_LAYOUT_INTERLEAVED) ? "interleaved" :
          "non-interleaved");

      if (!gst_structure_has_field (structure, "channel-mask")) {
        gboolean ok;
        gint num_channels;
        guint64 channel_mask;

        ok = gst_structure_get_int (structure, "channels", &num_channels);
        g_assert (ok);

        /* Set the channel mask, unless this is mono data
         * (channel masks make no sense with mono). */
        if (num_channels > 1) {
          if (!gst_audio_channel_positions_to_mask (&(GST_AUDIO_INFO_POSITION
                      (pcm_info, 0)), num_channels, FALSE, &channel_mask))
            channel_mask = gst_audio_channel_get_fallback_mask (num_channels);

          gst_structure_set (structure, "channel-mask", GST_TYPE_BITMASK,
              channel_mask, NULL);
        }
      }

      break;
    }

    case GST_INTER_PIPELINE_AUDIO_TYPE_MULAW:
    case GST_INTER_PIPELINE_AUDIO_TYPE_ALAW:
    {
      FIXATE_FIELD_TO_REFERENCE_VALUE ("rate", nearest_int, G_TYPE_INT,
          ref_audio_info->details.a_mu_law_details.sample_rate);
      FIXATE_FIELD_TO_REFERENCE_VALUE ("channels", nearest_int, G_TYPE_INT,
          ref_audio_info->details.a_mu_law_details.num_channels);

      /* Set the channel mask, unless this is mono data
       * (channel masks make no sense with mono). */
      if (ref_audio_info->details.a_mu_law_details.num_channels > 1) {
        if (!gst_structure_has_field (structure, "channel-mask"))
          gst_structure_set (structure, "channel-mask", GST_TYPE_BITMASK,
              ref_audio_info->details.a_mu_law_details.channel_mask, NULL);
      }

      break;
    }

    default:
      g_assert_not_reached ();
  }

#undef FIXATE_FIELD_TO_REFERENCE_VALUE

  /* Fixate any remaining unfixed values by using the default method. */
  gst_structure_fixate (structure);

  GST_DEBUG_OBJECT (inter_pipeline_audio_src,
      "caps after fixation: %" GST_PTR_FORMAT, (gpointer) caps);
  return caps;
}


static gboolean
gst_inter_pipeline_audio_src_query (GstBaseSrc * base_src, GstQuery * query)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);
  gboolean ret;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_LATENCY:
    {
      GstClockTime base_latency, capacity;

      GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_src->channel);
      capacity =
          GST_INTER_PIPELINE_AUDIO_CHANNEL (inter_pipeline_audio_src->channel)->
          properties.capacity_in_ns;
      base_latency =
          GST_INTER_PIPELINE_AUDIO_CHANNEL (inter_pipeline_audio_src->channel)->
          properties.base_latency_in_ns;
      GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_src->channel);

      GST_DEBUG_OBJECT (inter_pipeline_audio_src,
          "reporting channel base-latency %" GST_TIME_FORMAT
          " as min latency and channel capacity %" GST_TIME_FORMAT
          " as max latency", GST_TIME_ARGS (base_latency),
          GST_TIME_ARGS (capacity)
          );

      gst_query_set_latency (query, gst_base_src_is_live (base_src),
          base_latency, capacity);

      ret = TRUE;

      break;
    }

    default:
      ret =
          GST_BASE_SRC_CLASS (gst_inter_pipeline_audio_src_parent_class)->query
          (base_src, query);
  }

  return ret;
}


static gboolean
gst_inter_pipeline_audio_src_negotiate (GstBaseSrc * base_src)
{
  GstCaps *channel_caps;
  GstCaps *downstream_caps;
  GstCaps *negotiated_caps;
  gboolean ret = TRUE;
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);

  /* If output_audio_info was set earlier (in the set_caps() vmethod),
   * and we are here again, it means we are _re_negotiating the output
   * caps. As such, we need to update the timestamp_base_offset, since
   * we might continue output with a different frame rate (depending on
   * whatever downstream requires). */
  if (inter_pipeline_audio_src->output_audio_info.valid) {
    guint sample_rate =
        gst_inter_pipeline_audio_info_get_sample_rate (&
        (inter_pipeline_audio_src->output_audio_info));

    inter_pipeline_audio_src->timestamp_base_offset +=
        gst_util_uint64_scale (inter_pipeline_audio_src->num_frames_pushed,
        GST_SECOND, sample_rate);
  }

  /* After updating timestamp_base_offset, we must set num_frames_pushed to
   * 0, since its previous value was factored into timestamp_base_offset
   * during its update. */
  inter_pipeline_audio_src->num_frames_pushed = 0;

  /* If the pipeline channel currently has no valid audio info,
   * just let the base class perform regular negotiation and exit.
   * (The channel's audio info is checked in the create() vmethod.) */
  if (!inter_pipeline_audio_src->cur_channel_audio_info.valid)
    return GST_BASE_SRC_CLASS
        (gst_inter_pipeline_audio_src_parent_class)->negotiate (base_src);


  /* At this point, it is clear that (a) timestamp_base_offset was updated
   * if needed and (b) there is a valid audio info in the pipeline channel.
   * We use that audio info, along with the allowed caps from downstream,
   * to negotiate caps that satisfy all requirements from the channel and
   * from downstream. */


  /* Convert the channel's audio info to caps, for further caps negotiation.
   * Remove its framerate and max-framerate fields though, since we do
   * retimestamp output frames based on the framerate downstream requires. */
  channel_caps =
      gst_inter_pipeline_audio_info_to_caps
      (&inter_pipeline_audio_src->cur_channel_audio_info);
  if (G_UNLIKELY (channel_caps == NULL)) {
    GST_ERROR_OBJECT (inter_pipeline_audio_src,
        "could not produce caps to negotiate with");
    return FALSE;
  }

  /* Query the caps downstream allows, for further caps negotiation.
   * In the caps query, we specify the channel_caps as filter. */
  {
    GstQuery *query;

    /* NOTE: We could in theory use gst_pad_get_allowed_caps() to achieve
     * the same result. However, that function leads to our get_caps()
     * vmethod being called, which locks the pipeline channel's mutex.
     * And, when negotiate() is called, this mutex may already be locked
     * (for example because of the gst_base_src_negotiate() call in the
     * create() vmethod), so we'd end up deadlocked. */

    downstream_caps = NULL;

    query = gst_query_new_caps (channel_caps);

    if (!gst_pad_peer_query (GST_BASE_SRC_PAD (base_src), query)) {
      GST_ERROR_OBJECT (inter_pipeline_audio_src,
          "could not query peer pad's caps");
      goto query_done;
    }

    gst_query_parse_caps_result (query, &downstream_caps);
    if (downstream_caps == NULL) {
      GST_ERROR_OBJECT (inter_pipeline_audio_src, "peer has no caps");
      goto query_done;
    }
    gst_caps_ref (downstream_caps);

    GST_DEBUG_OBJECT (inter_pipeline_audio_src,
        "downstream caps: %" GST_PTR_FORMAT, (gpointer) downstream_caps);

  query_done:
    gst_query_unref (query);

    if (downstream_caps == NULL)
      return FALSE;
  }


  /* The final negotiated caps are the intersection between the channel caps
   * (= channel's audio info in caps form) and the caps downstream allows. */
  negotiated_caps =
      gst_caps_intersect_full (downstream_caps, channel_caps,
      GST_CAPS_INTERSECT_FIRST);

  if (gst_caps_is_empty (negotiated_caps)) {
    GST_ERROR_OBJECT (base_src,
        "failed to negotiate src caps (using channel caps %" GST_PTR_FORMAT ")",
        (gpointer) channel_caps);
    ret = GST_FLOW_NOT_NEGOTIATED;
  }

  GST_DEBUG_OBJECT (inter_pipeline_audio_src,
      "negotiated src caps: %" GST_PTR_FORMAT, (gpointer) negotiated_caps);


  /* Fixate the negotiated caps before we output them. */
  negotiated_caps =
      gst_inter_pipeline_audio_src_fixate (base_src, negotiated_caps);
  GST_DEBUG_OBJECT (inter_pipeline_audio_src, "fixated to: %" GST_PTR_FORMAT,
      (gpointer) negotiated_caps);


  /* Negotiation completed. Output the negotiated caps, cleanup, and exit. */

  ret = gst_base_src_set_caps (base_src, negotiated_caps);

  gst_caps_unref (negotiated_caps);
  gst_caps_unref (channel_caps);
  gst_caps_unref (downstream_caps);

  return ret;
}


static gboolean
gst_inter_pipeline_audio_src_start (GstBaseSrc * base_src)
{
  gchar *channel_name;
  GstInterPipelineAudioChannelProperties channel_properties;
  gboolean ret = TRUE;
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);


  /* Make copies of channel_name and channel_properties to avoid
   * race conditions and having to hold the object lock throughout
   * this entire function. */
  GST_OBJECT_LOCK (inter_pipeline_audio_src);
  channel_name = g_strdup (inter_pipeline_audio_src->channel_name);
  channel_properties = inter_pipeline_audio_src->channel_properties;
  GST_OBJECT_UNLOCK (inter_pipeline_audio_src);


  /* Perform some sanity checks. */

  if ((channel_name == NULL) || (channel_name[0] == '\0')) {
    GST_ELEMENT_ERROR (inter_pipeline_audio_src, RESOURCE, NOT_FOUND,
        ("Channel name is NULL or an empty string."), (NULL));
    ret = FALSE;
    goto finish;
  }

  if (channel_properties.capacity_in_ns <
      channel_properties.period_length_in_ns) {
    GST_ELEMENT_ERROR (inter_pipeline_audio_src, RESOURCE, NOT_FOUND,
        ("Channel capacity is set to a value greater than period length."),
        ("channel capacity: %" GST_TIME_FORMAT "  period length: %"
            GST_TIME_FORMAT, GST_TIME_ARGS (channel_properties.capacity_in_ns),
            GST_TIME_ARGS (channel_properties.period_length_in_ns)));
    ret = FALSE;
    goto finish;
  }


  /* Initialize structures and values. */

  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_src->output_audio_info));
  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_src->cur_channel_audio_info));

  inter_pipeline_audio_src->num_frames_pushed = 0;
  inter_pipeline_audio_src->timestamp_base_offset = 0;


  /* Now get the channel with the specified name. */

  GST_DEBUG_OBJECT (inter_pipeline_audio_src,
      "getting audio channel \"%s\" (gets created if it does not exist yet)",
      channel_name);

  inter_pipeline_audio_src->channel =
      GST_INTER_PIPELINE_AUDIO_CHANNEL
      (gst_inter_pipeline_channel_table_get_channel
      (gst_inter_pipeline_audio_channel_table_get (),
          GST_ELEMENT_CAST (base_src), FALSE, channel_name, &channel_properties)
      );

  if (G_UNLIKELY (inter_pipeline_audio_src->channel == NULL)) {
    GST_ERROR_OBJECT (inter_pipeline_audio_src, "could not get audio channel");
    ret = FALSE;
  }


  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_src->channel);

  /* Get a copy of the channel properties that are actually in use.
   * These might be different from the ones we supplied to the get_channel()
   * if the channel already existed (its properties are set only once, at
   * the time of the channel's creation). To be able to query the actual
   * properties via the actual-* GObject properties, make a copy. */
  GST_OBJECT_LOCK (inter_pipeline_audio_src);
  inter_pipeline_audio_src->actual_channel_properties =
      inter_pipeline_audio_src->channel->properties;
  GST_OBJECT_UNLOCK (inter_pipeline_audio_src);

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_src->channel);


  GST_TRACE_OBJECT (inter_pipeline_audio_src,
      "inter pipeline audio source started");

finish:
  g_free (channel_name);
  return ret;
}


static gboolean
gst_inter_pipeline_audio_src_stop (GstBaseSrc * base_src)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);

  /* The channel's finalize function takes care of removing the
   * channel from the table if the refcount reaches 0. We do it
   * this way, since the sink element at the other end may still
   * be holding a reference to the channel, so removing it here
   * from the table would not be correct. */
  if (G_LIKELY (inter_pipeline_audio_src->channel != NULL)) {
    GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_src->channel);
    gst_adapter_clear (inter_pipeline_audio_src->channel->audio_adapter);
    GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_src->channel);

    GST_DEBUG_OBJECT (inter_pipeline_audio_src,
        "unref'ing audio channel \"%s\"",
        GST_INTER_PIPELINE_CHANNEL_NAME (inter_pipeline_audio_src->channel));
    gst_inter_pipeline_channel_unref (GST_INTER_PIPELINE_CHANNEL_CAST
        (inter_pipeline_audio_src->channel), GST_ELEMENT_CAST (base_src));
    inter_pipeline_audio_src->channel = NULL;
  } else
    GST_DEBUG_OBJECT (inter_pipeline_audio_src,
        "there is no audio channel; nothing to unref");

  GST_TRACE_OBJECT (inter_pipeline_audio_src,
      "inter pipeline audio source stopped");

  return TRUE;
}


static void
gst_inter_pipeline_audio_src_get_times (GstBaseSrc * base_src,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end)
{
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC (base_src);

  /* Compute valid start / end timestamps based on the information we have.
   * If there is no valid timestamp data, we cannot compute anything,
   * and start & end are set to GST_CLOCK_TIME_NONE.
   * If there is a valid buffer timestamp, we can assign its value to start,
   * and compute end either by using the buffer's duration (if there is one),
   * or assume that the buffer duration implicitely corresponds to the
   * size of the buffer (which is translated to a duration by using the
   * sample rate and the number of bytes per frame (BPF)). */

  if (GST_BUFFER_PTS_IS_VALID (buffer)) {
    GstClockTime pts = GST_BUFFER_PTS (buffer);

    *start = pts;

    if (GST_BUFFER_DURATION_IS_VALID (buffer)) {
      *end = pts + GST_BUFFER_DURATION (buffer);
      GST_LOG_OBJECT (inter_pipeline_audio_src,
          "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
          GST_TIME_FORMAT, (gpointer) buffer, GST_TIME_ARGS (*start),
          GST_TIME_ARGS (*end));
    } else {
      guint sample_rate =
          gst_inter_pipeline_audio_info_get_sample_rate (&
          (inter_pipeline_audio_src->output_audio_info));
      guint bpf =
          gst_inter_pipeline_audio_info_get_bpf (&
          (inter_pipeline_audio_src->output_audio_info));

      if (sample_rate > 0)
        *end =
            pts + gst_util_uint64_scale_int (gst_buffer_get_size (buffer),
            GST_SECOND, sample_rate * bpf);

      GST_LOG_OBJECT (inter_pipeline_audio_src,
          "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
          GST_TIME_FORMAT " based on a sample rate of %u Hz and %u bpf",
          (gpointer) buffer, GST_TIME_ARGS (*start), GST_TIME_ARGS (*end),
          sample_rate, bpf);
    }

    return;
  }

  GST_LOG_OBJECT (inter_pipeline_audio_src,
      "no start-end times for given buffer %p specified since buffer has no PTS",
      (gpointer) buffer);

  *start = *end = GST_CLOCK_TIME_NONE;
}


static GstFlowReturn
gst_inter_pipeline_audio_src_create (GstPushSrc * push_src, GstBuffer ** buffer)
{
  GstFlowReturn flow_ret = GST_FLOW_OK;
  GstBuffer *audio_data = NULL;
  gboolean is_gap = FALSE;
  GstAllocator *allocator = NULL;
  GstAllocationParams alloc_params;
  gboolean nullsamples_as_gap;
  guint bpf, sample_rate, num_frames_to_take;
  guint64 period_length_in_frames;
  const GstInterPipelineAudioChannelProperties *properties;
  GstInterPipelineAudioSrc *inter_pipeline_audio_src =
      GST_INTER_PIPELINE_AUDIO_SRC_CAST (push_src);

  GST_LOG_OBJECT (inter_pipeline_audio_src,
      "about to push audio data downstream");


  /* Copy GObject property values to avoid race conditions and having
   * to hold the object lock throughout this entire function. */
  GST_OBJECT_LOCK (inter_pipeline_audio_src);
  nullsamples_as_gap = inter_pipeline_audio_src->nullsamples_as_gap;
  GST_OBJECT_UNLOCK (inter_pipeline_audio_src);


  /* Lock the pipeline channel's mutex since we need to access the channel's
   * audio info, audio data, etc. */
  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_src->channel);

  /* If the channel's audio info was updated since the last time we looked
   * at it, we must renegotiate the output caps to be able to continue
   * output the audio data that arrives over the channel. */
  if (inter_pipeline_audio_src->channel->audio_info.updated) {
    GST_DEBUG_OBJECT (inter_pipeline_audio_src,
        "channel's audio info got updated; need to renegotiate");

    /* Make a copy of the channel's audio info for use in
     * gst_inter_pipeline_audio_src_negotiate(). */
    gst_inter_pipeline_audio_info_copy (&
        (inter_pipeline_audio_src->cur_channel_audio_info),
        &(inter_pipeline_audio_src->channel->audio_info));

    /* Trigger renegotiation. This will eventually call our
     * gst_inter_pipeline_audio_src_negotiate() function. */
    if (!gst_base_src_negotiate (GST_BASE_SRC_CAST (push_src))) {
      GST_ERROR_OBJECT (inter_pipeline_audio_src, "could not renegotiate");
      GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_src->channel);
      flow_ret = GST_FLOW_NOT_NEGOTIATED;
      goto error;
    }

    /* Clear the updated flag so we don't renegotiate
     * again the next time create() is called (unless the audio info
     * really was updated again). */
    inter_pipeline_audio_src->channel->audio_info.updated = FALSE;
  }


  properties = &(inter_pipeline_audio_src->channel->properties);
  period_length_in_frames = properties->period_length_in_frames;

  bpf =
      gst_inter_pipeline_audio_info_get_bpf (&
      (inter_pipeline_audio_src->channel->audio_info));
  g_assert (bpf > 0);

  sample_rate =
      gst_inter_pipeline_audio_info_get_sample_rate (&
      (inter_pipeline_audio_src->channel->audio_info));
  g_assert (sample_rate > 0);


  /* Retrieve frames from the channel's adapter.
   * We take up to a period's length of data. */

  num_frames_to_take =
      gst_adapter_available (inter_pipeline_audio_src->channel->audio_adapter) /
      bpf;

  if (num_frames_to_take > period_length_in_frames)
    num_frames_to_take = period_length_in_frames;

  if (num_frames_to_take > 0) {
    audio_data =
        gst_adapter_take_buffer (inter_pipeline_audio_src->channel->
        audio_adapter, num_frames_to_take * bpf);
  } else
    audio_data = gst_buffer_new ();


  /* If the interpipelineaudiosink at the other end of the channel is waiting,
   * wake it up here, since at this point, we have taken audio data from
   * the channel, so there's newly freed room in there for the sink to use. */
  GST_INTER_PIPELINE_CHANNEL_SIGNAL_COND (inter_pipeline_audio_src->channel);

  /* We no longer need to access the channel, so unlock its mutex. */
  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_src->channel);


  /* Fill the buffer with nullsamples if it holds less than a period
   * length's worth of data. */

  audio_data = gst_buffer_make_writable (audio_data);

  if (num_frames_to_take < period_length_in_frames) {
    GstMemory *memory;
    GstMapInfo map_info;

    /* For allocating the black frame and any temporary frames, use the
     * allocator that was picked during the caps negotiation phase. */
    gst_base_src_get_allocator (GST_BASE_SRC (inter_pipeline_audio_src),
        &allocator, &alloc_params);

    memory =
        gst_allocator_alloc (allocator,
        (period_length_in_frames - num_frames_to_take) * bpf, &alloc_params);
    if (G_UNLIKELY (memory == NULL)) {
      GST_ERROR_OBJECT (inter_pipeline_audio_src,
          "could not allocate nullsample memory block");
      flow_ret = GST_FLOW_ERROR;
      goto error;
    }

    if (G_UNLIKELY (!gst_memory_map (memory, &map_info, GST_MAP_WRITE))) {
      GST_ERROR_OBJECT (inter_pipeline_audio_src,
          "could not map nullsample memory block");
      flow_ret = GST_FLOW_ERROR;
      goto error;
    }

    gst_inter_pipeline_audio_fill_silence (&
        (inter_pipeline_audio_src->output_audio_info), map_info.data,
        map_info.size / bpf);

    gst_memory_unmap (memory, &map_info);

    gst_buffer_append_memory (audio_data, memory);

    is_gap = nullsamples_as_gap;
  }

  num_frames_to_take = period_length_in_frames;


  /* At this point, we retrieved at most period length's worth of data
   * from the channel's audio_adapter, added nullsamples to the retrieved
   * buffer if necessary, and determined whether or not to mark the output
   * as a gap. Set the metadata of the outgoing GstBuffer (which contains
   * the audio data), including the gap flag, timestamps etc. */


  {
    GstClockTime pts, pts_end;

    if (is_gap)
      GST_BUFFER_FLAG_SET (audio_data, GST_BUFFER_FLAG_GAP);

    pts =
        inter_pipeline_audio_src->timestamp_base_offset +
        gst_util_uint64_scale (inter_pipeline_audio_src->num_frames_pushed,
        GST_SECOND, sample_rate);
    pts_end =
        inter_pipeline_audio_src->timestamp_base_offset +
        gst_util_uint64_scale (inter_pipeline_audio_src->num_frames_pushed +
        num_frames_to_take, GST_SECOND, sample_rate);

    GST_BUFFER_DTS (audio_data) = GST_CLOCK_TIME_NONE;
    GST_BUFFER_PTS (audio_data) = pts;
    GST_BUFFER_DURATION (audio_data) = pts_end - pts;

    GST_BUFFER_OFFSET (audio_data) =
        inter_pipeline_audio_src->num_frames_pushed;
    GST_BUFFER_OFFSET_END (audio_data) =
        inter_pipeline_audio_src->num_frames_pushed + num_frames_to_take;

    /* Mark the first frame post-(re)negotiation as DISCONT. */
    if (inter_pipeline_audio_src->num_frames_pushed == 0)
      GST_BUFFER_FLAG_SET (audio_data, GST_BUFFER_FLAG_DISCONT);
    else
      GST_BUFFER_FLAG_UNSET (audio_data, GST_BUFFER_FLAG_DISCONT);

    GST_LOG_OBJECT (inter_pipeline_audio_src,
        "pushing audio data gstbuffer downstream: %" GST_PTR_FORMAT,
        (gpointer) audio_data);

    /* Increment the num_frames_pushed counter for PTS calculation. */
    inter_pipeline_audio_src->num_frames_pushed += num_frames_to_take;
  }


  /* We are done. */
finish:
  if (allocator != NULL)
    gst_object_unref (GST_OBJECT (allocator));

  *buffer = audio_data;
  return flow_ret;

error:
  gst_buffer_replace (&audio_data, NULL);
  goto finish;
}


static gboolean
gst_inter_pipeline_audio_src_update_fixation_states (GstInterPipelineAudioSrc *
    inter_pipeline_audio_src, const GstCaps * input_fixation_caps)
{
  /* The "fixation states" are fixation_caps and fixation_audio_info.
   * We set them to the values from input_fixation_caps here. */

  GstInterPipelineAudioInfo new_fixation_audio_info;
  GstCaps *new_fixation_caps;

  /* First, convert caps to audio info. This sets various defaults in the
   * audio info if the caps do not contain the corresponding values. */
  if (!gst_inter_pipeline_audio_info_from_caps (&new_fixation_audio_info,
          input_fixation_caps)) {
    GST_ERROR_OBJECT (inter_pipeline_audio_src,
        "could not convert input fixation caps %" GST_PTR_FORMAT
        " to a audio info; are these valid audio caps?",
        (gpointer) input_fixation_caps);
    return FALSE;
  }

  /* Next, convert the audio info back to caps, containing defaults that
   * may not have been set in the initial caps. This makes it possible
   * for the user to see these defaults by getting a GstCaps value from
   * the fixation-caps property. */
  new_fixation_caps =
      gst_inter_pipeline_audio_info_to_caps (&new_fixation_audio_info);
  g_return_val_if_fail (new_fixation_caps != NULL, FALSE);

  /* Now set the new audio info & the new caps as the new fixation states. */
  gst_caps_replace (&(inter_pipeline_audio_src->fixation_caps), NULL);
  inter_pipeline_audio_src->fixation_caps = new_fixation_caps;
  inter_pipeline_audio_src->fixation_audio_info = new_fixation_audio_info;

  return TRUE;
}
