#include <gst/gst.h>
#include <gst/video/video.h>
#include "gstinterpipelinecommon.h"
#include "gstinterpipelinevideocommon.h"
#include "gstinterpipelinevideosink.h"


GST_DEBUG_CATEGORY_STATIC (inter_pipeline_video_sink_debug);
#define GST_CAT_DEFAULT inter_pipeline_video_sink_debug


enum
{
  PROP_0,
  PROP_CHANNEL_NAME,
  PROP_OVERFLOW_MODE
};


#define DEFAULT_CHANNEL_NAME  NULL
#define DEFAULT_OVERFLOW_MODE GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE


static GstStaticPadTemplate static_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (GST_VIDEO_FORMATS_ALL)));


#define GST_INTER_PIPELINE_VIDEO_SINK(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_VIDEO_SINK, \
    GstInterPipelineVideoSink))
#define GST_INTER_PIPELINE_VIDEO_SINK_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_VIDEO_SINK, \
    GstInterPipelineVideoSinkClass))
#define GST_INTER_PIPELINE_VIDEO_SINK_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_VIDEO_SINK, \
    GstInterPipelineVideoSinkClass))
#define GST_INTER_PIPELINE_VIDEO_SINK_CAST(obj) \
    ((GstInterPipelineVideoSink *)(obj))
#define GST_IS_INTER_PIPELINE_VIDEO_SINK(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_VIDEO_SINK))
#define GST_IS_INTER_PIPELINE_VIDEO_SINK_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_VIDEO_SINK))


typedef struct _GstInterPipelineVideoSink GstInterPipelineVideoSink;
typedef struct _GstInterPipelineVideoSinkClass GstInterPipelineVideoSinkClass;


struct _GstInterPipelineVideoSink
{
  GstVideoSink parent;

  /* Value of channel-name property.
   * Must be accessed with OBJECT_LOCK held. */
  gchar *channel_name;
  /* Value of overflow-mode property.
   * Must be accessed with OBJECT_LOCK held. */
  GstInterPipelineOverflowMode overflow_mode;

  /* The inter pipeline channel to use for transmitting video frames
   * to an interpipelinevideosrc element. */
  GstInterPipelineVideoChannel *channel;

  /* GstVideoInfo describing the format of the incoming frames. */
  GstVideoInfo input_video_info;

  /* If 1, then the sink is currently unlocked, and any calls that would
   * normally block must not block (and exit immediately instead). */
  gint unlocked;
};

struct _GstInterPipelineVideoSinkClass
{
  GstVideoSinkClass parent_class;
};


G_DEFINE_TYPE (GstInterPipelineVideoSink, gst_inter_pipeline_video_sink,
    GST_TYPE_VIDEO_SINK);


static void gst_inter_pipeline_video_sink_dispose (GObject * object);
static void gst_inter_pipeline_video_sink_set_property (GObject * object,
    guint prop_id, GValue const *value, GParamSpec * pspec);
static void gst_inter_pipeline_video_sink_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_inter_pipeline_video_sink_set_caps (GstBaseSink *
    base_sink, GstCaps * caps);
static void gst_inter_pipeline_video_sink_get_times (GstBaseSink * base_sink,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_inter_pipeline_video_sink_start (GstBaseSink * base_sink);
static gboolean gst_inter_pipeline_video_sink_stop (GstBaseSink * base_sink);
static gboolean gst_inter_pipeline_video_sink_unlock (GstBaseSink * base_sink);
static gboolean gst_inter_pipeline_video_sink_unlock_stop (GstBaseSink *
    base_sink);

static GstFlowReturn gst_inter_pipeline_video_sink_show_frame (GstVideoSink
    * video_sink, GstBuffer * buffer);




static void
gst_inter_pipeline_video_sink_class_init (GstInterPipelineVideoSinkClass *
    klass)
{
  GObjectClass *object_class;
  GstElementClass *element_class;
  GstBaseSinkClass *base_sink_class;
  GstVideoSinkClass *video_sink_class;

  GST_DEBUG_CATEGORY_INIT (inter_pipeline_video_sink_debug,
      "interpipelinevideosink", 0, "inter pipeline video sink");

  object_class = G_OBJECT_CLASS (klass);
  element_class = GST_ELEMENT_CLASS (klass);
  base_sink_class = GST_BASE_SINK_CLASS (klass);
  video_sink_class = GST_VIDEO_SINK_CLASS (klass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&static_sink_template));

  object_class->dispose =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_dispose);
  object_class->set_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_set_property);
  object_class->get_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_get_property);

  base_sink_class->set_caps =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_set_caps);
  base_sink_class->get_times =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_get_times);
  base_sink_class->start =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_start);
  base_sink_class->stop =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_stop);
  base_sink_class->unlock =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_unlock);
  base_sink_class->unlock_stop =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_unlock_stop);

  video_sink_class->show_frame =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_sink_show_frame);

  gst_element_class_set_static_metadata (element_class,
      "interpipelinevideosink",
      "Sink/Video",
      "Virtual video sink for transferring video frames between pipelines in "
      "the same process", "Carlos Rafael Giani <crg7475@mailbox.org>");

  g_object_class_install_property (object_class,
      PROP_CHANNEL_NAME,
      g_param_spec_string ("channel-name",
          "Channel name",
          "Unique name of transmission channel between this sink and a "
          "corresponding source (must be a non-NULL non-empty string)",
          DEFAULT_CHANNEL_NAME, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_OVERFLOW_MODE,
      g_param_spec_enum ("overflow-mode",
          "Overflow mode",
          "What to do in case the channel's buffer is full",
          gst_inter_pipeline_overflow_mode_get_type (),
          DEFAULT_OVERFLOW_MODE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
}


static void
gst_inter_pipeline_video_sink_init (GstInterPipelineVideoSink *
    inter_pipeline_video_sink)
{
  inter_pipeline_video_sink->channel_name = g_strdup (DEFAULT_CHANNEL_NAME);
  inter_pipeline_video_sink->overflow_mode = DEFAULT_OVERFLOW_MODE;

  inter_pipeline_video_sink->channel = NULL;
  gst_video_info_init (&(inter_pipeline_video_sink->input_video_info));
  inter_pipeline_video_sink->unlocked = 0;
}


static void
gst_inter_pipeline_video_sink_dispose (GObject * object)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (object);

  g_free (inter_pipeline_video_sink->channel_name);

  G_OBJECT_CLASS (gst_inter_pipeline_video_sink_parent_class)->dispose (object);
}


static void
gst_inter_pipeline_video_sink_set_property (GObject * object, guint prop_id,
    GValue const *value, GParamSpec * pspec)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_sink);
      g_free (inter_pipeline_video_sink->channel_name);
      inter_pipeline_video_sink->channel_name = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (inter_pipeline_video_sink);
      break;
    }

    case PROP_OVERFLOW_MODE:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_sink);
      inter_pipeline_video_sink->overflow_mode = g_value_get_enum (value);
      GST_OBJECT_UNLOCK (inter_pipeline_video_sink);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_inter_pipeline_video_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_sink);
      g_value_set_string (value, inter_pipeline_video_sink->channel_name);
      GST_OBJECT_UNLOCK (inter_pipeline_video_sink);
      break;
    }

    case PROP_OVERFLOW_MODE:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_sink);
      g_value_set_enum (value, inter_pipeline_video_sink->overflow_mode);
      GST_OBJECT_UNLOCK (inter_pipeline_video_sink);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static gboolean
gst_inter_pipeline_video_sink_set_caps (GstBaseSink * base_sink, GstCaps * caps)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (base_sink);
  GstVideoInfo video_info;

  if (!gst_video_info_from_caps (&video_info, caps)) {
    GST_ERROR_OBJECT (base_sink, "failed to parse caps %" GST_PTR_FORMAT,
        (gpointer) caps);
    return FALSE;
  }

  GST_DEBUG_OBJECT (base_sink,
      "setting video channel \"%s\"'s video info to the caps %" GST_PTR_FORMAT,
      GST_INTER_PIPELINE_CHANNEL_NAME (inter_pipeline_video_sink->channel),
      (gpointer) caps);

  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_video_sink->channel);
  inter_pipeline_video_sink->input_video_info = video_info;
  inter_pipeline_video_sink->channel->video_info = video_info;
  inter_pipeline_video_sink->channel->video_info_valid = TRUE;
  inter_pipeline_video_sink->channel->video_info_updated = TRUE;
  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_video_sink->channel);

  return TRUE;
}


static void
gst_inter_pipeline_video_sink_get_times (GstBaseSink * base_sink,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (base_sink);

  /* Compute valid start / end timestamps based on the information we have.
   * If there is no valid timestamp data, we cannot compute anything,
   * and start & end are set to GST_CLOCK_TIME_NONE.
   * If there is a valid buffer timestamp, we can assign its value to start,
   * and compute end either by using the buffer's duration (if there is one),
   * or assume that the buffer duration implicitely corresponds to the
   * framerate. */

  if (GST_BUFFER_PTS_IS_VALID (buffer)) {
    GstClockTime pts = GST_BUFFER_PTS (buffer);

    *start = pts;

    if (GST_BUFFER_DURATION_IS_VALID (buffer)) {
      *end = pts + GST_BUFFER_DURATION (buffer);
      GST_LOG_OBJECT (inter_pipeline_video_sink,
          "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
          GST_TIME_FORMAT, (gpointer) buffer, GST_TIME_ARGS (*start),
          GST_TIME_ARGS (*end));
    } else {
      gint fps_n =
          GST_VIDEO_INFO_FPS_N (&(inter_pipeline_video_sink->input_video_info));
      gint fps_d =
          GST_VIDEO_INFO_FPS_D (&(inter_pipeline_video_sink->input_video_info));
      if (fps_n > 0)
        *end = pts + gst_util_uint64_scale_int (GST_SECOND, fps_d, fps_n);

      GST_LOG_OBJECT (inter_pipeline_video_sink,
          "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
          GST_TIME_FORMAT " based on FPS %d/%d", (gpointer) buffer,
          GST_TIME_ARGS (*start), GST_TIME_ARGS (*end), fps_n, fps_d);
    }

    return;
  }

  GST_LOG_OBJECT (inter_pipeline_video_sink,
      "no start-end times for given buffer %p specified since buffer has "
      "no PTS", (gpointer) buffer);

  *start = *end = GST_CLOCK_TIME_NONE;
}


static gboolean
gst_inter_pipeline_video_sink_start (GstBaseSink * base_sink)
{
  gchar *channel_name;
  gboolean ret = TRUE;
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (base_sink);


  /* Get a copy of channel_name to avoid race conditions and having
   * to hold the object lock throughout this entire function. */
  GST_OBJECT_LOCK (inter_pipeline_video_sink);
  channel_name = g_strdup (inter_pipeline_video_sink->channel_name);
  GST_OBJECT_UNLOCK (inter_pipeline_video_sink);


  /* Perform some sanity checks. */

  if ((channel_name == NULL) || (channel_name[0] == '\0')) {
    GST_ELEMENT_ERROR (inter_pipeline_video_sink, RESOURCE, NOT_FOUND,
        ("Channel name is NULL or an empty string."), (NULL));
    ret = FALSE;
    goto finish;
  }


  /* Initialize structures. */
  gst_video_info_init (&(inter_pipeline_video_sink->input_video_info));


  /* Now get the channel with the specified name. */

  GST_DEBUG_OBJECT (inter_pipeline_video_sink,
      "getting video channel \"%s\" (gets created if it does not exist yet)",
      channel_name);

  inter_pipeline_video_sink->channel =
      GST_INTER_PIPELINE_VIDEO_CHANNEL
      (gst_inter_pipeline_channel_table_get_channel
      (gst_inter_pipeline_video_channel_table_get (),
          GST_ELEMENT_CAST (base_sink), TRUE, channel_name, NULL)
      );

  if (G_UNLIKELY (inter_pipeline_video_sink->channel == NULL)) {
    GST_ERROR_OBJECT (inter_pipeline_video_sink, "could not get video channel");
    ret = FALSE;
  }


  GST_TRACE_OBJECT (inter_pipeline_video_sink,
      "inter pipeline video sink started");

finish:
  g_free (channel_name);
  return ret;
}


static gboolean
gst_inter_pipeline_video_sink_stop (GstBaseSink * base_sink)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (base_sink);

  /* The channel's finalize function takes care of removing the
   * channel from the table if the refcount reaches 0. We do it
   * this way, since the source element at the other end may still
   * be holding a reference to the channel, so removing it here
   * from the table would not be correct. */
  if (G_LIKELY (inter_pipeline_video_sink->channel != NULL)) {
    GST_DEBUG_OBJECT (inter_pipeline_video_sink,
        "unref'ing video channel \"%s\"",
        GST_INTER_PIPELINE_CHANNEL_NAME (inter_pipeline_video_sink->channel));
    gst_inter_pipeline_channel_unref (GST_INTER_PIPELINE_CHANNEL_CAST
        (inter_pipeline_video_sink->channel), GST_ELEMENT_CAST (base_sink));
    inter_pipeline_video_sink->channel = NULL;
  } else
    GST_DEBUG_OBJECT (inter_pipeline_video_sink,
        "there is no video channel; nothing to unref");

  GST_TRACE_OBJECT (inter_pipeline_video_sink,
      "inter pipeline video sink stopped");

  return TRUE;
}


static gboolean
gst_inter_pipeline_video_sink_unlock (GstBaseSink * base_sink)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (base_sink);

  /* Raise the unlocked flag. */
  g_atomic_int_set (&inter_pipeline_video_sink->unlocked, 1);

  /* Wake up the show_frame() function if it is currently
   * waiting on our condition variable. */
  GST_DEBUG_OBJECT (inter_pipeline_video_sink,
      "sink unlocked; waking up condition variable (if it is waiting)");
  GST_INTER_PIPELINE_CHANNEL_SIGNAL_COND (inter_pipeline_video_sink->channel);

  return TRUE;
}


static gboolean
gst_inter_pipeline_video_sink_unlock_stop (GstBaseSink * base_sink)
{
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK (base_sink);

  /* Clear the unlocked flag so that show_frame() can wait on
   * the condition variable again like normal if it needs to. */
  g_atomic_int_set (&inter_pipeline_video_sink->unlocked, 0);

  GST_DEBUG_OBJECT (inter_pipeline_video_sink, "sink unlock stopped");

  return TRUE;
}


static GstFlowReturn
gst_inter_pipeline_video_sink_show_frame (GstVideoSink * video_sink,
    GstBuffer * buffer)
{
  GstFlowReturn flow_ret = GST_FLOW_OK;
  GstBaseSink *base_sink = GST_BASE_SINK_CAST (video_sink);
  GstInterPipelineVideoSink *inter_pipeline_video_sink =
      GST_INTER_PIPELINE_VIDEO_SINK_CAST (video_sink);

  GST_LOG_OBJECT (inter_pipeline_video_sink,
      "transmitting video frame over video channel; %" GST_PTR_FORMAT,
      (gpointer) buffer);

  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_video_sink->channel);

  if (inter_pipeline_video_sink->channel->video_frame != NULL) {
    switch (inter_pipeline_video_sink->overflow_mode) {
      case GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE:
      {
        /* In overwrite mode, "overwrite" the oldest data by
         * discarding the frame that is currently in the channel. */

        GST_LOG_OBJECT (inter_pipeline_video_sink,
            "replacing existing video frame in channel");
        /* Discard the old frame right away,
         * even if the consumer hasn't used it yet. */
        gst_buffer_replace (&(inter_pipeline_video_sink->channel->video_frame),
            NULL);
        break;
      }

      case GST_INTER_PIPELINE_OVERFLOW_MODE_BLOCK:
      {
        /* In block mode, wait until the other side (= the source
         * element that is also using this channel) signals that
         * data has been consumed, or until the unlocked flag is
         * set. In the latter case, we have to wait for preroll.
         * This is because the unlock() function (which sets the
         * unlocked flag) is called whenever something occurs that
         * requires prerolling again, such as pause/unpause. */

        while (inter_pipeline_video_sink->channel->
            num_times_video_frame_pushed == 0) {
          GST_LOG_OBJECT (inter_pipeline_video_sink,
              "waiting until existing video frame in channel is consumed");
          if (g_atomic_int_get (&inter_pipeline_video_sink->unlocked)) {
            GST_INTER_PIPELINE_CHANNEL_UNLOCK
                (inter_pipeline_video_sink->channel);

            /* Waiting according to the GstBaseSink documentation,
             * which states that "subclasses should unblock any
             * blocked function ASAP and call
             * gst_base_sink_wait_preroll()". */
            if ((flow_ret =
                    gst_base_sink_wait_preroll (base_sink)) != GST_FLOW_OK)
              goto stopping;

            GST_INTER_PIPELINE_CHANNEL_LOCK
                (inter_pipeline_video_sink->channel);
          }

          GST_INTER_PIPELINE_CHANNEL_WAIT_COND
              (inter_pipeline_video_sink->channel);
        }

        /* Discard the old frame after the consumer used it. */
        gst_buffer_replace (&(inter_pipeline_video_sink->channel->video_frame),
            NULL);

        break;
      }
    }
  }

  /* Place the new video frame in the channel. Also reset the
   * num_times_video_frame_pushed counter, since it hasn't been
   * pushed downstream by the client yet of course (it did not get
   * the chance yet to do anything with the video frame). */
  inter_pipeline_video_sink->channel->video_frame = gst_buffer_ref (buffer);
  inter_pipeline_video_sink->channel->num_times_video_frame_pushed = 0;

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_video_sink->channel);
  return flow_ret;

stopping:
  GST_DEBUG_OBJECT (inter_pipeline_video_sink,
      "stopping due to early exit after waiting for preroll "
      "(flow return value: %s)", gst_flow_get_name (flow_ret));
  return flow_ret;
}
