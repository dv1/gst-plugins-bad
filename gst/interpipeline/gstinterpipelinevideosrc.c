#include <gst/gst.h>
#include <gst/base/base.h>
#include <gst/video/video.h>
#include "gstinterpipelinecommon.h"
#include "gstinterpipelinevideocommon.h"
#include "gstinterpipelinevideosrc.h"


GST_DEBUG_CATEGORY_STATIC (inter_pipeline_video_src_debug);
#define GST_CAT_DEFAULT inter_pipeline_video_src_debug


enum
{
  PROP_0,
  PROP_CHANNEL_NAME,
  PROP_TIMEOUT,
  PROP_REPEATED_FRAME_AS_GAP,
  PROP_BLACK_FRAME_AS_GAP,
  PROP_FIXATION_CAPS
};


#define DEFAULT_CHANNEL_NAME          NULL
#define DEFAULT_TIMEOUT               GST_SECOND
#define DEFAULT_REPEATED_FRAME_AS_GAP TRUE
#define DEFAULT_BLACK_FRAME_AS_GAP    TRUE

#define DEFAULT_FIXATION_CAPS \
    "video/x-raw, " \
    "format = (string) I420, " \
    "width = (int) 320, " \
    "height = (int) 240, " \
    "framerate = (fraction) 30/1, " \
    "pixel-aspect-ratio = (fraction) 1/1, " \
    "interlaced = (gboolean) false"


static GstStaticPadTemplate static_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE (GST_VIDEO_FORMATS_ALL)));


#define GST_INTER_PIPELINE_VIDEO_SRC(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_VIDEO_SRC, \
    GstInterPipelineVideoSrc))
#define GST_INTER_PIPELINE_VIDEO_SRC_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_VIDEO_SRC, \
    GstInterPipelineVideoSrcClass))
#define GST_INTER_PIPELINE_VIDEO_SRC_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_VIDEO_SRC, \
    GstInterPipelineVideoSrcClass))
#define GST_INTER_PIPELINE_VIDEO_SRC_CAST(obj) \
    ((GstInterPipelineVideoSrc *)(obj))
#define GST_IS_INTER_PIPELINE_VIDEO_SRC(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_VIDEO_SRC))
#define GST_IS_INTER_PIPELINE_VIDEO_SRC_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_VIDEO_SRC))


typedef struct _GstInterPipelineVideoSrc GstInterPipelineVideoSrc;
typedef struct _GstInterPipelineVideoSrcClass GstInterPipelineVideoSrcClass;


struct _GstInterPipelineVideoSrc
{
  GstPushSrc parent;

  /* Value of channel-name property.
   * Must be accessed with OBJECT_LOCK held. */
  gchar *channel_name;
  /* Value of timeout property.
   * Must be accessed with OBJECT_LOCK held. */
  GstClockTime timeout;
  /* Value of repeated-frame-as-gap property.
   * Must be accessed with OBJECT_LOCK held. */
  gboolean repeated_frame_as_gap;
  /* Value of black-frame-as-gap property.
   * Must be accessed with OBJECT_LOCK held. */
  gboolean black_frame_as_gap;
  /* Value of fixation-caps property.
   * Must be accessed with OBJECT_LOCK held. */
  GstCaps *fixation_caps;

  /* The inter pipeline channel to use for receiving video frames
   * from an interpipelinevideosink element. */
  GstInterPipelineVideoChannel *channel;

  /* TRUE if output_video_info has been set to a valid value. */
  gboolean output_video_info_set;
  /* Video info computed from the negotiated src caps. */
  GstVideoInfo output_video_info;

  /* Video frame containing black pixels. Used if there hasn't
   * been a new input frame coming over the pipeline channel and
   * the timeout ran out. (Re)created in the set_caps() vmethod. */
  GstBuffer *black_video_frame_buffer;

  /* A copy of the video info from the pipeline channel.
   * We use a copy to avoid race conditions and/or overly long
   * mutex locks. (We copy the video info while the channel's
   * mutex is locked. Later, the copy can be used even without
   * that mutex locked.) */
  gboolean cur_channel_video_info_valid;
  GstVideoInfo cur_channel_video_info;

  /* How many frames make up the time period specified by the
   * timeout property. */
  gsize num_timeout_frames;

  /* Video info containing reference values that are used in the
   * fixate() vmethod. */
  GstVideoInfo fixation_video_info;

  /* How many frames got pushed since the last time the srcpad's
   * caps were (re)negotiated. Used for timestamp generation. */
  gsize num_frames_pushed;
  /* Base offset for generated timestamps. This gets incremented
   * every time caps are (re)negotiated. */
  GstClockTime timestamp_base_offset;
};

struct _GstInterPipelineVideoSrcClass
{
  GstPushSrcClass parent_class;
};


G_DEFINE_TYPE (GstInterPipelineVideoSrc, gst_inter_pipeline_video_src,
    GST_TYPE_PUSH_SRC);


static void gst_inter_pipeline_video_src_dispose (GObject * object);
static void gst_inter_pipeline_video_src_set_property (GObject * object,
    guint prop_id, GValue const *value, GParamSpec * pspec);
static void gst_inter_pipeline_video_src_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static GstCaps *gst_inter_pipeline_video_src_get_caps (GstBaseSrc * base_src,
    GstCaps * filter);
static gboolean gst_inter_pipeline_video_src_set_caps (GstBaseSrc * base_src,
    GstCaps * caps);
static GstCaps *gst_inter_pipeline_video_src_fixate (GstBaseSrc * base_src,
    GstCaps * caps);
static gboolean gst_inter_pipeline_video_src_negotiate (GstBaseSrc * base_src);
static gboolean gst_inter_pipeline_video_src_start (GstBaseSrc * base_src);
static gboolean gst_inter_pipeline_video_src_stop (GstBaseSrc * base_src);
static void gst_inter_pipeline_video_src_get_times (GstBaseSrc * base_src,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);

static GstFlowReturn gst_inter_pipeline_video_src_create (GstPushSrc *
    push_src, GstBuffer ** buffer);

static void
    gst_inter_pipeline_video_src_recalculate_num_timeout_frames
    (GstInterPipelineVideoSrc * inter_pipeline_video_src, GstClockTime timeout);
static gboolean
gst_inter_pipeline_video_src_update_fixation_states (GstInterPipelineVideoSrc *
    inter_pipeline_video_src, const GstCaps * input_fixation_caps);




static void
gst_inter_pipeline_video_src_class_init (GstInterPipelineVideoSrcClass * klass)
{
  GObjectClass *object_class;
  GstElementClass *element_class;
  GstBaseSrcClass *base_src_class;
  GstPushSrcClass *push_src_class;

  GST_DEBUG_CATEGORY_INIT (inter_pipeline_video_src_debug,
      "interpipelinevideosrc", 0, "inter pipeline video source");

  object_class = G_OBJECT_CLASS (klass);
  element_class = GST_ELEMENT_CLASS (klass);
  base_src_class = GST_BASE_SRC_CLASS (klass);
  push_src_class = GST_PUSH_SRC_CLASS (klass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&static_src_template));

  object_class->dispose =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_dispose);
  object_class->set_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_set_property);
  object_class->get_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_get_property);

  base_src_class->get_caps =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_get_caps);
  base_src_class->set_caps =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_set_caps);
  base_src_class->fixate =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_fixate);
  base_src_class->negotiate =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_negotiate);
  base_src_class->start =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_start);
  base_src_class->stop = GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_stop);
  base_src_class->get_times =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_get_times);

  push_src_class->create =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_src_create);

  gst_element_class_set_static_metadata (element_class,
      "interpipelinevideosrc",
      "Src/Video",
      "Virtual video source for transferring video frames between pipelines "
      "in the same process", "Carlos Rafael Giani <crg7475@mailbox.org>");

  g_object_class_install_property (object_class,
      PROP_CHANNEL_NAME,
      g_param_spec_string ("channel-name",
          "Channel name",
          "Unique name of transmission channel between this source and a "
          "corresponding sink",
          DEFAULT_CHANNEL_NAME, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_TIMEOUT,
      g_param_spec_uint64 ("timeout",
          "Timeout",
          "Timeout after which to start outputting black frames, in "
          "nanoseconds (0 = do not output black frames, just repeat the "
          "last frame indefinitely)",
          0, G_MAXUINT64,
          DEFAULT_TIMEOUT, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_REPEATED_FRAME_AS_GAP,
      g_param_spec_boolean ("repeated-frame-as-gap",
          "Repeated frame as gap",
          "Set the gap flag in buffers of repeated video frames",
          DEFAULT_REPEATED_FRAME_AS_GAP,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_BLACK_FRAME_AS_GAP,
      g_param_spec_boolean ("black-frame-as-gap",
          "Black frame as gap",
          "Set the gap flag in buffers of black video frames",
          DEFAULT_BLACK_FRAME_AS_GAP,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
  g_object_class_install_property (object_class,
      PROP_FIXATION_CAPS,
      g_param_spec_boxed ("fixation-caps",
          "Fixation caps",
          "Fixed caps to use as reference during the fixation process",
          GST_TYPE_CAPS, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)
      );
}


static void
gst_inter_pipeline_video_src_init (GstInterPipelineVideoSrc *
    inter_pipeline_video_src)
{
  gboolean fixation_states_set_up;
  GstCaps *initial_fixation_caps;

  gst_base_src_set_format (GST_BASE_SRC (inter_pipeline_video_src),
      GST_FORMAT_TIME);
  gst_base_src_set_live (GST_BASE_SRC (inter_pipeline_video_src), TRUE);

  inter_pipeline_video_src->channel_name = g_strdup (DEFAULT_CHANNEL_NAME);
  inter_pipeline_video_src->timeout = DEFAULT_TIMEOUT;
  inter_pipeline_video_src->repeated_frame_as_gap =
      DEFAULT_REPEATED_FRAME_AS_GAP;
  inter_pipeline_video_src->black_frame_as_gap = DEFAULT_BLACK_FRAME_AS_GAP;

  inter_pipeline_video_src->channel = NULL;
  inter_pipeline_video_src->output_video_info_set = FALSE;
  gst_video_info_init (&(inter_pipeline_video_src->output_video_info));
  inter_pipeline_video_src->black_video_frame_buffer = NULL;

  inter_pipeline_video_src->cur_channel_video_info_valid = FALSE;
  gst_video_info_init (&(inter_pipeline_video_src->cur_channel_video_info));

  inter_pipeline_video_src->num_timeout_frames = 0;

  inter_pipeline_video_src->num_frames_pushed = 0;
  inter_pipeline_video_src->timestamp_base_offset = 0;

  /* Fill fixation_caps and fixation_video_info to
   * values from the hardcoded DEFAULT_FIXATION_CAPS. */
  initial_fixation_caps = gst_caps_from_string (DEFAULT_FIXATION_CAPS);
  g_assert (initial_fixation_caps != NULL);
  fixation_states_set_up =
      gst_inter_pipeline_video_src_update_fixation_states
      (inter_pipeline_video_src, initial_fixation_caps);
  gst_caps_unref (initial_fixation_caps);
  g_assert (fixation_states_set_up);
}


static void
gst_inter_pipeline_video_src_dispose (GObject * object)
{
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (object);

  g_free (inter_pipeline_video_src->channel_name);
  gst_caps_replace (&(inter_pipeline_video_src->fixation_caps), NULL);

  G_OBJECT_CLASS (gst_inter_pipeline_video_src_parent_class)->dispose (object);
}


static void
gst_inter_pipeline_video_src_set_property (GObject * object, guint prop_id,
    GValue const *value, GParamSpec * pspec)
{
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      g_free (inter_pipeline_video_src->channel_name);
      inter_pipeline_video_src->channel_name = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_TIMEOUT:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      inter_pipeline_video_src->timeout = g_value_get_uint64 (value);
      gst_inter_pipeline_video_src_recalculate_num_timeout_frames
          (inter_pipeline_video_src, inter_pipeline_video_src->timeout);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_REPEATED_FRAME_AS_GAP:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      inter_pipeline_video_src->repeated_frame_as_gap =
          g_value_get_boolean (value);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_BLACK_FRAME_AS_GAP:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      inter_pipeline_video_src->black_frame_as_gap =
          g_value_get_boolean (value);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_FIXATION_CAPS:
    {
      gboolean ret;
      GstCaps *caps = GST_CAPS_CAST (g_value_get_boxed (value));

      if (caps == NULL) {
        GST_ELEMENT_ERROR (inter_pipeline_video_src, STREAM, FORMAT,
            ("Cannot use NULL as fixation caps"), (NULL));
        break;
      }

      if (gst_caps_is_empty (caps)) {
        GST_ELEMENT_ERROR (inter_pipeline_video_src, STREAM, FORMAT,
            ("Cannot use empty caps as fixation caps"), (NULL));
        break;
      }

      if (!gst_caps_is_fixed (caps)) {
        GST_ELEMENT_ERROR (inter_pipeline_video_src, STREAM, FORMAT,
            ("Fixation caps must themselves also be fixed"),
            ("provided caps: %" GST_PTR_FORMAT, (gpointer) caps));
        break;
      }

      GST_OBJECT_LOCK (inter_pipeline_video_src);
      /* Update fixation_caps and fixation_video_info to
       * the values provided here over the GObject property. */
      ret =
          gst_inter_pipeline_video_src_update_fixation_states
          (inter_pipeline_video_src, caps);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);

      if (!ret) {
        GST_ELEMENT_ERROR (inter_pipeline_video_src, STREAM, FORMAT,
            ("Could not use provided fixation caps"),
            ("provided caps: %" GST_PTR_FORMAT, (gpointer) caps));
        break;
      }

      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_inter_pipeline_video_src_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      g_value_set_string (value, inter_pipeline_video_src->channel_name);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_TIMEOUT:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      g_value_set_uint64 (value, inter_pipeline_video_src->timeout);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_REPEATED_FRAME_AS_GAP:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      g_value_set_boolean (value,
          inter_pipeline_video_src->repeated_frame_as_gap);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_BLACK_FRAME_AS_GAP:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      g_value_set_boolean (value, inter_pipeline_video_src->black_frame_as_gap);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    case PROP_FIXATION_CAPS:
    {
      GST_OBJECT_LOCK (inter_pipeline_video_src);
      gst_caps_ref (inter_pipeline_video_src->fixation_caps);
      g_value_take_boxed (value, inter_pipeline_video_src->fixation_caps);
      GST_OBJECT_UNLOCK (inter_pipeline_video_src);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static GstCaps *
gst_inter_pipeline_video_src_get_caps (GstBaseSrc * base_src, GstCaps * filter)
{
  GstCaps *caps = NULL;
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);

  if (inter_pipeline_video_src->channel == NULL) {
    /* If we haven't gotten a channel (because start() was not yet called),
     * just call the base class' get_caps() vfunction, since we cannot
     * do anything else without information from channel. */

    return
        GST_BASE_SRC_CLASS (gst_inter_pipeline_video_src_parent_class)->get_caps
        (base_src, filter);
  }

  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_video_src->channel);

  /* If we already have a channel, use its video info to generate caps.
   * get_caps() is called to report to other elements what caps we
   * can handle (by using a caps query). And the caps we can handle are
   * defined by what formats the channel can handle.
   * If however the channel does not (yet) have a valid video info,
   * then we cannot generate any caps. In that case, fall back to
   * default base class behavior (done at the end of the function). */
  if (inter_pipeline_video_src->channel->video_info_valid) {
    caps =
        gst_video_info_to_caps (&(inter_pipeline_video_src->channel->
            video_info));
    g_assert (caps != NULL);

    /* We retimestamp the video frames according to whatever framerate
     * downstream requires, so make sure the framerate field isn't fixed
     * at all, and instead encompasses the entire possible range. */
    gst_caps_set_simple (caps, "framerate", GST_TYPE_FRACTION_RANGE, 1,
        G_MAXINT, G_MAXINT, 1, NULL);

    /* Apply filter caps if present. */
    if (filter != NULL) {
      GstCaps *tmp;

      tmp = gst_caps_intersect_full (filter, caps, GST_CAPS_INTERSECT_FIRST);
      gst_caps_unref (caps);
      caps = tmp;
    }
  }

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_video_src->channel);

  if (caps != NULL)
    return caps;
  else
    return
        GST_BASE_SRC_CLASS (gst_inter_pipeline_video_src_parent_class)->get_caps
        (base_src, filter);
}


static gboolean
gst_inter_pipeline_video_src_set_caps (GstBaseSrc * base_src, GstCaps * caps)
{
  gboolean ret = TRUE;
  GstAllocator *allocator = NULL;
  GstAllocationParams alloc_params;
  GstBuffer *temp_buffer = NULL;
  GstClockTime timeout;
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);


  /* Set output_video_info to the new output caps. */

  if (!gst_video_info_from_caps (&(inter_pipeline_video_src->output_video_info),
          caps)) {
    GST_ERROR_OBJECT (inter_pipeline_video_src,
        "could not parse video caps %" GST_PTR_FORMAT, (gpointer) caps);
    return FALSE;
  }

  inter_pipeline_video_src->output_video_info_set = TRUE;


  /* Recalculate how many frames equal the specified timeout span.
   * This requires the newly defined output video info, since the
   * framerate is a factor in the calculations. */
  GST_OBJECT_LOCK (inter_pipeline_video_src);
  timeout = inter_pipeline_video_src->timeout;
  GST_OBJECT_UNLOCK (inter_pipeline_video_src);
  gst_inter_pipeline_video_src_recalculate_num_timeout_frames
      (inter_pipeline_video_src, timeout);


  /* Generate black frame that is compatible with the new output caps. */

  /* For allocating the black frame and any temporary frames, use the
   * allocator that was picked during the caps negotiation phase. */
  gst_base_src_get_allocator (base_src, &allocator, &alloc_params);

  /* Discard any already existing black frame first, since it might
   * not be of correct size etc. */
  gst_buffer_replace (&(inter_pipeline_video_src->black_video_frame_buffer),
      NULL);

  {
    GstVideoInfo temp_video_info;
    GstVideoFrame src_frame, dest_frame;
    GstVideoConverter *converter = NULL;

    /* We need to make sure the black frame is filled with values that
     * represent black pixels. These values differ depending on the format.
     * To avoid having to set specific values for all kinds of formats,
     * we simply create a temporary buffer that has a fixed RGB format,
     * fill it with nullbytes (which creates a black frame in RGB format),
     * and then convert its contents to the actual black frame by using
     * GstVideoConverter.
     *
     * As a prerequisite, we initialize temp_video_info to the same
     * width/height as the channel's video info, the same framerate as
     * the output_video_info, and ARGB as hardcoded format. This
     * temp_video_info is then used to create temp_buffer that will hold
     * the temporary black RGB pixels. */

    gst_video_info_set_format (&temp_video_info,
        GST_VIDEO_FORMAT_ARGB,
        GST_VIDEO_INFO_WIDTH (&(inter_pipeline_video_src->output_video_info)),
        GST_VIDEO_INFO_HEIGHT (&(inter_pipeline_video_src->output_video_info))
        );
    GST_VIDEO_INFO_FPS_N (&temp_video_info) =
        GST_VIDEO_INFO_FPS_N (&(inter_pipeline_video_src->output_video_info));
    GST_VIDEO_INFO_FPS_D (&temp_video_info) =
        GST_VIDEO_INFO_FPS_D (&(inter_pipeline_video_src->output_video_info));

    temp_buffer = gst_buffer_new_allocate (allocator,
        GST_VIDEO_INFO_SIZE (&temp_video_info), &alloc_params);
    if (G_UNLIKELY (temp_buffer == NULL)) {
      GST_ERROR_OBJECT (inter_pipeline_video_src,
          "could not allocate temp buffer for generating black frame");
      goto error;
    }

    gst_buffer_memset (temp_buffer, 0, 0,
        GST_VIDEO_INFO_SIZE (&temp_video_info));

    inter_pipeline_video_src->black_video_frame_buffer =
        gst_buffer_new_allocate (allocator,
        GST_VIDEO_INFO_SIZE (&(inter_pipeline_video_src->output_video_info)),
        &alloc_params);
    if (G_UNLIKELY (inter_pipeline_video_src->black_video_frame_buffer == NULL))
      goto error;

    gst_video_frame_map (&src_frame, &temp_video_info, temp_buffer,
        GST_MAP_READ);
    gst_video_frame_map (&dest_frame,
        &(inter_pipeline_video_src->output_video_info),
        inter_pipeline_video_src->black_video_frame_buffer, GST_MAP_WRITE);

    converter =
        gst_video_converter_new (&temp_video_info,
        &(inter_pipeline_video_src->output_video_info), NULL);
    gst_video_converter_frame (converter, &src_frame, &dest_frame);
    gst_video_converter_free (converter);

    gst_video_frame_unmap (&src_frame);
    gst_video_frame_unmap (&dest_frame);
  }


finish:
  gst_buffer_replace (&temp_buffer, NULL);

  if (allocator != NULL)
    gst_object_unref (GST_OBJECT (allocator));

  return ret;

error:
  ret = FALSE;
  goto finish;
}


static GstCaps *
gst_inter_pipeline_video_src_fixate (GstBaseSrc * base_src, GstCaps * caps)
{
  /* In here, we fixate unfixed caps to the reference values from
   * fixation_video_info. That video info is configurable over the
   * fixation-caps GObject property. */

  gint fps_n, fps_d;
  gint par_n, par_d;
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);
  GstStructure *structure;
  const GstVideoInfo *ref_video_info =
      &(inter_pipeline_video_src->fixation_video_info);

  GST_DEBUG_OBJECT (inter_pipeline_video_src,
      "caps before fixation: %" GST_PTR_FORMAT, (gpointer) caps);


  /* First, make the caps writable and truncate it (= get rid of all but the
   * first GstStructure in the caps). */
  caps = gst_caps_make_writable (caps);
  caps = gst_caps_truncate (caps);
  structure = gst_caps_get_structure (caps, 0);


  /* Next, fixate specific fields if they aren't fixed already. These fields
   * are fixated to reference values from fixation_video_info. If these
   * fields are not present, they are set to the reference values.*/

#define FIXATE_FIELD_TO_REFERENCE_VALUE(FIELD_NAME, FIXATE_TYPE, FIELD_GTYPE, ...) \
  G_STMT_START \
    if (gst_structure_has_field(structure, (FIELD_NAME))) \
      gst_structure_fixate_field_ ##FIXATE_TYPE (structure, (FIELD_NAME), \
          __VA_ARGS__); \
    else \
      gst_structure_set(structure, (FIELD_NAME), FIELD_GTYPE, __VA_ARGS__, \
          NULL); \
  G_STMT_END

  FIXATE_FIELD_TO_REFERENCE_VALUE ("format", string, G_TYPE_STRING,
      gst_video_format_to_string (GST_VIDEO_INFO_FORMAT (ref_video_info)));

  FIXATE_FIELD_TO_REFERENCE_VALUE ("interlace-mode", string, G_TYPE_STRING,
      gst_video_interlace_mode_to_string (GST_VIDEO_INFO_INTERLACE_MODE
          (ref_video_info)));

  FIXATE_FIELD_TO_REFERENCE_VALUE ("width", nearest_int, G_TYPE_INT,
      GST_VIDEO_INFO_WIDTH (ref_video_info));
  FIXATE_FIELD_TO_REFERENCE_VALUE ("height", nearest_int, G_TYPE_INT,
      GST_VIDEO_INFO_HEIGHT (ref_video_info));

  FIXATE_FIELD_TO_REFERENCE_VALUE ("chroma-site", string, G_TYPE_STRING,
      gst_video_chroma_to_string (GST_VIDEO_INFO_CHROMA_SITE (ref_video_info)));
  FIXATE_FIELD_TO_REFERENCE_VALUE ("colorimetry", string, G_TYPE_STRING,
      gst_video_colorimetry_to_string (&GST_VIDEO_INFO_COLORIMETRY
          (ref_video_info)));

  /* Special case for framerate: We always need a fixed framerate, since we
   * retimestamp output frames. So, if the reference video info contains a
   * null framerate, fall back to hardcoded 30 fps. */
  fps_n = GST_VIDEO_INFO_FPS_N (ref_video_info);
  fps_d = GST_VIDEO_INFO_FPS_D (ref_video_info);
  if ((fps_n == 0) || (fps_d == 0)) {
    fps_n = 30;
    fps_d = 1;
  }
  FIXATE_FIELD_TO_REFERENCE_VALUE ("framerate", nearest_fraction,
      GST_TYPE_FRACTION, fps_n, fps_d);

  /* Special case for framerate: A null pixel aspect ratio makes no sense.
   * So, in such a case, fall back to a 1:1 ratio. */
  par_n = GST_VIDEO_INFO_PAR_N (ref_video_info);
  par_d = GST_VIDEO_INFO_PAR_D (ref_video_info);
  if ((par_n == 0) || (par_d == 0)) {
    par_n = 1;
    par_d = 1;
  }
  FIXATE_FIELD_TO_REFERENCE_VALUE ("pixel-aspect-ratio", nearest_fraction,
      GST_TYPE_FRACTION, par_n, par_d);

#undef FIXATE_FIELD_TO_REFERENCE_VALUE

  /* Fixate any remaining unfixed values by using the default method. */
  gst_structure_fixate (structure);

  GST_DEBUG_OBJECT (inter_pipeline_video_src,
      "caps after fixation: %" GST_PTR_FORMAT, (gpointer) caps);
  return caps;
}


static gboolean
gst_inter_pipeline_video_src_negotiate (GstBaseSrc * base_src)
{
  GstCaps *channel_caps;
  GstCaps *downstream_caps;
  GstCaps *negotiated_caps;
  GstStructure *s;
  gboolean ret = TRUE;
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);

  /* If output_video_info was set earlier (in the set_caps() vmethod),
   * and we are here again, it means we are _re_negotiating the output
   * caps. As such, we need to update the timestamp_base_offset, since
   * we might continue output with a different frame rate (depending on
   * whatever downstream requires). */
  if (inter_pipeline_video_src->output_video_info_set) {
    inter_pipeline_video_src->timestamp_base_offset +=
        gst_util_uint64_scale (GST_SECOND *
        inter_pipeline_video_src->num_frames_pushed,
        GST_VIDEO_INFO_FPS_D (&inter_pipeline_video_src->output_video_info),
        GST_VIDEO_INFO_FPS_N (&inter_pipeline_video_src->output_video_info)
        );
  }

  /* After updating timestamp_base_offset, we must set num_frames_pushed to
   * 0, since its previous value was factored into timestamp_base_offset
   * during its update. */
  inter_pipeline_video_src->num_frames_pushed = 0;

  /* If the pipeline channel currently has no valid video info,
   * just let the base class perform regular negotiation and exit.
   * (The channel's video info is checked in the create() vmethod.) */
  if (!inter_pipeline_video_src->cur_channel_video_info_valid)
    return
        GST_BASE_SRC_CLASS
        (gst_inter_pipeline_video_src_parent_class)->negotiate (base_src);


  /* At this point, it is clear that (a) timestamp_base_offset was updated
   * if needed and (b) there is a valid video info in the pipeline channel.
   * We use that video info, along with the allowed caps from downstream,
   * to negotiate caps that satisfy all requirements from the channel and
   * from downstream. */


  /* Convert the channel's video info to caps, for further caps negotiation.
   * Remove its framerate and max-framerate fields though, since we do
   * retimestamp output frames based on the framerate downstream requires. */
  channel_caps =
      gst_video_info_to_caps
      (&inter_pipeline_video_src->cur_channel_video_info);
  if (G_UNLIKELY (channel_caps == NULL)) {
    GST_ERROR_OBJECT (inter_pipeline_video_src,
        "could not produce caps to negotiate with");
    return FALSE;
  }
  s = gst_caps_get_structure (channel_caps, 0);
  gst_structure_remove_field (s, "framerate");
  gst_structure_remove_field (s, "max-framerate");

  /* Query the caps downstream allows, for further caps negotiation.
   * In the caps query, we specify the channel_caps as filter. */
  {
    GstQuery *query;

    /* NOTE: We could in theory use gst_pad_get_allowed_caps() to achieve
     * the same result. However, that function leads to our get_caps()
     * vmethod being called, which locks the pipeline channel's mutex.
     * And, when negotiate() is called, this mutex may already be locked
     * (for example because of the gst_base_src_negotiate() call in the
     * create() vmethod), so we'd end up deadlocked. */

    downstream_caps = NULL;

    query = gst_query_new_caps (channel_caps);

    if (!gst_pad_peer_query (GST_BASE_SRC_PAD (base_src), query)) {
      GST_ERROR_OBJECT (inter_pipeline_video_src,
          "could not query peer pad's caps");
      goto query_done;
    }

    gst_query_parse_caps_result (query, &downstream_caps);
    if (downstream_caps == NULL) {
      GST_ERROR_OBJECT (inter_pipeline_video_src, "peer has no caps");
      goto query_done;
    }
    gst_caps_ref (downstream_caps);

    GST_DEBUG_OBJECT (inter_pipeline_video_src,
        "downstream caps: %" GST_PTR_FORMAT, (gpointer) downstream_caps);

  query_done:
    gst_query_unref (query);

    if (downstream_caps == NULL)
      return FALSE;
  }


  /* The final negotiated caps are the intersection between the channel caps
   * (= channel's video info in caps form) and the caps downstream allows. */
  negotiated_caps =
      gst_caps_intersect_full (downstream_caps, channel_caps,
      GST_CAPS_INTERSECT_FIRST);

  if (gst_caps_is_empty (negotiated_caps)) {
    GST_ERROR_OBJECT (base_src,
        "failed to negotiate src caps "
        "(using channel caps %" GST_PTR_FORMAT ")", (gpointer) channel_caps);
    ret = GST_FLOW_NOT_NEGOTIATED;
  }

  GST_DEBUG_OBJECT (inter_pipeline_video_src,
      "negotiated src caps: %" GST_PTR_FORMAT, (gpointer) negotiated_caps);


  /* Fixate the negotiated caps before we output them. */
  negotiated_caps =
      gst_inter_pipeline_video_src_fixate (base_src, negotiated_caps);
  GST_DEBUG_OBJECT (inter_pipeline_video_src, "fixated to: %" GST_PTR_FORMAT,
      (gpointer) negotiated_caps);


  /* Negotiation completed. Output the negotiated caps, cleanup, and exit. */

  ret = gst_base_src_set_caps (base_src, negotiated_caps);

  gst_caps_unref (negotiated_caps);
  gst_caps_unref (channel_caps);
  gst_caps_unref (downstream_caps);

  return ret;
}


static gboolean
gst_inter_pipeline_video_src_start (GstBaseSrc * base_src)
{
  gchar *channel_name;
  gboolean ret = TRUE;
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);


  /* Get a copy of channel_name to avoid race conditions and having
   * to hold the object lock throughout this entire function. */
  GST_OBJECT_LOCK (inter_pipeline_video_src);
  channel_name = g_strdup (inter_pipeline_video_src->channel_name);
  GST_OBJECT_UNLOCK (inter_pipeline_video_src);


  /* Perform some sanity checks. */

  if ((channel_name == NULL) || (channel_name[0] == '\0')) {
    GST_ELEMENT_ERROR (inter_pipeline_video_src, RESOURCE, NOT_FOUND,
        ("Channel name is NULL or an empty string."), (NULL));
    ret = FALSE;
    goto finish;
  }


  /* Initialize structures and values. */

  inter_pipeline_video_src->output_video_info_set = FALSE;
  gst_video_info_init (&(inter_pipeline_video_src->output_video_info));

  inter_pipeline_video_src->cur_channel_video_info_valid = FALSE;
  gst_video_info_init (&(inter_pipeline_video_src->cur_channel_video_info));

  inter_pipeline_video_src->num_frames_pushed = 0;
  inter_pipeline_video_src->timestamp_base_offset = 0;


  /* Now get the channel with the specified name. */

  GST_DEBUG_OBJECT (inter_pipeline_video_src,
      "getting video channel \"%s\" (gets created if it does not exist yet)",
      channel_name);

  inter_pipeline_video_src->channel =
      GST_INTER_PIPELINE_VIDEO_CHANNEL
      (gst_inter_pipeline_channel_table_get_channel
      (gst_inter_pipeline_video_channel_table_get (),
          GST_ELEMENT_CAST (base_src), FALSE, channel_name, NULL)
      );

  if (G_UNLIKELY (inter_pipeline_video_src->channel == NULL)) {
    GST_ERROR_OBJECT (inter_pipeline_video_src, "could not get video channel");
    ret = FALSE;
  }


  GST_TRACE_OBJECT (inter_pipeline_video_src,
      "inter pipeline video source started");

finish:
  g_free (channel_name);
  return ret;
}


static gboolean
gst_inter_pipeline_video_src_stop (GstBaseSrc * base_src)
{
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);

  /* The channel's finalize function takes care of removing the
   * channel from the table if the refcount reaches 0. We do it
   * this way, since the sink element at the other end may still
   * be holding a reference to the channel, so removing it here
   * from the table would not be correct. */
  if (G_LIKELY (inter_pipeline_video_src->channel != NULL)) {
    GST_DEBUG_OBJECT (inter_pipeline_video_src,
        "unref'ing video channel \"%s\"",
        GST_INTER_PIPELINE_CHANNEL_NAME (inter_pipeline_video_src->channel));
    gst_inter_pipeline_channel_unref (GST_INTER_PIPELINE_CHANNEL_CAST
        (inter_pipeline_video_src->channel), GST_ELEMENT_CAST (base_src));
    inter_pipeline_video_src->channel = NULL;
  }

  if (inter_pipeline_video_src->black_video_frame_buffer != NULL) {
    gst_buffer_unref (inter_pipeline_video_src->black_video_frame_buffer);
    inter_pipeline_video_src->black_video_frame_buffer = NULL;
  }

  return TRUE;
}


static void
gst_inter_pipeline_video_src_get_times (GstBaseSrc * base_src,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end)
{
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC (base_src);

  /* Compute valid start / end timestamps based on the information we have.
   * If there is no valid timestamp data, we cannot compute anything,
   * and start & end are set to GST_CLOCK_TIME_NONE.
   * If there is a valid buffer timestamp, we can assign its value to start,
   * and compute end either by using the buffer's duration (if there is one),
   * or assume that the buffer duration implicitely corresponds to the
   * framerate. */

  if (gst_base_src_is_live (base_src)) {
    if (GST_BUFFER_PTS_IS_VALID (buffer)) {
      GstClockTime pts = GST_BUFFER_PTS (buffer);

      *start = pts;

      if (GST_BUFFER_DURATION_IS_VALID (buffer)) {
        *end = pts + GST_BUFFER_DURATION (buffer);
        GST_LOG_OBJECT (inter_pipeline_video_src,
            "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
            GST_TIME_FORMAT, (gpointer) buffer, GST_TIME_ARGS (*start),
            GST_TIME_ARGS (*end));
      } else {
        gint fps_n =
            GST_VIDEO_INFO_FPS_N (&
            (inter_pipeline_video_src->output_video_info));
        gint fps_d =
            GST_VIDEO_INFO_FPS_D (&
            (inter_pipeline_video_src->output_video_info));
        if (fps_n > 0)
          *end = pts + gst_util_uint64_scale_int (GST_SECOND, fps_d, fps_n);

        GST_LOG_OBJECT (inter_pipeline_video_src,
            "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
            GST_TIME_FORMAT " based on FPS %d/%d", (gpointer) buffer,
            GST_TIME_ARGS (*start), GST_TIME_ARGS (*end), fps_n, fps_d);
      }

      return;
    }
  }

  GST_LOG_OBJECT (inter_pipeline_video_src,
      "no start-end times for given buffer %p specified since buffer has no PTS",
      (gpointer) buffer);

  *start = *end = GST_CLOCK_TIME_NONE;
}


static GstFlowReturn
gst_inter_pipeline_video_src_create (GstPushSrc * push_src, GstBuffer ** buffer)
{
  gsize num_timeout_frames;
  GstFlowReturn flow_ret = GST_FLOW_OK;
  GstBuffer *video_frame = NULL;
  gboolean is_gap = FALSE;
  gboolean repeated_frame_as_gap, black_frame_as_gap;
  GstInterPipelineVideoSrc *inter_pipeline_video_src =
      GST_INTER_PIPELINE_VIDEO_SRC_CAST (push_src);

  GST_LOG_OBJECT (inter_pipeline_video_src,
      "about to push a video frame downstream");


  /* Copy GObject property values to avoid race conditions and having
   * to hold the object lock throughout this entire function. */
  GST_OBJECT_LOCK (inter_pipeline_video_src);
  num_timeout_frames = inter_pipeline_video_src->num_timeout_frames;
  repeated_frame_as_gap = inter_pipeline_video_src->repeated_frame_as_gap;
  black_frame_as_gap = inter_pipeline_video_src->black_frame_as_gap;
  GST_OBJECT_UNLOCK (inter_pipeline_video_src);


  /* Lock the pipeline channel's mutex since we need to access the channel's
   * video info, video frame, and num_times_video_frame_pushed counter. */
  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_video_src->channel);

  /* If the channel's video info was updated since the last time we looked
   * at it, we must renegotiate the output caps to be able to continue
   * output the video frames that arrive over the channel. */
  if (inter_pipeline_video_src->channel->video_info_updated) {
    GST_DEBUG_OBJECT (inter_pipeline_video_src,
        "channel's video info got updated; need to renegotiate");

    /* Make a copy of the channel's video info for use in
     * gst_inter_pipeline_video_src_negotiate(). */
    inter_pipeline_video_src->cur_channel_video_info_valid =
        inter_pipeline_video_src->channel->video_info_valid;
    inter_pipeline_video_src->cur_channel_video_info =
        inter_pipeline_video_src->channel->video_info;

    /* Trigger renegotiation. This will eventually call our
     * gst_inter_pipeline_video_src_negotiate() function. */
    if (!gst_base_src_negotiate (GST_BASE_SRC_CAST (push_src))) {
      GST_ERROR_OBJECT (inter_pipeline_video_src, "could not renegotiate");
      GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_video_src->channel);
      flow_ret = GST_FLOW_NOT_NEGOTIATED;
      goto error;
    }

    /* Clear the video_info_updated flag so we don't renegotiate
     * again the next time create() is called (unless the video info
     * really was updated again). */
    inter_pipeline_video_src->channel->video_info_updated = FALSE;
  }

  /* If the channel actually has a video frame, push it downstream.
   * If the other side hasn't provided a new video frame for a while
   * now, then we'll be repeating the current video frame. Check for this.
   * If the timeout is nonzero, then we can repeatedly push a video frame
   * (= "repeating a frame") only a finite number of times, after which
   * we discard this frame, and instead push a black frame downstream. */
  if (inter_pipeline_video_src->channel->video_frame != NULL) {
    /* Ref the current frame. We do this because we want to output the
     * current frame (if it isn't NULL), even if we unref it in the block
     * below (because the block below makes sure the _next_ create()
     * call outputs a black frame, not _this_ call). */
    video_frame =
        gst_buffer_ref (inter_pipeline_video_src->channel->video_frame);

    /* Nonzero timeout, and timeout was exceeded -> discard existing frame
     * to make sure that the next create() call will push a black frame
     * downstream instead. */
    if ((num_timeout_frames > 0)
        && (inter_pipeline_video_src->channel->num_times_video_frame_pushed ==
            num_timeout_frames)) {
      GST_LOG_OBJECT (inter_pipeline_video_src,
          "timeout exceeded; subsequent create() calls "
          "will output black frames");
      gst_buffer_unref (inter_pipeline_video_src->channel->video_frame);
      inter_pipeline_video_src->channel->video_frame = NULL;
    }
  }

  if (video_frame != NULL) {
    /* We have an actual frame to output. Increment num_times_video_frame_pushed
     * to keep track of how many times we repeatedly pushed that frame
     * (required if the timeout property is nonzero; see above).
     * Also mark repeated frames as gaps if we are required to do so. */

    is_gap = repeated_frame_as_gap
        && (inter_pipeline_video_src->channel->num_times_video_frame_pushed !=
        0);

    GST_LOG_OBJECT (inter_pipeline_video_src,
        "output is an actual video frame:  "
        "marked as gap: %d  is repeated frame: %d",
        is_gap,
        (inter_pipeline_video_src->channel->num_times_video_frame_pushed != 0));

    inter_pipeline_video_src->channel->num_times_video_frame_pushed++;
  } else {
    /* We have no actual frame to output, so output a black frame instead.
     * These are marked as gaps if we are required to do so. */

    video_frame =
        gst_buffer_ref (inter_pipeline_video_src->black_video_frame_buffer);

    is_gap = black_frame_as_gap;

    GST_LOG_OBJECT (inter_pipeline_video_src,
        "output is a black video frame:  marked as gap: %d", is_gap);
  }

  /* If the interpipelinevideosink at the other end of the channel is waiting,
   * wake it up here, since at this point, we have taken the video frame off
   * the channel, so the sink can now put a new one in there. */
  GST_INTER_PIPELINE_CHANNEL_SIGNAL_COND (inter_pipeline_video_src->channel);

  /* We no longer need to access the channel, so unlock its mutex. */
  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_video_src->channel);


  /* At this point, we retrieved a video frame from the channel, or ref'd
   * a black frame in case the channel had no video frame, and determined
   * whether or not to mark the output as a gap. Set the metadata of the
   * outgoing GstBuffer (which contains the video frame), including the
   * gap flag, timestamps etc. */

  {
    GstClockTime pts, pts_end;

    if (is_gap)
      GST_BUFFER_FLAG_SET (video_frame, GST_BUFFER_FLAG_GAP);

    pts =
        inter_pipeline_video_src->timestamp_base_offset +
        gst_util_uint64_scale (GST_SECOND *
        inter_pipeline_video_src->num_frames_pushed,
        GST_VIDEO_INFO_FPS_D (&(inter_pipeline_video_src->output_video_info)),
        GST_VIDEO_INFO_FPS_N (&(inter_pipeline_video_src->output_video_info))
        );
    pts_end =
        inter_pipeline_video_src->timestamp_base_offset +
        gst_util_uint64_scale (GST_SECOND *
        (inter_pipeline_video_src->num_frames_pushed + 1),
        GST_VIDEO_INFO_FPS_D (&(inter_pipeline_video_src->output_video_info)),
        GST_VIDEO_INFO_FPS_N (&(inter_pipeline_video_src->output_video_info))
        );

    GST_BUFFER_DTS (video_frame) = GST_CLOCK_TIME_NONE;
    GST_BUFFER_PTS (video_frame) = pts;
    GST_BUFFER_DURATION (video_frame) = pts_end - pts;

    GST_BUFFER_OFFSET (video_frame) =
        inter_pipeline_video_src->num_frames_pushed;
    GST_BUFFER_OFFSET_END (video_frame) =
        inter_pipeline_video_src->num_frames_pushed + 1;

    /* Mark the first frame post-(re)negotiation as DISCONT. */
    if (inter_pipeline_video_src->num_frames_pushed == 0)
      GST_BUFFER_FLAG_SET (video_frame, GST_BUFFER_FLAG_DISCONT);
    else
      GST_BUFFER_FLAG_UNSET (video_frame, GST_BUFFER_FLAG_DISCONT);

    GST_LOG_OBJECT (inter_pipeline_video_src,
        "pushing video frame gstbuffer downstream: %" GST_PTR_FORMAT,
        (gpointer) video_frame);

    /* Increment the num_frames_pushed counter for PTS calculation. */
    inter_pipeline_video_src->num_frames_pushed++;
  }


  /* We are done. */
finish:
  *buffer = video_frame;
  return flow_ret;

error:
  gst_buffer_replace (&video_frame, NULL);
  goto finish;
}


static void
    gst_inter_pipeline_video_src_recalculate_num_timeout_frames
    (GstInterPipelineVideoSrc * inter_pipeline_video_src, GstClockTime timeout)
{
  gint fps_n, fps_d;

  fps_n = GST_VIDEO_INFO_FPS_N (&inter_pipeline_video_src->output_video_info);
  fps_d = GST_VIDEO_INFO_FPS_D (&inter_pipeline_video_src->output_video_info);

  if ((fps_n != 0) && (fps_d != 0)) {
    inter_pipeline_video_src->num_timeout_frames =
        gst_util_uint64_scale_ceil (timeout, fps_n, fps_d * GST_SECOND);
    GST_DEBUG_OBJECT (inter_pipeline_video_src,
        "there is output video info; convert timeout in ns %" GST_TIME_FORMAT
        " to timeout in frames %" G_GSIZE_FORMAT, GST_TIME_ARGS (timeout),
        inter_pipeline_video_src->num_timeout_frames);
  } else {
    GST_DEBUG_OBJECT (inter_pipeline_video_src,
        "there is no output video info; cannot convert timeout in ns %"
        GST_TIME_FORMAT " to anything; setting timeout in frames to 0",
        GST_TIME_ARGS (timeout)
        );
    inter_pipeline_video_src->num_timeout_frames = 0;
  }
}


static gboolean
gst_inter_pipeline_video_src_update_fixation_states (GstInterPipelineVideoSrc *
    inter_pipeline_video_src, const GstCaps * input_fixation_caps)
{
  /* The "fixation states" are fixation_caps and fixation_video_info.
   * We set them to the values from input_fixation_caps here. */

  GstVideoInfo new_fixation_video_info;
  GstCaps *new_fixation_caps;

  /* First, convert caps to video info. This sets various defaults in the
   * video info if the caps do not contain the corresponding values. */
  if (!gst_video_info_from_caps (&new_fixation_video_info, input_fixation_caps)) {
    GST_ERROR_OBJECT (inter_pipeline_video_src,
        "could not convert input fixation caps %" GST_PTR_FORMAT
        " to a video info; are these valid video caps?",
        (gpointer) input_fixation_caps);
    return FALSE;
  }

  /* Next, convert the video info back to caps, containing defaults that
   * may not have been set in the initial caps. This makes it possible
   * for the user to see these defaults by getting a GstCaps value from
   * the fixation-caps property. */
  new_fixation_caps = gst_video_info_to_caps (&new_fixation_video_info);
  g_return_val_if_fail (new_fixation_caps != NULL, FALSE);

  /* Now set the new video info & the new caps as the new fixation states. */
  gst_caps_replace (&(inter_pipeline_video_src->fixation_caps), NULL);
  inter_pipeline_video_src->fixation_caps = new_fixation_caps;
  inter_pipeline_video_src->fixation_video_info = new_fixation_video_info;

  return TRUE;
}
