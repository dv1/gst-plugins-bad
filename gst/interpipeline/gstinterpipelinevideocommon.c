#include "gstinterpipelinevideocommon.h"


G_DEFINE_TYPE (GstInterPipelineVideoChannel, gst_inter_pipeline_video_channel,
    GST_TYPE_INTER_PIPELINE_CHANNEL);


static void gst_inter_pipeline_video_channel_finalize (GObject * object);

static GstInterPipelineChannel
    * gst_inter_pipeline_video_channel_create (GstInterPipelineChannelTable *
    table, const gchar * name, gpointer extra_params, gpointer user_data);


void
gst_inter_pipeline_video_channel_class_init (GstInterPipelineVideoChannelClass *
    klass)
{
  GObjectClass *object_class;

  object_class = G_OBJECT_CLASS (klass);
  object_class->finalize =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_video_channel_finalize);
}


void
gst_inter_pipeline_video_channel_init (GstInterPipelineVideoChannel *
    inter_pipeline_video_channel)
{
  gst_video_info_init (&(inter_pipeline_video_channel->video_info));
  inter_pipeline_video_channel->video_info_valid = FALSE;
  inter_pipeline_video_channel->video_info_updated = FALSE;
  inter_pipeline_video_channel->video_frame = NULL;
  inter_pipeline_video_channel->num_times_video_frame_pushed = 0;
}


static void
gst_inter_pipeline_video_channel_finalize (GObject * object)
{
  GstInterPipelineVideoChannel *inter_pipeline_video_channel =
      GST_INTER_PIPELINE_VIDEO_CHANNEL (object);

  if (inter_pipeline_video_channel->video_frame != NULL)
    gst_buffer_unref (inter_pipeline_video_channel->video_frame);

  G_OBJECT_CLASS (gst_inter_pipeline_video_channel_parent_class)->finalize
      (object);
}


static GstInterPipelineChannel *
gst_inter_pipeline_video_channel_create (G_GNUC_UNUSED
    GstInterPipelineChannelTable * table, const gchar * name,
    G_GNUC_UNUSED gpointer extra_params, G_GNUC_UNUSED gpointer user_data)
{
  GstInterPipelineChannel *video_channel = (GstInterPipelineChannel *)
      g_object_new (gst_inter_pipeline_video_channel_get_type (), NULL);
  gst_object_set_name (GST_OBJECT (video_channel), name);
  gst_object_ref_sink (GST_OBJECT (video_channel));

  return video_channel;
}



static GstInterPipelineChannelTable video_channel_table =
GST_INTER_PIPELINE_CHANNEL_TABLE_INIT ("video-channel-table",
    gst_inter_pipeline_video_channel_create, NULL);

GstInterPipelineChannelTable *
gst_inter_pipeline_video_channel_table_get (void)
{
  return &video_channel_table;
}
