#include <gst/gst.h>
#include <gst/audio/audio.h>
#include "gstinterpipelinecommon.h"
#include "gstinterpipelineaudiocommon.h"
#include "gstinterpipelineaudiosink.h"


GST_DEBUG_CATEGORY_STATIC (inter_pipeline_audio_sink_debug);
#define GST_CAT_DEFAULT inter_pipeline_audio_sink_debug


enum
{
  PROP_0,
  PROP_CHANNEL_NAME,
  PROP_OVERFLOW_MODE,
  PROP_CHANNEL_CAPACITY,
  PROP_CHANNEL_BASE_LATENCY,
  PROP_CHANNEL_PERIOD_LENGTH,
  PROP_ACTUAL_CHANNEL_CAPACITY,
  PROP_ACTUAL_CHANNEL_BASE_LATENCY,
  PROP_ACTUAL_CHANNEL_PERIOD_LENGTH
};


#define DEFAULT_CHANNEL_NAME  NULL
#define DEFAULT_OVERFLOW_MODE GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE


static GstStaticPadTemplate static_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_INTER_PIPELINE_AUDIO_CAPS));


#define GST_INTER_PIPELINE_AUDIO_SINK(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_INTER_PIPELINE_AUDIO_SINK, \
    GstInterPipelineAudioSink))
#define GST_INTER_PIPELINE_AUDIO_SINK_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_INTER_PIPELINE_AUDIO_SINK, \
    GstInterPipelineAudioSinkClass))
#define GST_INTER_PIPELINE_AUDIO_SINK_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS((obj), GST_TYPE_INTER_PIPELINE_AUDIO_SINK, \
    GstInterPipelineAudioSinkClass))
#define GST_INTER_PIPELINE_AUDIO_SINK_CAST(obj) \
    ((GstInterPipelineAudioSink *)(obj))
#define GST_IS_INTER_PIPELINE_AUDIO_SINK(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_INTER_PIPELINE_AUDIO_SINK))
#define GST_IS_INTER_PIPELINE_AUDIO_SINK_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_INTER_PIPELINE_AUDIO_SINK))


typedef struct _GstInterPipelineAudioSink GstInterPipelineAudioSink;
typedef struct _GstInterPipelineAudioSinkClass GstInterPipelineAudioSinkClass;


struct _GstInterPipelineAudioSink
{
  GstBaseSink parent;

  /* Value of channel-name property.
   * Must be accessed with OBJECT_LOCK held. */
  gchar *channel_name;
  /* Value of overflow-mode property.
   * Must be accessed with OBJECT_LOCK held. */
  GstInterPipelineOverflowMode overflow_mode;

  /* These are local copies of the corresponding
   * members in GstInterPipelineAudioChannel.
   * These members exist here to make it possible
   * to set and store their values over GObject
   * properties even before the channel exists.
   * Must be accessed with OBJECT_LOCK held. */
  GstInterPipelineAudioChannelProperties channel_properties;
  GstInterPipelineAudioChannelProperties actual_channel_properties;

  /* The inter pipeline channel to use for transmitting audio data
   * to an interpipelineaudiosrc element. */
  GstInterPipelineAudioChannel *channel;

  /* Information about the input audio data. */
  GstInterPipelineAudioInfo input_audio_info;

  GstAdapter *input_adapter;

  /* If 1, then the sink is currently unlocked, and any calls that would
   * normally block must not block (and exit immediately instead). */
  gint unlocked;
};

struct _GstInterPipelineAudioSinkClass
{
  GstBaseSinkClass parent_class;
};


G_DEFINE_TYPE (GstInterPipelineAudioSink, gst_inter_pipeline_audio_sink,
    GST_TYPE_BASE_SINK);


static void gst_inter_pipeline_audio_sink_dispose (GObject * object);
static void gst_inter_pipeline_audio_sink_set_property (GObject * object,
    guint prop_id, GValue const *value, GParamSpec * pspec);
static void gst_inter_pipeline_audio_sink_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);

static gboolean gst_inter_pipeline_audio_sink_set_caps (GstBaseSink *
    base_sink, GstCaps * caps);
static void gst_inter_pipeline_audio_sink_get_times (GstBaseSink *
    base_sink, GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_inter_pipeline_audio_sink_start (GstBaseSink * base_sink);
static gboolean gst_inter_pipeline_audio_sink_stop (GstBaseSink * base_sink);
static gboolean gst_inter_pipeline_audio_sink_event (GstBaseSink * base_sink,
    GstEvent * event);
static gboolean gst_inter_pipeline_audio_sink_query (GstBaseSink * base_sink,
    GstQuery * query);
static gboolean gst_inter_pipeline_audio_sink_unlock (GstBaseSink * base_sink);
static gboolean gst_inter_pipeline_audio_sink_unlock_stop (GstBaseSink *
    base_sink);
static GstFlowReturn gst_inter_pipeline_audio_sink_render (GstBaseSink *
    base_sink, GstBuffer * buffer);




static void
gst_inter_pipeline_audio_sink_class_init (GstInterPipelineAudioSinkClass *
    klass)
{
  GObjectClass *object_class;
  GstElementClass *element_class;
  GstBaseSinkClass *base_sink_class;

  GST_DEBUG_CATEGORY_INIT (inter_pipeline_audio_sink_debug,
      "interpipelineaudiosink", 0, "inter pipeline audio sink");

  object_class = G_OBJECT_CLASS (klass);
  element_class = GST_ELEMENT_CLASS (klass);
  base_sink_class = GST_BASE_SINK_CLASS (klass);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&static_sink_template));

  object_class->dispose =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_dispose);
  object_class->set_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_set_property);
  object_class->get_property =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_get_property);

  base_sink_class->set_caps =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_set_caps);
  base_sink_class->get_times =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_get_times);
  base_sink_class->start =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_start);
  base_sink_class->stop =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_stop);
  base_sink_class->event =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_event);
  base_sink_class->query =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_query);
  base_sink_class->unlock =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_unlock);
  base_sink_class->unlock_stop =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_unlock_stop);
  base_sink_class->render =
      GST_DEBUG_FUNCPTR (gst_inter_pipeline_audio_sink_render);

  gst_element_class_set_static_metadata (element_class,
      "interpipelineaudiosink",
      "Sink/Audio",
      "Virtual audio sink for transferring audio frames between pipelines "
      " in the same process", "Carlos Rafael Giani <crg7475@mailbox.org>");

  g_object_class_install_property (object_class,
      PROP_CHANNEL_NAME,
      g_param_spec_string ("channel-name",
          "Channel name",
          "Unique name of transmission channel between this sink and a "
          "corresponding source (must be a non-NULL non-empty string)",
          DEFAULT_CHANNEL_NAME, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_OVERFLOW_MODE,
      g_param_spec_enum ("overflow-mode",
          "Overflow mode",
          "What to do in case the channel's buffer is full",
          gst_inter_pipeline_overflow_mode_get_type (),
          DEFAULT_OVERFLOW_MODE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_CHANNEL_CAPACITY,
      g_param_spec_uint64 ("channel-capacity",
          "Channel capacity",
          "How much data the channel can maximally hold, in nanoseconds",
          0, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_CAPACITY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_CHANNEL_BASE_LATENCY,
      g_param_spec_uint64 ("base-latency",
          "Base latency",
          "The base latency that is always present, in nanoseconds",
          0, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_BASE_LATENCY,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_CHANNEL_PERIOD_LENGTH,
      g_param_spec_uint64 ("period-length",
          "Period length",
          "Length of one period, in nanoseconds; transmissions from sink to "
          "source are performed in period units",
          0, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_ACTUAL_CHANNEL_CAPACITY,
      g_param_spec_uint64 ("actual-channel-capacity",
          "Actual channel capacity",
          "Actual channel capacity that is being used, in nanoseconds",
          1, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_CAPACITY,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_ACTUAL_CHANNEL_BASE_LATENCY,
      g_param_spec_uint64 ("actual-base-latency",
          "Actual base latency",
          "Actual base latency that is being used, in nanoseconds",
          1, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_BASE_LATENCY,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
  g_object_class_install_property (object_class,
      PROP_ACTUAL_CHANNEL_PERIOD_LENGTH,
      g_param_spec_uint64 ("actual-period-length",
          "Actual period length",
          "Actual period length that is being used, in nanoseconds",
          1, G_MAXUINT64,
          DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH,
          G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));
}


static void
gst_inter_pipeline_audio_sink_init (GstInterPipelineAudioSink *
    inter_pipeline_audio_sink)
{
  inter_pipeline_audio_sink->channel_name = g_strdup (DEFAULT_CHANNEL_NAME);
  inter_pipeline_audio_sink->overflow_mode = DEFAULT_OVERFLOW_MODE;

  inter_pipeline_audio_sink->channel = NULL;
  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_sink->input_audio_info));
  inter_pipeline_audio_sink->input_adapter = gst_adapter_new ();
  inter_pipeline_audio_sink->unlocked = 0;

  inter_pipeline_audio_sink->channel_properties.capacity_in_ns =
      DEFAULT_AUDIO_CHANNEL_CAPACITY;
  inter_pipeline_audio_sink->channel_properties.base_latency_in_ns =
      DEFAULT_AUDIO_CHANNEL_BASE_LATENCY;
  inter_pipeline_audio_sink->channel_properties.period_length_in_ns =
      DEFAULT_AUDIO_CHANNEL_PERIOD_LENGTH;

  inter_pipeline_audio_sink->actual_channel_properties =
      inter_pipeline_audio_sink->channel_properties;
}


static void
gst_inter_pipeline_audio_sink_dispose (GObject * object)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (object);

  g_free (inter_pipeline_audio_sink->channel_name);

  if (inter_pipeline_audio_sink->input_adapter != NULL) {
    g_object_unref (G_OBJECT (inter_pipeline_audio_sink->input_adapter));
    inter_pipeline_audio_sink->input_adapter = NULL;
  }

  G_OBJECT_CLASS (gst_inter_pipeline_audio_sink_parent_class)->dispose (object);
}


static void
gst_inter_pipeline_audio_sink_set_property (GObject * object, guint prop_id,
    GValue const *value, GParamSpec * pspec)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_free (inter_pipeline_audio_sink->channel_name);
      inter_pipeline_audio_sink->channel_name = g_value_dup_string (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_OVERFLOW_MODE:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      inter_pipeline_audio_sink->overflow_mode = g_value_get_enum (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_CHANNEL_CAPACITY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      inter_pipeline_audio_sink->channel_properties.capacity_in_ns =
          g_value_get_uint64 (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_CHANNEL_BASE_LATENCY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      inter_pipeline_audio_sink->channel_properties.base_latency_in_ns =
          g_value_get_uint64 (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_CHANNEL_PERIOD_LENGTH:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      inter_pipeline_audio_sink->channel_properties.period_length_in_ns =
          g_value_get_uint64 (value);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static void
gst_inter_pipeline_audio_sink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (object);

  switch (prop_id) {
    case PROP_CHANNEL_NAME:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_string (value, inter_pipeline_audio_sink->channel_name);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_OVERFLOW_MODE:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_enum (value, inter_pipeline_audio_sink->overflow_mode);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_CHANNEL_CAPACITY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_uint64 (value,
          inter_pipeline_audio_sink->channel_properties.capacity_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_CHANNEL_BASE_LATENCY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_uint64 (value,
          inter_pipeline_audio_sink->channel_properties.base_latency_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_CHANNEL_PERIOD_LENGTH:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_uint64 (value,
          inter_pipeline_audio_sink->channel_properties.period_length_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_ACTUAL_CHANNEL_CAPACITY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_uint64 (value,
          inter_pipeline_audio_sink->actual_channel_properties.capacity_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_ACTUAL_CHANNEL_BASE_LATENCY:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_uint64 (value,
          inter_pipeline_audio_sink->actual_channel_properties.
          base_latency_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    case PROP_ACTUAL_CHANNEL_PERIOD_LENGTH:
    {
      GST_OBJECT_LOCK (inter_pipeline_audio_sink);
      g_value_set_uint64 (value,
          inter_pipeline_audio_sink->actual_channel_properties.
          period_length_in_ns);
      GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);
      break;
    }

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


static gboolean
gst_inter_pipeline_audio_sink_set_caps (GstBaseSink * base_sink, GstCaps * caps)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);
  GstInterPipelineAudioInfo new_audio_info;
  gsize num_bytes;

  if (!gst_inter_pipeline_audio_info_from_caps (&new_audio_info, caps)) {
    GST_ERROR_OBJECT (inter_pipeline_audio_sink,
        "caps %" GST_PTR_FORMAT " could not be parsed or recognized",
        (gpointer) caps);
    return FALSE;
  }

  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);

  /* Drain any remaining samples before setting new caps. Do so by first
   * pushing any remaining input samples into the channel's audio adapter. */
  num_bytes = gst_adapter_available (inter_pipeline_audio_sink->input_adapter);
  if (num_bytes > 0) {
    GstBuffer *tmp_buffer;

    GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
        "pushing remaining %" G_GSIZE_FORMAT
        " bytes from our adapter into the channel's", num_bytes);
    tmp_buffer =
        gst_adapter_take_buffer (inter_pipeline_audio_sink->input_adapter,
        num_bytes);
    gst_adapter_push (inter_pipeline_audio_sink->channel->audio_adapter,
        tmp_buffer);
  }

  /* Now wait until the other side consumes all data from the channel's
   * audio adapter. */
  while (TRUE) {
    num_bytes =
        gst_adapter_available (inter_pipeline_audio_sink->channel->
        audio_adapter);
    if (num_bytes == 0)
      break;

    GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
        "the other side hasn't consumed remaining %" G_GSIZE_FORMAT
        " bytes from the channel's adapter yet; waiting", num_bytes);
    GST_INTER_PIPELINE_CHANNEL_WAIT_COND (inter_pipeline_audio_sink->channel);

    /* If the condition variable was signaled by unlock(), we have
     * to call gst_base_sink_wait_preroll() according to the GstBaseSink
     * documentation. */
    if (g_atomic_int_get (&inter_pipeline_audio_sink->unlocked)) {
      GstFlowReturn ret;
      GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);
      if ((ret = gst_base_sink_wait_preroll (base_sink)) != GST_FLOW_OK)
        goto stopping;
      GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);
    }
  }

  GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
      "the other side consumed all remaining data; setting new caps now");

  /* Now it is safe to set new audio info. This will cause the other
   * side to set different caps, which in turn will trigger a caps
   * event etc. Since we made sure all old samples have been transmitted
   * earlier, there is no danger of setting the new caps too early. */
  inter_pipeline_audio_sink->channel->audio_info = new_audio_info;
  inter_pipeline_audio_sink->input_audio_info = new_audio_info;

  /* Recalculate channel properties like capacity_in_frames, since they
   * depend on audio info fields like the sample rate, and we just set
   * a new audio info. */
  gst_inter_pipeline_audio_channel_properties_recalc_num_frames (&
      (inter_pipeline_audio_sink->channel->properties), &new_audio_info);

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);

stopping:
  return TRUE;
}


static void
gst_inter_pipeline_audio_sink_get_times (GstBaseSink * base_sink,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);

  /* Compute valid start / end timestamps based on the information we have.
   * If there is no valid timestamp data, we cannot compute anything,
   * and start & end are set to GST_CLOCK_TIME_NONE.
   * If there is a valid buffer timestamp, we can assign its value to start,
   * and compute end either by using the buffer's duration (if there is one),
   * or assume that the buffer duration implicitely corresponds to the
   * size of the buffer (which is translated to a duration by using the
   * sample rate and the number of bytes per frame (BPF)). */

  if (GST_BUFFER_PTS_IS_VALID (buffer)) {
    GstClockTime pts = GST_BUFFER_PTS (buffer);

    *start = pts;

    if (GST_BUFFER_DURATION_IS_VALID (buffer)) {
      *end = pts + GST_BUFFER_DURATION (buffer);
      GST_LOG_OBJECT (inter_pipeline_audio_sink,
          "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
          GST_TIME_FORMAT, (gpointer) buffer, GST_TIME_ARGS (*start),
          GST_TIME_ARGS (*end));
    } else {
      guint sample_rate =
          gst_inter_pipeline_audio_info_get_sample_rate (&
          (inter_pipeline_audio_sink->input_audio_info));
      guint bpf =
          gst_inter_pipeline_audio_info_get_bpf (&
          (inter_pipeline_audio_sink->input_audio_info));

      if (sample_rate > 0)
        *end =
            pts + gst_util_uint64_scale_int (gst_buffer_get_size (buffer),
            GST_SECOND, sample_rate * bpf);

      GST_LOG_OBJECT (inter_pipeline_audio_sink,
          "start-end times for given buffer %p: %" GST_TIME_FORMAT " - %"
          GST_TIME_FORMAT " based on a sample rate of %u Hz and %u bpf",
          (gpointer) buffer, GST_TIME_ARGS (*start), GST_TIME_ARGS (*end),
          sample_rate, bpf);
    }

    return;
  }

  GST_LOG_OBJECT (inter_pipeline_audio_sink,
      "no start-end times for given buffer %p specified since buffer "
      "has no PTS", (gpointer) buffer);

  *start = *end = GST_CLOCK_TIME_NONE;
}


static gboolean
gst_inter_pipeline_audio_sink_start (GstBaseSink * base_sink)
{
  gchar *channel_name;
  GstInterPipelineAudioChannelProperties channel_properties;
  gboolean ret = TRUE;
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);


  /* Make copies of channel_name and channel_properties to avoid
   * race conditions and having to hold the object lock throughout
   * this entire function. */
  GST_OBJECT_LOCK (inter_pipeline_audio_sink);
  channel_name = g_strdup (inter_pipeline_audio_sink->channel_name);
  channel_properties = inter_pipeline_audio_sink->channel_properties;
  GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);


  /* Perform some sanity checks. */

  if ((channel_name == NULL) || (channel_name[0] == '\0')) {
    GST_ELEMENT_ERROR (inter_pipeline_audio_sink, RESOURCE, NOT_FOUND,
        ("Channel name is NULL or an empty string."), (NULL));
    ret = FALSE;
    goto finish;
  }

  if (channel_properties.capacity_in_ns <
      channel_properties.period_length_in_ns) {
    GST_ELEMENT_ERROR (inter_pipeline_audio_sink, RESOURCE, NOT_FOUND,
        ("Channel capacity is set to a value greater than period length."),
        ("channel capacity: %" GST_TIME_FORMAT "  period length: %"
            GST_TIME_FORMAT, GST_TIME_ARGS (channel_properties.capacity_in_ns),
            GST_TIME_ARGS (channel_properties.period_length_in_ns)));
    ret = FALSE;
    goto finish;
  }


  /* Initialize structures. */

  gst_inter_pipeline_audio_info_init (&
      (inter_pipeline_audio_sink->input_audio_info));
  gst_adapter_clear (inter_pipeline_audio_sink->input_adapter);


  /* Now get the channel with the specified name. */

  GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
      "getting audio channel \"%s\" (gets created if it does not exist yet)",
      channel_name);

  inter_pipeline_audio_sink->channel =
      GST_INTER_PIPELINE_AUDIO_CHANNEL
      (gst_inter_pipeline_channel_table_get_channel
      (gst_inter_pipeline_audio_channel_table_get (),
          GST_ELEMENT_CAST (base_sink), TRUE, channel_name, &channel_properties)
      );

  if (G_UNLIKELY (inter_pipeline_audio_sink->channel == NULL)) {
    GST_ERROR_OBJECT (inter_pipeline_audio_sink, "could not get audio channel");
    ret = FALSE;
  }


  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);

  /* Get a copy of the channel properties that are actually in use.
   * These might be different from the ones we supplied to the get_channel()
   * if the channel already existed (its properties are set only once, at
   * the time of the channel's creation). To be able to query the actual
   * properties via the actual-* GObject properties, make a copy. */
  GST_OBJECT_LOCK (inter_pipeline_audio_sink);
  inter_pipeline_audio_sink->actual_channel_properties =
      inter_pipeline_audio_sink->channel->properties;
  GST_OBJECT_UNLOCK (inter_pipeline_audio_sink);

  /* The base latency is the minimum latency that is always present.
   * This corresponds to the sink's render delay, so pass it on as such. */
  gst_base_sink_set_render_delay (base_sink,
      inter_pipeline_audio_sink->channel->properties.base_latency_in_ns);

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);


  GST_TRACE_OBJECT (inter_pipeline_audio_sink,
      "inter pipeline audio sink started");

finish:
  g_free (channel_name);
  return ret;
}


static gboolean
gst_inter_pipeline_audio_sink_stop (GstBaseSink * base_sink)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);

  /* The channel's finalize function takes care of removing the
   * channel from the table if the refcount reaches 0. We do it
   * this way, since the source element at the other end may still
   * be holding a reference to the channel, so removing it here
   * from the table would not be correct. */
  if (G_LIKELY (inter_pipeline_audio_sink->channel != NULL)) {
    GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);
    gst_adapter_clear (inter_pipeline_audio_sink->channel->audio_adapter);
    GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);

    GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
        "unref'ing audio channel \"%s\"",
        GST_INTER_PIPELINE_CHANNEL_NAME (inter_pipeline_audio_sink->channel));
    gst_inter_pipeline_channel_unref (GST_INTER_PIPELINE_CHANNEL_CAST
        (inter_pipeline_audio_sink->channel), GST_ELEMENT_CAST (base_sink));
    inter_pipeline_audio_sink->channel = NULL;
  } else
    GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
        "there is no audio channel; nothing to unref");

  gst_adapter_clear (inter_pipeline_audio_sink->input_adapter);

  GST_TRACE_OBJECT (inter_pipeline_audio_sink,
      "inter pipeline audio sink stopped");

  return TRUE;
}


static gboolean
gst_inter_pipeline_audio_sink_event (GstBaseSink * base_sink, GstEvent * event)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
    {
      GstBuffer *tmp;
      guint n;

      /* We need to drain the input adapter when an EOS happens. The drained
       * data is placed into the channel so that the source at the other side
       * receives it. Otherwise, the data left in the input adapter would get lost. */
      if ((n = gst_adapter_available (inter_pipeline_audio_sink->input_adapter))
          > 0) {
        GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);
        tmp =
            gst_adapter_take_buffer (inter_pipeline_audio_sink->input_adapter,
            n);
        gst_adapter_push (inter_pipeline_audio_sink->channel->audio_adapter,
            tmp);
        GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);
      }

      break;
    }
    default:
      break;
  }

  return
      GST_BASE_SINK_CLASS (gst_inter_pipeline_audio_sink_parent_class)->event
      (base_sink, event);
}


static gboolean
gst_inter_pipeline_audio_sink_query (GstBaseSink * base_sink, GstQuery * query)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);
  gboolean ret;

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_LATENCY:
    {
      gboolean sink_is_live, upstream_is_live;
      GstClockTime upstream_min_latency, upstream_max_latency;
      GstClockTime base_latency, min_latency, max_latency;

      if (!gst_base_sink_query_latency (GST_BASE_SINK_CAST (base_sink),
              &sink_is_live, &upstream_is_live, &upstream_min_latency,
              &upstream_max_latency))
        return FALSE;

      /* Both sink and upstream are live; adjust the min_latency. */
      if (sink_is_live && upstream_is_live) {
        GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);
        base_latency =
            GST_INTER_PIPELINE_AUDIO_CHANNEL
            (inter_pipeline_audio_sink->channel)->properties.base_latency_in_ns;
        GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);

        /* Can't go lower than the buffer size and upstream's min latency. */
        min_latency = base_latency + upstream_min_latency;
        /* Unlike with min latency, there's no upper latency limit. The
         * total max latency is the sum of upstream's latency and our
         * channel's base latency. */
        max_latency =
            GST_CLOCK_TIME_IS_VALID (upstream_max_latency) ? (base_latency +
            upstream_max_latency) : GST_CLOCK_TIME_NONE;

        GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
            "upstream's min latency: %" GST_TIME_FORMAT "  our min latency: %"
            GST_TIME_FORMAT, GST_TIME_ARGS (upstream_min_latency),
            GST_TIME_ARGS (min_latency));
        GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
            "upstream's max latency %" GST_TIME_FORMAT "  our max latency: %"
            GST_TIME_FORMAT, GST_TIME_ARGS (upstream_max_latency),
            GST_TIME_ARGS (max_latency));
      } else {
        GST_DEBUG_OBJECT (base_sink,
            "either sink is not live, or upstream isn't, or none are; "
            "don't care about latency");
        min_latency = upstream_min_latency;
        max_latency = upstream_max_latency;
      }

      gst_query_set_latency (query, sink_is_live, min_latency, max_latency);

      ret = TRUE;

      break;
    }

    default:
      ret =
          GST_BASE_SINK_CLASS
          (gst_inter_pipeline_audio_sink_parent_class)->query (base_sink,
          query);
  }

  return ret;
}


static gboolean
gst_inter_pipeline_audio_sink_unlock (GstBaseSink * base_sink)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);

  /* Raise the unlocked flag. */
  g_atomic_int_set (&inter_pipeline_audio_sink->unlocked, 1);

  /* Wake up the show_frame() function if it is currently
   * waiting on our condition variable. */
  GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
      "sink unlocked; waking up condition variable (if it is waiting)");
  GST_INTER_PIPELINE_CHANNEL_SIGNAL_COND (inter_pipeline_audio_sink->channel);

  return TRUE;
}


static gboolean
gst_inter_pipeline_audio_sink_unlock_stop (GstBaseSink * base_sink)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);

  /* Clear the unlocked flag so that show_frame() can wait on
   * the condition variable again like normal if it needs to. */
  g_atomic_int_set (&inter_pipeline_audio_sink->unlocked, 0);

  GST_DEBUG_OBJECT (inter_pipeline_audio_sink, "sink unlock stopped");

  return TRUE;
}


static GstFlowReturn
gst_inter_pipeline_audio_sink_render (GstBaseSink * base_sink,
    GstBuffer * buffer)
{
  GstInterPipelineAudioSink *inter_pipeline_audio_sink =
      GST_INTER_PIPELINE_AUDIO_SINK (base_sink);
  GstFlowReturn ret = GST_FLOW_OK;
  GstClockTime period_length_in_ns;
  guint64 capacity_in_frames, period_length_in_frames;
  guint bpf;
  gsize num_frames_in_channel;
  gsize num_bytes_in_input_adapter;

  /* We need to access the channel during the execution of this
   * whole function. Lock its mutex to avoid race conditions. */
  GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);

  /* Make copies of the channel properties for easier use here. */
  period_length_in_ns =
      GST_INTER_PIPELINE_AUDIO_CHANNEL (inter_pipeline_audio_sink->channel)->
      properties.period_length_in_ns;
  capacity_in_frames =
      GST_INTER_PIPELINE_AUDIO_CHANNEL (inter_pipeline_audio_sink->channel)->
      properties.capacity_in_frames;
  period_length_in_frames =
      GST_INTER_PIPELINE_AUDIO_CHANNEL (inter_pipeline_audio_sink->channel)->
      properties.period_length_in_frames;

  bpf =
      gst_inter_pipeline_audio_info_get_bpf (&
      (inter_pipeline_audio_sink->input_audio_info));
  g_assert (bpf > 0);

  num_frames_in_channel =
      gst_adapter_available (inter_pipeline_audio_sink->channel->
      audio_adapter) / bpf;

  switch (inter_pipeline_audio_sink->overflow_mode) {
    case GST_INTER_PIPELINE_OVERFLOW_MODE_OVERWRITE:
    {
      /* In overwrite mode, "overwrite" the oldest data in the
       * channel by throwing away the oldest frames that are
       * currently stored in the channel's adapter. Do this
       * in period length steps, since source elements also
       * pull data in period length units. */

      while (num_frames_in_channel > capacity_in_frames) {
        GST_DEBUG_OBJECT (inter_pipeline_audio_sink,
            "discarding oldest %" G_GUINT64_FORMAT
            " frames in channel (corresponds to period length %" GST_TIME_FORMAT
            ")", period_length_in_frames, GST_TIME_ARGS (period_length_in_ns));
        gst_adapter_flush (inter_pipeline_audio_sink->channel->audio_adapter,
            period_length_in_frames * bpf);
        num_frames_in_channel -= period_length_in_frames;
      }

      break;
    }

    case GST_INTER_PIPELINE_OVERFLOW_MODE_BLOCK:
    {
      /* In block mode, wait until the other side (= the source
       * element that is also using this channel) signals that
       * data has been consumed, or until the unlocked flag is
       * set. In the latter case, we have to wait for preroll.
       * This is because the unlock() function (which sets the
       * unlocked flag) is called whenever something occurs that
       * requires prerolling again, such as pause/unpause. */

      while (num_frames_in_channel > capacity_in_frames) {
        if (g_atomic_int_get (&inter_pipeline_audio_sink->unlocked)) {
          GST_INTER_PIPELINE_CHANNEL_UNLOCK
              (inter_pipeline_audio_sink->channel);

          /* Waiting according to the GstBaseSink documentation, which states
           * that "subclasses should unblock any blocked function ASAP and
           * call gst_base_sink_wait_preroll()". */
          if ((ret = gst_base_sink_wait_preroll (base_sink)) != GST_FLOW_OK)
            goto stopping;

          GST_INTER_PIPELINE_CHANNEL_LOCK (inter_pipeline_audio_sink->channel);
        }

        GST_INTER_PIPELINE_CHANNEL_WAIT_COND
            (inter_pipeline_audio_sink->channel);

        num_frames_in_channel =
            gst_adapter_available (inter_pipeline_audio_sink->channel->
            audio_adapter) / bpf;
      }

      break;
    }

      /* There are no other modes defined. Should never reach this place. */
    default:
      g_assert_not_reached ();
  }

  num_bytes_in_input_adapter =
      gst_adapter_available (inter_pipeline_audio_sink->input_adapter);
  if ((gst_buffer_get_size (buffer) + num_bytes_in_input_adapter) <
      (period_length_in_frames * bpf)) {
    /* There is not enough data for one full period, so just push
     * the input buffer into the input adapter. We'll continue
     * gathering data there until eventually we do have enough
     * data for one full period that we can transfer over to
     * the channel's adapter. */
    gst_adapter_push (inter_pipeline_audio_sink->input_adapter,
        gst_buffer_ref (buffer));
  } else {
    GstBuffer *tmp_buffer;

    /* The input adapter and the incoming buffer together have
     * enough data for at least one full period. So, we can
     * transfer data over to the channel. */

    if (num_bytes_in_input_adapter > 0) {
      tmp_buffer =
          gst_adapter_take_buffer (inter_pipeline_audio_sink->input_adapter,
          num_bytes_in_input_adapter);
      gst_adapter_push (inter_pipeline_audio_sink->channel->audio_adapter,
          tmp_buffer);
    }

    gst_adapter_push (inter_pipeline_audio_sink->channel->audio_adapter,
        gst_buffer_ref (buffer));
  }

  GST_INTER_PIPELINE_CHANNEL_UNLOCK (inter_pipeline_audio_sink->channel);
  return ret;

stopping:
  GST_DEBUG_OBJECT (inter_pipeline_audio_sink, "we are stopping");
  return ret;
}
