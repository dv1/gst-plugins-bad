/* GStreamer
 * Copyright (C) 2011 David A. Schleef <ds@schleef.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Suite 500,
 * Boston, MA 02110-1335, USA.
 */
/**
 * SECTION:element-gstinteraudiosink
 * @title: gstinteraudiosink
 *
 * The interaudiosink element is an audio sink element.  It is used
 * in connection with a interaudiosrc element in a different pipeline,
 * similar to intervideosink and intervideosrc.
 *
 * ## Example launch line
 * |[
 * gst-launch-1.0 -v audiotestsrc ! queue ! interaudiosink
 * ]|
 *
 * The interaudiosink element cannot be used effectively with gst-launch-1.0,
 * as it requires a second pipeline in the application to receive the
 * audio.
 * See the gstintertest.c example in the gst-plugins-bad source code for
 * more details.
 *
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include <gst/base/gstbasesink.h>
#include <gst/audio/audio.h>
#include "gstinteraudiosink.h"
#include <string.h>

GST_DEBUG_CATEGORY_STATIC (gst_inter_audio_sink_debug_category);
#define GST_CAT_DEFAULT gst_inter_audio_sink_debug_category

/* prototypes */
static void gst_inter_audio_sink_set_property (GObject * object,
    guint property_id, const GValue * value, GParamSpec * pspec);
static void gst_inter_audio_sink_get_property (GObject * object,
    guint property_id, GValue * value, GParamSpec * pspec);
static void gst_inter_audio_sink_finalize (GObject * object);

static void gst_inter_audio_sink_get_times (GstBaseSink * sink,
    GstBuffer * buffer, GstClockTime * start, GstClockTime * end);
static gboolean gst_inter_audio_sink_start (GstBaseSink * sink);
static gboolean gst_inter_audio_sink_stop (GstBaseSink * sink);
static gboolean gst_inter_audio_sink_set_caps (GstBaseSink * sink,
    GstCaps * caps);
static gboolean gst_inter_audio_sink_event (GstBaseSink * sink,
    GstEvent * event);
static GstFlowReturn gst_inter_audio_sink_render (GstBaseSink * sink,
    GstBuffer * buffer);
static gboolean gst_inter_audio_sink_query (GstBaseSink * sink,
    GstQuery * query);
static gboolean gst_inter_audio_sink_unlock (GstBaseSink * sink);
static gboolean gst_inter_audio_sink_unlock_stop (GstBaseSink * sink);

enum
{
  PROP_0,
  PROP_CHANNEL,
  PROP_OVERFLOW_MODE
};

#define DEFAULT_CHANNEL ("default")
#define DEFAULT_OVERFLOW_MODE GST_INTER_OVERFLOW_MODE_OVERWRITE

/* pad templates */
static GstStaticPadTemplate gst_inter_audio_sink_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_AUDIO_CAPS_MAKE (GST_AUDIO_FORMATS_ALL))
    );

/* class initialization */
#define parent_class gst_inter_audio_sink_parent_class
G_DEFINE_TYPE (GstInterAudioSink, gst_inter_audio_sink, GST_TYPE_BASE_SINK);

static void
gst_inter_audio_sink_class_init (GstInterAudioSinkClass * klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);
  GstBaseSinkClass *base_sink_class = GST_BASE_SINK_CLASS (klass);

  GST_DEBUG_CATEGORY_INIT (gst_inter_audio_sink_debug_category,
      "interaudiosink", 0, "debug category for interaudiosink element");
  gst_element_class_add_static_pad_template (element_class,
      &gst_inter_audio_sink_sink_template);

  gst_element_class_set_static_metadata (element_class,
      "Internal audio sink",
      "Sink/Audio",
      "Virtual audio sink for internal process communication",
      "David Schleef <ds@schleef.org>");

  gobject_class->set_property = gst_inter_audio_sink_set_property;
  gobject_class->get_property = gst_inter_audio_sink_get_property;
  gobject_class->finalize = gst_inter_audio_sink_finalize;
  base_sink_class->get_times =
      GST_DEBUG_FUNCPTR (gst_inter_audio_sink_get_times);
  base_sink_class->start = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_start);
  base_sink_class->stop = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_stop);
  base_sink_class->event = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_event);
  base_sink_class->set_caps = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_set_caps);
  base_sink_class->render = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_render);
  base_sink_class->query = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_query);
  base_sink_class->unlock = GST_DEBUG_FUNCPTR (gst_inter_audio_sink_unlock);
  base_sink_class->unlock_stop =
      GST_DEBUG_FUNCPTR (gst_inter_audio_sink_unlock_stop);

  g_object_class_install_property (gobject_class, PROP_CHANNEL,
      g_param_spec_string ("channel", "Channel",
          "Channel name to match inter src and sink elements",
          DEFAULT_CHANNEL, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_property (gobject_class, PROP_OVERFLOW_MODE,
      g_param_spec_enum ("overflow-mode", "Overflow mode",
          "What to do in case the audio buffer is full",
          GST_TYPE_INTER_OVERFLOW_MODE,
          DEFAULT_OVERFLOW_MODE, G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));
}

static void
gst_inter_audio_sink_init (GstInterAudioSink * interaudiosink)
{
  interaudiosink->channel = g_strdup (DEFAULT_CHANNEL);
  interaudiosink->overflow_mode = DEFAULT_OVERFLOW_MODE;
  interaudiosink->unlocked = 0;
  interaudiosink->input_adapter = gst_adapter_new ();
}

void
gst_inter_audio_sink_set_property (GObject * object, guint property_id,
    const GValue * value, GParamSpec * pspec)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (object);

  switch (property_id) {
    case PROP_CHANNEL:
      g_free (interaudiosink->channel);
      interaudiosink->channel = g_value_dup_string (value);
      break;
    case PROP_OVERFLOW_MODE:
      interaudiosink->overflow_mode = g_value_get_enum (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_inter_audio_sink_get_property (GObject * object, guint property_id,
    GValue * value, GParamSpec * pspec)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (object);

  switch (property_id) {
    case PROP_CHANNEL:
      g_value_set_string (value, interaudiosink->channel);
      break;
    case PROP_OVERFLOW_MODE:
      g_value_set_enum (value, interaudiosink->overflow_mode);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
      break;
  }
}

void
gst_inter_audio_sink_finalize (GObject * object)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (object);

  /* clean up object here */
  g_free (interaudiosink->channel);
  gst_object_unref (interaudiosink->input_adapter);

  G_OBJECT_CLASS (gst_inter_audio_sink_parent_class)->finalize (object);
}

static void
gst_inter_audio_sink_get_times (GstBaseSink * sink, GstBuffer * buffer,
    GstClockTime * start, GstClockTime * end)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer)) {
    *start = GST_BUFFER_TIMESTAMP (buffer);
    if (GST_BUFFER_DURATION_IS_VALID (buffer)) {
      *end = *start + GST_BUFFER_DURATION (buffer);
    } else {
      if (interaudiosink->info.rate > 0) {
        *end = *start +
            gst_util_uint64_scale_int (gst_buffer_get_size (buffer), GST_SECOND,
            interaudiosink->info.rate * interaudiosink->info.bpf);
      }
    }
  }
}

static gboolean
gst_inter_audio_sink_start (GstBaseSink * sink)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);

  GST_DEBUG_OBJECT (interaudiosink, "start");

  interaudiosink->surface = gst_inter_surface_get (interaudiosink->channel);
  g_mutex_lock (&interaudiosink->surface->mutex);
  memset (&interaudiosink->surface->audio_info, 0, sizeof (GstAudioInfo));

  /* We want to write latency-time before syncing has happened */
  /* FIXME: The other side can change this value when it starts */
  gst_base_sink_set_render_delay (sink,
      interaudiosink->surface->audio_latency_time);
  g_mutex_unlock (&interaudiosink->surface->mutex);

  return TRUE;
}

static gboolean
gst_inter_audio_sink_stop (GstBaseSink * sink)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);

  GST_DEBUG_OBJECT (interaudiosink, "stop");

  g_mutex_lock (&interaudiosink->surface->mutex);
  gst_adapter_clear (interaudiosink->surface->audio_adapter);
  memset (&interaudiosink->surface->audio_info, 0, sizeof (GstAudioInfo));
  g_mutex_unlock (&interaudiosink->surface->mutex);

  gst_inter_surface_unref (interaudiosink->surface);
  interaudiosink->surface = NULL;

  gst_adapter_clear (interaudiosink->input_adapter);

  return TRUE;
}

static gboolean
gst_inter_audio_sink_set_caps (GstBaseSink * sink, GstCaps * caps)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);
  GstAudioInfo info;
  GstBuffer *tmp;
  gsize num_bytes;

  GST_TRACE_OBJECT (sink, "Setting new caps %" GST_PTR_FORMAT, caps);

  if (!gst_audio_info_from_caps (&info, caps)) {
    GST_ERROR_OBJECT (sink, "Failed to parse caps %" GST_PTR_FORMAT, caps);
    return FALSE;
  }

  g_mutex_lock (&interaudiosink->surface->mutex);

  /* Drain any remaining samples before setting new caps. Do so by first
   * pushing any remaining input samples into the surface's audio adapter. */
  num_bytes = gst_adapter_available (interaudiosink->input_adapter);
  if (num_bytes > 0) {
    GST_DEBUG_OBJECT (sink, "Pushing remaining %" G_GSIZE_FORMAT " bytes "
        "from our adapter into the surface's", num_bytes);
    tmp = gst_adapter_take_buffer (interaudiosink->input_adapter, num_bytes);
    gst_adapter_push (interaudiosink->surface->audio_adapter, tmp);
  }

  /* Now wait until the other side consumes all data from the surface's
   * audio adapter. */
  while (TRUE) {
    num_bytes = gst_adapter_available (interaudiosink->surface->audio_adapter);
    if (num_bytes == 0)
      break;

    GST_DEBUG_OBJECT (sink, "The other side hasn't consumed remaining %"
        G_GSIZE_FORMAT " bytes from the surface's adapter yet; waiting",
        num_bytes);
    g_cond_wait (&interaudiosink->surface->cond,
        &interaudiosink->surface->mutex);

    /* If the condition variable was signaled by unlock(), we have
     * to call gst_base_sink_wait_preroll() according to the GstBaseSink
     * documentation. */
    if (g_atomic_int_get (&interaudiosink->unlocked)) {
      GstFlowReturn ret;
      g_mutex_unlock (&interaudiosink->surface->mutex);
      if ((ret = gst_base_sink_wait_preroll (sink)) != GST_FLOW_OK)
        goto stopping;
      g_mutex_lock (&interaudiosink->surface->mutex);
    }
  }

  GST_DEBUG_OBJECT (sink, "The other side consumed all remaining data; "
      "setting new caps now");

  /* Now it is safe to set new audio info. This will cause the other
   * side to set different caps, which in turn will trigger a caps
   * event etc. Since we made sure all old samples have been transmitted
   * earlier, there is no danger of setting the new caps too early. */
  interaudiosink->surface->audio_info = info;
  interaudiosink->info = info;

  g_mutex_unlock (&interaudiosink->surface->mutex);

stopping:
  return TRUE;
}

static gboolean
gst_inter_audio_sink_event (GstBaseSink * sink, GstEvent * event)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:{
      GstBuffer *tmp;
      guint n;

      if ((n = gst_adapter_available (interaudiosink->input_adapter)) > 0) {
        g_mutex_lock (&interaudiosink->surface->mutex);
        tmp = gst_adapter_take_buffer (interaudiosink->input_adapter, n);
        gst_adapter_push (interaudiosink->surface->audio_adapter, tmp);
        g_mutex_unlock (&interaudiosink->surface->mutex);
      }
      break;
    }
    default:
      break;
  }

  return GST_BASE_SINK_CLASS (parent_class)->event (sink, event);
}

static GstFlowReturn
gst_inter_audio_sink_render (GstBaseSink * sink, GstBuffer * buffer)
{
  GstFlowReturn ret = GST_FLOW_OK;
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);
  guint n, bpf;
  guint64 period_time, buffer_time;
  guint64 period_samples, buffer_samples;

  GST_DEBUG_OBJECT (interaudiosink, "render %" G_GSIZE_FORMAT,
      gst_buffer_get_size (buffer));
  bpf = interaudiosink->info.bpf;

  g_mutex_lock (&interaudiosink->surface->mutex);

  buffer_time = interaudiosink->surface->audio_buffer_time;
  period_time = interaudiosink->surface->audio_period_time;

  if (buffer_time < period_time) {
    GST_ERROR_OBJECT (interaudiosink,
        "Buffer time smaller than period time (%" GST_TIME_FORMAT " < %"
        GST_TIME_FORMAT ")", GST_TIME_ARGS (buffer_time),
        GST_TIME_ARGS (period_time));
    g_mutex_unlock (&interaudiosink->surface->mutex);
    return GST_FLOW_ERROR;
  }

  buffer_samples =
      gst_util_uint64_scale (buffer_time, interaudiosink->info.rate,
      GST_SECOND);
  period_samples =
      gst_util_uint64_scale (period_time, interaudiosink->info.rate,
      GST_SECOND);

  n = gst_adapter_available (interaudiosink->surface->audio_adapter) / bpf;
  switch (interaudiosink->overflow_mode) {
    case GST_INTER_OVERFLOW_MODE_OVERWRITE:{
      while (n > buffer_samples) {
        GST_DEBUG_OBJECT (interaudiosink, "discarding oldest %" GST_TIME_FORMAT
            " ns", GST_TIME_ARGS (period_time));
        gst_adapter_flush (interaudiosink->surface->audio_adapter,
            period_samples * bpf);
        n -= period_samples;
      }
      break;
    }

    case GST_INTER_OVERFLOW_MODE_BLOCK:{
      while ((n > buffer_samples)) {
        if (g_atomic_int_get (&interaudiosink->unlocked)) {
          g_mutex_unlock (&interaudiosink->surface->mutex);
          /* Waiting according to the GstBaseSink documentation, which states
           * that "subclasses should unblock any blocked function ASAP and
           * call gst_base_sink_wait_preroll()". */
          if ((ret = gst_base_sink_wait_preroll (sink)) != GST_FLOW_OK)
            goto stopping;
          g_mutex_lock (&interaudiosink->surface->mutex);
        }

        g_cond_wait (&interaudiosink->surface->cond,
            &interaudiosink->surface->mutex);
        n = gst_adapter_available (interaudiosink->surface->audio_adapter) /
            bpf;
      }
      break;
    }
  }

  n = gst_adapter_available (interaudiosink->input_adapter);
  if (period_samples * bpf > gst_buffer_get_size (buffer) + n) {
    gst_adapter_push (interaudiosink->input_adapter, gst_buffer_ref (buffer));
  } else {
    GstBuffer *tmp;

    if (n > 0) {
      tmp = gst_adapter_take_buffer (interaudiosink->input_adapter, n);
      gst_adapter_push (interaudiosink->surface->audio_adapter, tmp);
    }
    gst_adapter_push (interaudiosink->surface->audio_adapter,
        gst_buffer_ref (buffer));
  }

  g_mutex_unlock (&interaudiosink->surface->mutex);
  return ret;

stopping:
  GST_DEBUG_OBJECT (interaudiosink, "we are stopping");
  return ret;
}

static gboolean
gst_inter_audio_sink_query (GstBaseSink * sink, GstQuery * query)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);
  gboolean ret;

  GST_DEBUG_OBJECT (sink, "query");

  switch (GST_QUERY_TYPE (query)) {
    case GST_QUERY_LATENCY:{
      gboolean live, us_live;
      GstClockTime min_l, max_l;

      GST_DEBUG_OBJECT (sink, "latency query");

      if ((ret =
              gst_base_sink_query_latency (GST_BASE_SINK_CAST (sink), &live,
                  &us_live, &min_l, &max_l))) {
        GstClockTime base_latency, min_latency, max_latency;

        /* we and upstream are both live, adjust the min_latency */
        if (live && us_live) {
          /* FIXME: The other side can change this value when it starts */
          base_latency = interaudiosink->surface->audio_latency_time;

          /* we cannot go lower than the buffer size and the min peer latency */
          min_latency = base_latency + min_l;
          /* the max latency is the max of the peer, we can delay an infinite
           * amount of time. */
          max_latency = (max_l == -1) ? -1 : (base_latency + max_l);

          GST_DEBUG_OBJECT (sink,
              "peer min %" GST_TIME_FORMAT ", our min latency: %"
              GST_TIME_FORMAT, GST_TIME_ARGS (min_l),
              GST_TIME_ARGS (min_latency));
          GST_DEBUG_OBJECT (sink,
              "peer max %" GST_TIME_FORMAT ", our max latency: %"
              GST_TIME_FORMAT, GST_TIME_ARGS (max_l),
              GST_TIME_ARGS (max_latency));
        } else {
          GST_DEBUG_OBJECT (sink,
              "peer or we are not live, don't care about latency");
          min_latency = min_l;
          max_latency = max_l;
        }
        gst_query_set_latency (query, live, min_latency, max_latency);
      }
      break;
    }
    default:
      ret =
          GST_BASE_SINK_CLASS (gst_inter_audio_sink_parent_class)->query (sink,
          query);
      break;
  }

  return ret;
}

static gboolean
gst_inter_audio_sink_unlock (GstBaseSink * sink)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);
  g_atomic_int_set (&interaudiosink->unlocked, 1);
  g_cond_signal (&interaudiosink->surface->cond);

  return TRUE;
}

static gboolean
gst_inter_audio_sink_unlock_stop (GstBaseSink * sink)
{
  GstInterAudioSink *interaudiosink = GST_INTER_AUDIO_SINK (sink);
  g_atomic_int_set (&interaudiosink->unlocked, 0);

  return TRUE;
}
