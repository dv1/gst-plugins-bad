/* GStreamer
 * Copyright (C) 2017 Carlos Rafael Giani <dv@pseudoterminal.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef _GST_INTER_H_
#define _GST_INTER_H_

#include <gst/gst.h>

G_BEGIN_DECLS


typedef enum {
  GST_INTER_OVERFLOW_MODE_OVERWRITE,
  GST_INTER_OVERFLOW_MODE_BLOCK
} GstInterOverflowMode;

#define GST_TYPE_INTER_OVERFLOW_MODE (gst_inter_overflow_mode_get_type())
GType gst_inter_overflow_mode_get_type (void);


G_END_DECLS

#endif

